<?php

namespace Modules\Preselection\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class NotifyCheckingInterview extends Mailable
{
    use Queueable, SerializesModels;

    public $aspirant;
    public $subject;
    public $view;

    public function __construct($aspirant,$subject,$view)
    {
        $this->subject = $subject;
        $this->view = $view;
        $this->aspirant = $aspirant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view($this->view)
            ->subject($this->subject);
    }
}
