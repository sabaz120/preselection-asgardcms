<?php

namespace Modules\Preselection\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class NotifyAspirantMedicalExam extends Mailable
{
    use Queueable, SerializesModels;

    public $aspirant;
    public $subject;
    public $view;
    public $laboratory_address;

    public function __construct($aspirants,$subject,$view,$laboratory_address)
    {
        $this->aspirant = $aspirants;
        $this->subject = $subject;
        $this->view = $view;
        $this->laboratory_address = $laboratory_address;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view($this->view)
            ->subject($this->subject);
    }
}
