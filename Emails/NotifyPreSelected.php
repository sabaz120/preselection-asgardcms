<?php

namespace Modules\Preselection\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class NotifyPreSelected extends Mailable
{
    use Queueable, SerializesModels;

    public $aspirant;
    public $subject;
    public $view;
    public $code;

    public function __construct($aspirants,$subject,$view,$code)
    {
        $this->aspirant = $aspirants;
        $this->subject = $subject;
        $this->view = $view;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view($this->view)
            ->subject($this->subject);
    }
}
