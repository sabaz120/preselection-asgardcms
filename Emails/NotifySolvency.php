<?php

namespace Modules\Preselection\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class NotifySolvency extends Mailable
{
    use Queueable, SerializesModels;

    public $solvency;
    public $subject;
    public $view;

    public function __construct($solvency,$subject,$view)
    {
        $this->subject = $subject;
        $this->view = $view;
        $this->solvency = $solvency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view($this->view)
            ->subject($this->subject);
    }
}
