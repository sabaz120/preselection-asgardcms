<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewDocumentsFieldsAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->string('passport_document')->nullable();//Pasaporte
        $table->string('identification_document')->nullable();//Documento de identidad
        $table->string('food_handling_document')->nullable();//Manipulacion de alimentos
        $table->string('good_conduct_letter_document')->nullable();//Carta de buena conducta
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('preselection__aspirants', function (Blueprint $table) {
      $table->dropColumn('passport_document');//Pasaporte
      $table->dropColumn('identification_document');//Documento de identidad
      $table->dropColumn('food_handling_document');//Manipulacion de alimentos
      $table->dropColumn('good_conduct_letter_document');//Carta de buena conducta
    });
        //
    }
}
