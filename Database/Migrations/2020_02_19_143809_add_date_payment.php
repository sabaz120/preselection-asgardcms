<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->date('date_payment_generate')->nullable();
        $table->date('date_payment_delivered')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->dropColumn('date_payment_generate');
        $table->dropColumn('date_payment_delivered');
      });
    }
}
