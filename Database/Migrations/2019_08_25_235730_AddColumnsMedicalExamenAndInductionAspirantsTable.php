<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMedicalExamenAndInductionAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->boolean('medical_exam')->default(false);
        $table->boolean('induction')->default(false);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('preselection__aspirants', function (Blueprint $table) {
      $table->dropColumn('medical_exam');
      $table->dropColumn('induction');
    });
    }
}
