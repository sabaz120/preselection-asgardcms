<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicalExamenHourAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('preselection__aspirants', function (Blueprint $table) {
          $table->time('medical_exam_hour')->nullable();

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->dropColumn('medical_exam_hour');
      });
    }
}
