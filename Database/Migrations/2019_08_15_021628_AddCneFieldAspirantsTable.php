<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCneFieldAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->string('cne_data')->nullable();
        $table->boolean('cne_query')->default(false);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('preselection__aspirants', function (Blueprint $table) {
      $table->dropColumn('cne_data');
      $table->dropColumn('cne_query');
    });
    }
}
