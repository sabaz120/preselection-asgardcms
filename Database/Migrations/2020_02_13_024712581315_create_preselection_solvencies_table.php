<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreselectionSolvenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__solvencies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('aspirant_id')->unsigned();
            $table->foreign('aspirant_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->text('reason');
            $table->text('observation')->nullable();
            $table->integer('user1_id')->unsigned();
            $table->foreign('user1_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->integer('user2_id')->unsigned()->nullable();
            $table->foreign('user2_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->integer('user3_id')->unsigned()->nullable();
            $table->foreign('user3_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->integer('user4_id')->unsigned()->nullable();
            $table->foreign('user4_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->smallInteger('status')->default(0);//0 - En proceso / 1 - Generadas
            $table->float('settlement_amount', 10, 2)->default(0);//Monto de liquidación
            $table->smallInteger('status_payment')->default(0);//0 - Pendiente / 1 - generado / 2 - Entregado
            $table->integer('payer_id')->unsigned()->nullable();//Usuario que entrego el pago
            $table->foreign('payer_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->integer('auth_payer_id')->unsigned()->nullable();//Usuario que registro la entrega del pago
            $table->foreign('auth_payer_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__solvencies');
    }
}
