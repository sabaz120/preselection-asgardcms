<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreselectionDataChangeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__datachangerequests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('field');
            /*
            Field:  (name,last_name,second_name,second_surname,phone,address,
            name,institution,hours,date,business_name,business_address,
            business_phone,business_contact_person,job_title,
            description_functions_performed,since,until,
            reason_for_termination,last_salary,type_education,
            instituion,date_end,city,title)
            */
            $table->string('type');
            $table->string('value');
            $table->smallInteger('status')->default(0);//0 - pending - 1 completed - rejected
            /*
            --type (aspirant,course,workexperience,study)
            */
            $table->integer('entity_id')->nullable();
            $table->integer('aspirant_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__datachangerequests');
    }
}
