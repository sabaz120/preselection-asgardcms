<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__applicant_work_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aspirant_id')->unsigned();
            $table->foreign('aspirant_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->string('business_name');
            $table->string('business_address');
            $table->string('business_phone');
            $table->string('business_contact_person');
            $table->string('job_title');
            $table->text('description_functions_performed');
            $table->date('since');
            $table->date('until');
            $table->text('reason_for_termination');
            $table->float('last_salary', 30, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__applicant_work_experiences');
    }
}
