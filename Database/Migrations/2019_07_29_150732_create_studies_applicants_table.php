<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudiesApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__studies_applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aspirant_id')->unsigned();
            $table->foreign('aspirant_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->string('type_education');
            $table->string('institution');
            $table->date('date_end');
            $table->string('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__studies_applicants');
    }
}
