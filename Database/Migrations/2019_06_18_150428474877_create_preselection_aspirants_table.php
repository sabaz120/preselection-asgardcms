<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreselectionAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__aspirants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('identification');
            $table->string('email');
            $table->string('sexper');
            $table->string('nationality');
            $table->string('phone');
            $table->string('address');
            $table->integer('unity_id');
            //$table->text('summary');
            $table->string('curriculum_file');
            $table->text('options')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__aspirants');
    }
}
