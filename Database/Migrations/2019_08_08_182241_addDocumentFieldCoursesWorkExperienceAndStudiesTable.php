<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentFieldCoursesWorkExperienceAndStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__studies_applicants', function (Blueprint $table) {
        $table->string('document')->nullable();
      });
      Schema::table('preselection__applicant_courses', function (Blueprint $table) {
        $table->string('document')->nullable();
      });
      Schema::table('preselection__applicant_work_experiences', function (Blueprint $table) {
        $table->string('document')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('preselection__studies_applicants', function (Blueprint $table) {
      $table->dropColumn('document');
    });
    Schema::table('preselection__applicant_courses', function (Blueprint $table) {
      $table->dropColumn('document');
    });
    Schema::table('preselection__applicant_work_experiences', function (Blueprint $table) {
      $table->dropColumn('document');
    });
        //
    }
}
