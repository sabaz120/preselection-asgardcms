<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFloatFieldsToPercentageRecommendedAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->float('percentage_studies', 10, 2)->default(0);
        $table->float('percentage_address', 10, 2)->default(0);
        $table->float('percentage_work_experience', 10, 2)->default(0);
        $table->timestamp('medical_exam_date')->nullable();
        $table->timestamp('induction_date')->nullable();
        $table->boolean('safety_equipment')->default(false);
        $table->timestamp('safety_equipment_date')->nullable();
        $table->boolean('identity_card')->default(false);
        $table->timestamp('identity_card_date')->nullable();
        $table->boolean('work_contract')->default(false);
        $table->timestamp('work_contract_date')->nullable();

     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->dropColumn('work_contract_date');
        $table->dropColumn('work_contract');
        $table->dropColumn('identity_card_date');
        $table->dropColumn('identity_card');
        $table->dropColumn('safety_equipment_date');
        $table->dropColumn('safety_equipment');
        $table->dropColumn('induction_date');
        $table->dropColumn('medical_exam_date');
        $table->dropColumn('percentage_studies');
        $table->dropColumn('percentage_address');
        $table->dropColumn('percentage_work_experience');
      });
    }
}
