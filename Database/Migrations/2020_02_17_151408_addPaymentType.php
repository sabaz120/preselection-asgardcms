<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /****TIPOS DE PAGOS
      #1 Transfernecia
      #2 Efectivo
      #3 Cheque
      #4 Otros
     ***/

    public function up()
    {
      Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->smallInteger('type_payment')->default(0);
        $table->integer('auth_payment_generator_id')->unsigned()->nullable();//Usuario que registro la entrega del pago
        $table->foreign('auth_payment_generator_id')->references('id')->on('users')->onDelete('cascade');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->dropColumn('type_payment');
      });
    }
}
