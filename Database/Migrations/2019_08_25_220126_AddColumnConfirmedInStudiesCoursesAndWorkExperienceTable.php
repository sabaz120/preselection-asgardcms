<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnConfirmedInStudiesCoursesAndWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__studies_applicants', function (Blueprint $table) {
        $table->boolean('confirmed')->default(false);
      });
      Schema::table('preselection__applicant_courses', function (Blueprint $table) {
        $table->boolean('confirmed')->default(false);
      });
      Schema::table('preselection__applicant_work_experiences', function (Blueprint $table) {
        $table->boolean('confirmed')->default(false);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::table('preselection__studies_applicants', function (Blueprint $table) {
      $table->dropColumn('confirmed');
    });
    Schema::table('preselection__applicant_courses', function (Blueprint $table) {
      $table->dropColumn('confirmed');
    });
    Schema::table('preselection__applicant_work_experiences', function (Blueprint $table) {
      $table->dropColumn('confirmed');
    });
    }
}
