<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSolvenciesBankReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->string('bank_reference')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preselection__solvencies', function (Blueprint $table) {
        $table->string('bank_reference')->nullable();
      });
    }
}
