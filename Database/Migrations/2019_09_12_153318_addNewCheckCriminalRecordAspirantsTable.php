<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewCheckCriminalRecordAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->boolean('criminal_record')->default(false);
        $table->timestamp('criminal_record_date')->nullable();

     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->dropColumn('criminal_record');
        $table->dropColumn('criminal_record_date');
      });
    }
}
