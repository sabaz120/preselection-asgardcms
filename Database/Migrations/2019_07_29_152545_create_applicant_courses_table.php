<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preselection__applicant_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aspirant_id')->unsigned();
            $table->foreign('aspirant_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');
            $table->string('name');
            $table->string('institution');
            $table->integer('hours');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preselection__applicant_courses');
    }
}
