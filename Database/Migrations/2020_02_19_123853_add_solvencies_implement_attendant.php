<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSolvenciesImplementAttendant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preselection__solvencies', function (Blueprint $table) {
          $table->integer('implement_attendant_id')->unsigned();
          $table->foreign('implement_attendant_id')->references('id')->on('preselection__aspirants')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preselection__solvencies', function (Blueprint $table) {
          $table->dropColumn('implement_attendant_id');
        });
    }
}
