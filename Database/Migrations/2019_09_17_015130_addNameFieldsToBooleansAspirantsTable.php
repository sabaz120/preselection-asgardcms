<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameFieldsToBooleansAspirantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->string('work_contract_attendant')->nullable();
        $table->string('identity_card_attendant')->nullable();
        $table->string('safety_equipment_attendant')->nullable();
        $table->string('induction_attendant')->nullable();
        $table->string('medical_exam_doctor')->nullable();
        $table->text('medical_exam_observation')->nullable();
        $table->string('medical_exam_file')->nullable();

     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('preselection__aspirants', function (Blueprint $table) {
        $table->dropColumn('work_contract_attendant');
        $table->dropColumn('identity_card_attendant');
        $table->dropColumn('safety_equipment_attendant');
        $table->dropColumn('induction_attendant');
        $table->dropColumn('medical_exam_doctor');
        $table->dropColumn('medical_exam_observation');
        $table->dropColumn('medical_exam_file');
      });

    }
}
