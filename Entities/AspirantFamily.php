<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class AspirantFamily extends Model
{

    protected $table = 'preselection__aspirantfamilies';
    protected $fillable = [
    	"aspirant_id",
    	"relationship",//parentesco
    	"identification",
    	"name",
    	"last_name",
    	"birthday",
    ];
}
