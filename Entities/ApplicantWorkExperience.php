<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class ApplicantWorkExperience extends Model
{
    protected $table="preselection__applicant_work_experiences";
    protected $fillable = [
      "aspirant_id",
      "business_name",
      "business_address",
      "business_phone",
      "business_contact_person",
      "job_title",
      "description_functions_performed",
      "since",
      "until",
      "reason_for_termination",
      "document",
      "confirmed",
      "last_salary"
    ];
}
