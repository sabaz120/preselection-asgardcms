<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class AspirantStatus extends Model
{

      protected $table = 'preselection__aspirant_statuses';
      protected $fillable = [
      	"aspirant_id",
      	"status",//
      	"user_id",
      ];
}
