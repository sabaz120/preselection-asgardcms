<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class ApplicantCourse extends Model
{
    protected $table="preselection__applicant_courses";
    protected $fillable = [
      "aspirant_id",
      "name",
      "institution",
      "hours",
      "document",
      "confirmed",
      "date"
    ];
}
