<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class DataChangeRequest extends Model
{

    protected $table = 'preselection__datachangerequests';
    protected $fillable = [
      "field",
      "type",
      "value",
      "entity_id",
      "aspirant_id",
      'status'
    ];
    public function aspirant(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','aspirant_id');
    }
    public function study(){
      return $this->belongsTo('Modules\Preselection\Entities\StudiesApplicant','entity_id');
    }
    public function course(){
      return $this->belongsTo('Modules\Preselection\Entities\ApplicantCourse','entity_id');
    }
    public function workExperience(){
      return $this->belongsTo('Modules\Preselection\Entities\ApplicantWorkExperience','entity_id');
    }
}
