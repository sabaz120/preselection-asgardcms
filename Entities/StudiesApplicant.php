<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class StudiesApplicant extends Model
{
    protected $table="preselection__studies_applicants";
    protected $fillable = [
      "aspirant_id",
      "type_education",
      "institution",
      "date_end",
      "confirmed",
      "document",
      "city",
      "title",
    ];
}
