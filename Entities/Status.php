<?php

namespace Modules\Preselection\Entities;

class Status
{
    const PENDIENTE = 1;
    const APROBADO = 2;
    const RECHAZADO = 3;
    const PORPAROBACIONENTREVISTA = 4;
    const ENPROCESODEENTREVISTA = 5;
    const ENTREVISTAAPROBADA = 6;
    const ENTREVISTANOAPROBADA = 7;
    const PORAPROBAREXAMENMEDICO = 8;
    const ENPROCESOEXAMENMEDICO = 9;
    const CONTRATADO = 10;
    const LIQUIDADO = 11;


    /**
     * @var array
     */
    private $statuses = [];

    public function __construct()
    {
        $this->statuses = [
            self::PENDIENTE => "Pendiente",
            self::APROBADO => "Aprobado",
            self::RECHAZADO => "Rechazado",
            self::PORPAROBACIONENTREVISTA => "Por aprobación de entrevista",
            self::ENPROCESODEENTREVISTA => "En proceso de entrevista",
            self::ENTREVISTAAPROBADA => "Entrevista aprobada",
            self::ENTREVISTANOAPROBADA => "Entrevista no aprobada",
            self::PORAPROBAREXAMENMEDICO => "Por aprobar examen médico",
            self::ENPROCESOEXAMENMEDICO => "En proceso de examen médico",
            self::CONTRATADO => "Contratado",
            self::LIQUIDADO => "Liquidado",

        ];
    }

    /**
     * Get the available statuses
     * @return array
     */
    public function lists()
    {
        return $this->statuses;
    }

    /**
     * Get the post status
     * @param int $statusId
     * @return string
     */
    public function get($statusId)
    {
        if (isset($this->statuses[$statusId])) {
            return $this->statuses[$statusId];
        }

        return $this->statuses[self::PENDIENTE];
    }
}
