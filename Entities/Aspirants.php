<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class Aspirants extends Model
{

    protected $table = 'preselection__aspirants';
    protected $fillable = [
      'name',
      'last_name',
      'identification',
      'email',
      'phone',
      'address',
      'unity_id',
      //'summary',
      'curriculum_file',
      'options',
      'status',
      'sexper',
      'nationality',
      'business_id',
      'parish_id',
      'city_id',
      'recommended',
      'second_name',
      'second_surname',
      'passport_document',
      'identification_document',
      'food_handling_document',
      'good_conduct_letter_document',
      'code',
      'cne_query',
      'cne_data',
      'medical_exam',
      'induction',
      'percentage_studies',
      'percentage_address',
      'percentage_work_experience',
      'medical_exam_date',
      'medical_exam_hour',
      'medical_exam_doctor',
      'medical_exam_observation',
      'medical_exam_file',
      'induction_date',
      'induction_attendant',
      'safety_equipment',
      'safety_equipment_date',
      'safety_equipment_attendant',
      'identity_card',
      'identity_card_date',
      'identity_card_attendant',
      'work_contract',
      'work_contract_date',
      'work_contract_attendant',
      'criminal_record',
      'criminal_record_date',
      'health_certificate',
      'unit_aspirated',
      'implements',

    ];

    protected $dates = [
        'medical_exam_date',
        'induction_date',
        'safety_equipment_date',
        'identity_card_date',
        'work_contract_date'
    ];

    protected $casts = [
        'options' => 'array',
        'implements' => 'array',
        "criminal_record"=>"boolean"

    ];

   protected $fakeColumns = ['options'];
    public function courses(){
      return $this->hasMany('Modules\Preselection\Entities\ApplicantCourse','aspirant_id');
    }

    public function studies(){
      return $this->hasMany('Modules\Preselection\Entities\StudiesApplicant','aspirant_id');
    }

    public function workExperience(){
      return $this->hasMany('Modules\Preselection\Entities\ApplicantWorkExperience','aspirant_id');
    }

    public function solvencies(){
      return $this->hasMany('Modules\Preselection\Entities\Solvency','aspirant_id');
    }

    public function families(){
      return $this->hasMany('Modules\Preselection\Entities\AspirantFamily','aspirant_id');
    }

    public function unit(){
      return $this->belongsTo('Modules\Business\Entities\Unit','unity_id');
    }

    public function business(){
      return $this->belongsTo('Modules\Business\Entities\Business','business_id');
    }

    public function parish(){
      return $this->belongsTo('Modules\Locations\Entities\Parish','parish_id');
    }

    public function city(){
      return $this->belongsTo('Modules\Locations\Entities\City','city_id');
    }

    // public function getOptionsAttribute($value) {
    //       if(!is_string(json_decode($value))){
    //           return json_decode($value);
    //       }
    //       return json_decode(json_decode($value));
    //   }
    public function getImplementsAttribute($value) {
        if($value){
          return $value;
        }else{
          return [];
        }
      }



}
