<?php

namespace Modules\Preselection\Entities;

use Illuminate\Database\Eloquent\Model;

class Solvency extends Model
{

    protected $table = 'preselection__solvencies';
    protected $fillable = [
      "aspirant_id",
      "reason",
      "observation",
      "user1_id",
      "user2_id",
      "user3_id",
      "user4_id",
      "status",
      "settlement_amount",
      "status_payment",
      "payer_id",
      "auth_payer_id",
      "auth_payment_generator_id",
      "type_payment",
      "payment_observation",
      "implement_attendant_id",
      "medical_exam_voucher",
      "date_payment_generate",
      "date_payment_delivered",
      "bank_name",
      "bank_reference",

    ];

    public function aspirant(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','aspirant_id');
    }

    public function user1(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','user1_id');
    }

    public function user2(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','user2_id');
    }

    public function user3(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','user3_id');
    }

    public function user4(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','user4_id');
    }

    public function payer(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','payer_id');
    }

    public function authPayer(){
      return $this->belongsTo('Modules\User\Entities\Sentinel\User','auth_payer_id');
    }

    public function authPaymentGenerator(){
      return $this->belongsTo('Modules\User\Entities\Sentinel\User','auth_payment_generator_id');
    }

    public function implementAttendant(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','implement_attendant_id');
    }


}
