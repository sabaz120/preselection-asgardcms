<?php

namespace Modules\Preselection\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Preselection\Events\Handlers\RegisterPreselectionSidebar;

class PreselectionServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterPreselectionSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('aspirants', array_dot(trans('preselection::aspirants')));
            $event->load('aspirantfamilies', array_dot(trans('preselection::aspirantfamilies')));
            $event->load('datachangerequests', array_dot(trans('preselection::datachangerequests')));
            $event->load('solvencies', array_dot(trans('preselection::solvencies')));
            // append translations





        });
    }

    public function boot()
    {
        $this->publishConfig('preselection', 'permissions');
        $this->publishConfig('preselection', 'settings');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Preselection\Repositories\AspirantsRepository',
            function () {
                $repository = new \Modules\Preselection\Repositories\Eloquent\EloquentAspirantsRepository(new \Modules\Preselection\Entities\Aspirants());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Preselection\Repositories\Cache\CacheAspirantsDecorator($repository);
            }
        );

        $this->app->bind(
            'Modules\Preselection\Repositories\AspirantFamilyRepository',
            function () {
                $repository = new \Modules\Preselection\Repositories\Eloquent\EloquentAspirantFamilyRepository(new \Modules\Preselection\Entities\AspirantFamily());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Preselection\Repositories\Cache\CacheAspirantFamilyDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Preselection\Repositories\DataChangeRequestRepository',
            function () {
                $repository = new \Modules\Preselection\Repositories\Eloquent\EloquentDataChangeRequestRepository(new \Modules\Preselection\Entities\DataChangeRequest());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Preselection\Repositories\Cache\CacheDataChangeRequestDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Preselection\Repositories\SolvencyRepository',
            function () {
                $repository = new \Modules\Preselection\Repositories\Eloquent\EloquentSolvencyRepository(new \Modules\Preselection\Entities\Solvency());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Preselection\Repositories\Cache\CacheSolvencyDecorator($repository);
            }
        );
// add bindings





    }
}
