<?php

namespace Modules\Preselection\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterPreselectionSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('preselection::common.title.preselections'), function (Item $item) {
                $item->icon('fa fa-edit');
                $item->weight(10);
                $item->authorize(
                     $this->auth->hasAccess('preselection.aspirants.menu')
                );
                $item->item(trans('preselection::aspirants.title.aspirants'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    // $item->append('admin.preselection.aspirants.create');
                    $item->route('admin.preselection.aspirants.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.aspirants.index')
                    );
                });

                $item->item(trans('preselection::aspirants.list resource doctor'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.preselection.aspirantsMedicalExam.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.aspirants.indexDoctor')
                    );
                });

                $item->item(trans('preselection::aspirants.list resource workers'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.preselection.workers.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.aspirants.indexWorkers')
                    );
                });

                $item->item(trans('preselection::datachangerequests.title.datachangerequests'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.preselection.datachangerequest.create');
                    $item->route('admin.preselection.datachangerequest.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.datachangerequests.index')
                    );
                });
                // $item->item(trans('preselection::solvencies.title.solvencies'), function (Item $item) {
                //     $item->icon('fa fa-copy');
                //     $item->weight(0);
                //     $item->append('admin.preselection.solvency.create');
                //     $item->route('admin.preselection.solvency.index');
                //     $item->authorize(
                //         $this->auth->hasAccess('preselection.solvencies.index')
                //     );
                // });
// append

            });


            $group->item(trans('Egreso'), function (Item $item) {
                $item->icon('fa fa-user-circle');
                $item->weight(10);
                $item->authorize(
                     $this->auth->hasAccess('preselection.solvencies.index')
                );
                $item->item(trans('preselection::solvencies.title.solvencies'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.preselection.solvency.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.solvencies.index')
                    );
                });


                $item->item(trans('preselection::solvencies.title.solvenciesGenerate'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.preselection.solvency.generated');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.solvencies.index')
                    );
                });

                $item->item(trans('Listado de egreso'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->route('admin.preselection.retiredWorkers.index');
                    $item->authorize(
                        $this->auth->hasAccess('preselection.aspirants.retiredWorkers')
                    );
                });

            });
        });

        return $menu;
    }
}
