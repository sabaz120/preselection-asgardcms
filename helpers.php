<?php
use Modules\Preselection\Entities\Status;
use Modules\Business\Entities\Unit;

if (!function_exists('business__getUnits')) {

	function business__getUnits()
	{
		return Unit::all();
	}
}

if (!function_exists('preselection__getStatus')) {

	function preselection__getStatus()
	{
		$status = new Status();
		return $status;
	}
}

if (!function_exists('getCneData')) {

	function getCneData($nac, $ci) {
		try {
			$url = "http://www.cne.gov.ve/web/registro_electoral/ce.php?nacionalidad=$nac&cedula=$ci";
			$resource = geUrl($url);
			$text = strip_tags($resource);
			$findme = 'SERVICIO ELECTORAL'; // Identifica que si es población Votante
			$pos = strpos($text, $findme);

			$findme2 = 'ADVERTENCIA'; // Identifica que si es población Votante
			$pos2 = strpos($text, $findme2);

			if ($pos == TRUE AND $pos2 == FALSE) {
				// Codigo buscar votante
				$rempl = array('Cédula:', 'Nombre:', 'Estado:', 'Municipio:', 'Parroquia:', 'Centro:', 'Dirección:', 'SERVICIO ELECTORAL', 'Mesa:');
				$r = trim(str_replace($rempl, '|', limpiarCampo($text)));
				$resource = explode("|", $r);
				$datos = explode(" ", limpiarCampo($resource[2]));
				$datoJson = array('error' => 0, 'nacionalidad' => $nac, 'cedula' => $ci, 'nombres' => $datos[0] . ' ' . $datos[1], 'apellidos' => $datos[2] . ' ' . $datos[3], 'inscrito' => 'SI', 'cvestado' => limpiarCampo($resource[3]), 'cvmunicipio' => limpiarCampo($resource[4]), 'cvparroquia' => limpiarCampo($resource[5]), 'centro' => limpiarCampo($resource[6]), 'direccion' => limpiarCampo($resource[7]));
			} elseif ($pos == FALSE AND $pos2 == FALSE) {
				// Codigo buscar votante
				$rempl = array('Cédula:', 'Primer Nombre:', 'Segundo Nombre:', 'Primer Apellido:', 'Segundo Apellido:', 'ESTATUS');
				$r = trim(str_replace($rempl, '|', $text));
				$resource = explode("|", $r);
				$resource=limpiarCampo($resource[count($resource)-1]);
				$nombres=explode("Nombre:",$resource);
				if (isset($nombres[1])){
					$r = explode("Estado", $nombres[1]);
					$nombres=$r[0];
				}


				$datoJson = array('error' => 0, 'nacionalidad' => $nac, 'cedula' => $ci, 'nombres' => $nombres, 'inscrito' => 'NO');
				if (strpos($nombres, 'Esta cédula de identidad') !== false) {
					$datoJson = array('error' => 1, 'nacionalidad' => $nac, 'cedula' => $ci, 'nombres' => NULL, 'apellidos' => NULL, 'inscrito' => 'NO');
				}
			} elseif ($pos == FALSE AND $pos2 == TRUE) {
				$datoJson = array('error' => 1, 'nacionalidad' => $nac, 'cedula' => $ci, 'nombres' => NULL, 'apellidos' => NULL, 'inscrito' => 'NO');
			}
			return $datoJson;
		} catch (\Exception $e) {
			return null;
		}

	}
}

if (!function_exists('limpiarCampo')) {

	function limpiarCampo($valor) {
		$rempl = array('\n', '\t');
		$r = trim(str_replace($rempl, ' ', $valor));
		return str_replace("\r", "", str_replace("\n", "", str_replace("\t", "", $r)));
	}

}

if (!function_exists('calculateRecommendedStudy')) {

	function calculateRecommendedStudy($minimumStudyUnit,$studyUser) {

		$array=[
			"Primaria",
			"Secundaria",
			"Técnico medio",
			"Programa Nacional de Aprendizaje(PNA)",
			"Técnico Superior Universitario (TSU)",
			"Universitaria (pregrado)",
			"Postgrado-Especialización",
			"Postgrado-Maestría",
			"Postgrado-Doctorado",
		];
		$minimumStudyValue=0;
		$studyUserValue=0;
		foreach($array as $key => $val){
			if($val==$minimumStudyUnit){
				$minimumStudyValue=$key;
			}
			if($val==$studyUser){
				$studyUserValue=$key;
			}
		}

		if($studyUserValue>=$minimumStudyValue){
			return true;
		}else{
			return false;
		}
	}

}

if (!function_exists('geUrl')) {

	function geUrl($url) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // almacene en una variable
		curl_setopt($curl, CURLOPT_HEADER, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		if (curl_exec($curl) === false) {
			echo 'Curl error: ' . curl_error($curl);
		} else {
			$return = curl_exec($curl);
		}
		curl_close($curl);

		return $return;
	}

}
