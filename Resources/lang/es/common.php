<?php
return [
  'title'=>[
    'preselections'=>'Preselecciones'
  ],
  "settings"=>[
    'percentage_studies'=>'Porcentaje por estudios',
    'percentage_work_experience'=>'Porcentaje por experiencia laboral',
    'percentage_address'=>'Porcentaje por dirección',
    "doctor_email"=>"Correo electrónico del doctor",
    "laboratory_address"=>"Dirección del laboratorio"
  ]
];
 ?>
