<?php

return [
    'list resource' => 'Solvencias en proceso',
    'list resource generate' => 'Solvencias generadas',
    'create resource' => 'Crear solvencias',
    'edit resource' => 'Editar solvencias',
    'edit resource generate' => 'Editar solvencias',
    'destroy resource' => 'Eliminar solvencias',
    'title' => [
        'solvencies' => 'Solvencias en proceso',
        'solvenciesGenerate' => 'Solvencias generadas',
        'create solvency' => 'Crear solvencias',
        'edit solvency' => 'Editar solvencias',
        'edit solvenciesGenerate' => 'Editar solvencias generadas',
    ],
    'button' => [
        'create solvency' => 'Crear solvencia',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
