<?php

return [
    'list resource' => 'Lista de aspirantes',
    'list resource doctor' => 'Aspirantes con examen médico',
    'list resource workers' => 'Listado de trabajadores',
    'list resource retiredWorkers' => 'Listado de egresos',
    'create resource' => 'Crear aspirantes',
    'edit resource' => 'Editar aspirantes',
    'destroy resource' => 'Borrar aspirantes',
    'title' => [
        'aspirants' => 'Aspirantes',
        'create aspirants' => 'Crear un aspirante',
        'edit aspirants' => 'Editar un aspirante',
        'edit retiredWorkers' => 'Editar trabajador egresado',
    ],
    'button' => [
        'create aspirants' => 'Crear un aspirante',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
