<?php
return [
  'title'=>[
    'preselections'=>'Preselections'
  ],
  "settings"=>[
    'percentage_studies'=>'Studies Percentage',
    'percentage_work_experience'=>'Work Experience Percentage',
    'percentage_address'=>'Address percentage',
    "doctor_email"=>"Doctor email",
    "laboratory_address"=>"Address laboratory"
  ]
];
 ?>
