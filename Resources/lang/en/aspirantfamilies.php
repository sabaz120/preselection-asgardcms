<?php

return [
    'list resource' => 'List aspirantfamilies',
    'create resource' => 'Create aspirantfamilies',
    'edit resource' => 'Edit aspirantfamilies',
    'destroy resource' => 'Destroy aspirantfamilies',
    'title' => [
        'aspirantfamilies' => 'AspirantFamily',
        'create aspirantfamily' => 'Create a aspirantfamily',
        'edit aspirantfamily' => 'Edit a aspirantfamily',
    ],
    'button' => [
        'create aspirantfamily' => 'Create a aspirantfamily',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
