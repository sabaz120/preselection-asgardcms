<?php

return [
    'list resource' => 'List units',
    'create resource' => 'Create units',
    'edit resource' => 'Edit units',
    'destroy resource' => 'Destroy units',
    'title' => [
        'units' => 'Units',
        'create units' => 'Create a units',
        'edit units' => 'Edit a units',
    ],
    'button' => [
        'create units' => 'Create a units',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
