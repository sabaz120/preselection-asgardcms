<?php

return [
    'list resource' => 'List solvencies',
    'create resource' => 'Create solvencies',
    'edit resource' => 'Edit solvencies',
    'destroy resource' => 'Destroy solvencies',
    'title' => [
        'solvencies' => 'Solvency',
        'create solvency' => 'Create a solvency',
        'edit solvency' => 'Edit a solvency',
    ],
    'button' => [
        'create solvency' => 'Create a solvency',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
