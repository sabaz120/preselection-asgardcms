<?php

return [
    'list resource' => 'List aspirants',
    'list resource doctor' => 'List aspirants',
    'list resource workers' => 'List workers',
    'create resource' => 'Create aspirants',
    'edit resource' => 'Edit aspirants',
    'destroy resource' => 'Destroy aspirants',
    'title' => [
        'aspirants' => 'Aspirants',
        'retiredWorkers' => 'Listado de trabajadores egresados',
        'create aspirants' => 'Create a aspirants',
        'edit aspirants' => 'Edit a aspirants',
    ],
    'button' => [
        'create aspirants' => 'Create a aspirants',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
