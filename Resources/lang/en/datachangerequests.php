<?php

return [
    'list resource' => 'List datachangerequests',
    'create resource' => 'Create datachangerequests',
    'edit resource' => 'Edit datachangerequests',
    'destroy resource' => 'Destroy datachangerequests',
    'title' => [
        'datachangerequests' => 'DataChangeRequest',
        'create datachangerequest' => 'Create a datachangerequest',
        'edit datachangerequest' => 'Edit a datachangerequest',
    ],
    'button' => [
        'create datachangerequest' => 'Create a datachangerequest',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
