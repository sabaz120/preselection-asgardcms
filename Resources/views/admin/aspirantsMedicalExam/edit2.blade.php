@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('preselection::aspirants.title.edit aspirants') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.preselection.aspirants.index') }}">{{ trans('preselection::aspirants.title.aspirants') }}</a></li>
        <li class="active">{{ trans('preselection::aspirants.title.edit aspirants') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.preselection.aspirants.update2', $aspirants->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    @include('preselection::admin.aspirantsMedicalExam.partials.edit-fields2')

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.preselection.aspirants.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        function updateWork(id){
            if(confirm("¿Esta seguro de validar esta experiencia laboral?")){
              $.ajax({
                  url:"{{url('/')}}"+'/workExperience/'+id,
                  type:'POST',
                  headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
                  dataType:"json",
                  data:{},
                  success:function(result){
                      alert(result.msg);
                  },
                  error:function(error){
                      console.log(error);
                  }
              });//ajax
            }else {
              console.log('falso')
              return false;
            }

        }//updateWork()
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.preselection.aspirants.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
