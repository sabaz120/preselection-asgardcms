@extends('layouts.master')

@section('content-header')
<link rel="stylesheet" href="{{url('modules/preselection/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('modules/preselection/css/buttons.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('modules/preselection/css/bootstrap-select.min.css')}}">


<h1>
  {{ trans('preselection::aspirants.title.retiredWorkers') }}
</h1>
<ol class="breadcrumb">
  <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
  <li class="active">{{ trans('preselection::aspirants.title.aspirants') }}</li>
</ol>
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
<div class="box box-primary">
  <div class="box-header">
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="table-responsive">
      <table id="tableAspirants" class="data-table table table-bordered table-hover">
        <thead>
          <tr>
            <th>Cédula</th>
            <th>Nombre</th>
            <th>Correo electrónico</th>
            <th>Parroquia</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>{{ trans('core::core.table.created at') }}</th>
            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($aspirants as $items)
          <tr>
            <td>{{$items->identification}}</td>
            <td>{{$items->name}} {{$items->last_name}}</td>
            <td>{{$items->email}}</td>
            <td>{{$items->parish->name}}</td>
            <td>{{$items->business->name}}</td>
            <td>{{$items->unit->name}}</td>
            <td>{{ $items->created_at }}</td>
            <td>
              <div class="btn-group">
                  <a href="{{ route('admin.preselection.aspirants.editRetired', [$items->id]) }}" class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Cédula</th>
            <th>Nombre</th>
            <th>Correo electrónico</th>
            <th>Parroquia</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>{{ trans('core::core.table.created at') }}</th>
            <th>{{ trans('core::core.table.actions') }}</th>
          </tr>
        </tfoot>
      </table>
      <!-- /.box-body -->
    </div>
  </div>
  <!-- /.box -->
</div>
</div>
</div>
@include('core::partials.delete-modal')
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
  <dt><code>c</code></dt>
  <dd>{{ trans('preselection::aspirants.title.create aspirants') }}</dd>
</dl>
@stop

@push('js-stack')
<script src="{{url('modules/preselection/js/dataTables.buttons.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/dataTables.bootstrap.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/jszip.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/pdfmake.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/vfs_fonts.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.html5.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.print.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.colVis.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.bootstrap.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/bootstrap-select.min.js')}}" charset="utf-8"></script>

<script type="text/javascript">
$( document ).ready(function() {
  $(document).keypressAction({
    actions: [
      { key: 'c', route: "<?= route('admin.preselection.aspirants.create') ?>" }
    ]
  });
});
</script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
