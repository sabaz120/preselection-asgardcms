<style media="screen">
textarea {
resize: none !important;
}
</style>
<div class="box-body">
  <div class="container-fluid">
    <div class="row padding-xl ">
      <div class="form-row">
        <div class="form-group">
          <div class="col-lg-4 text-center ">
            <img  src="{{url($aspirants->curriculum_file)}}" alt="{{$aspirants->name}}" style="height:225px;width:225px;border-radius: 14vh;">
            <br>
          </div>
        </div>

      </div>


      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <input type="hidden" name="aspirant_id" value="{{$aspirants->id}}">
            <label for="">Cédula</label>
            <input type="text" class="form-control"  value="{{$aspirants->identification}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->name}}" readonly>
          </div>

        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->second_name}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->last_name}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->second_surname}}" readonly>
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Correo Electronico</label>
            <input type="text" class="form-control"  value="{{$aspirants->email}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Teléfono</label>
            <input type="text" class="form-control"  value="{{$aspirants->phone}}" readonly>
          </div>
        </div>
        <div class="form-group">
          @if($aspirants->status==6)
          <div class="col-md-2">
            <label for="">Cargo</label>
            <select class="form-control" id="unit_id"  name="unit_id">
              <option value="0">Seleccione</option>
            </select>
            <input type="hidden" name="unit_aspirated" id="unit_aspirated" value="{{$aspirants->unit->name}}">
          </div>
          @else
          <div class="col-md-2">
            <label for="">Cargo</label>
            <input type="text" class="form-control" value="{{$aspirants->unit->name}}" readonly>
          </div>
          @endif
        </div>

      </div>

      <div class="form-row">
        <div class="form-group">
          @if($aspirants->cne_query)
          <div class="col-md-8">
            <label for="">Inscrito en CNE: @if($aspirants->cne_data) SI @else NO @endif</label>
            <input type="text" class="form-control" value="@if($aspirants->cne_data) {{$aspirants->cne_data}} @else NO POSEE @endif" readonly>
          </div>
          @endif
        </div>
      </div>
      @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')

      <div class="form-group col-md-offset-7">
        @if($aspirants->identification_document)
        <a href="{{url($aspirants->identification_document)}}" class="btn btn-danger" download="{{$aspirants->identification}}" style="margin-top:20px">Descargar C.I</a>
        @endif
        @if($aspirants->passport_document)
        <a href="{{url($aspirants->passport_document)}}" class="btn btn-danger" download="{{$aspirants->passport_document}}" style="margin-top:20px">Descargar RIF</a>
        @endif
      </div>


      @endif
      <br><br>
      <div class="form-group col-md-12">
        <br>
        <label for="">Estado del trabajador</label>
        <select class="form-control" name="status" id="status">
            <option @if($aspirants->status==10) selected @endif value="10">Contratado</option>
            <option @if($aspirants->status==11) selected @endif value="11">Liquidado</option>
        </select>
      </div>


      <div class="col-md-12 row text-center">
        <div class="col-md-12 text-center">
          <h3>Datos de dirección</h3>
        </div>

        <div class="col-md-4">
          <label for="">Estado</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->state->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Ciudad</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->city->city}}">
        </div>

        <div class="col-md-4">
          <label for="">Municipio</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Parroquia</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->name}}">
        </div>
        <div class="col-md-8">
          <label for="">Dirección</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->address}}">
        </div>
      </div>


      <div class="text-center col-md-12 row">
        <div class="col-md-6">
          <h3>Datos de estudio</h3>
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nivel de Instrucción</th>
                <th scope="col">Institución</th>
                <th scope="col">Titulo</th>
                <th scope="col">Culminación</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->studies as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->type_education}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->date_end}}</td>
                <td>{{$item->city}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->type_education}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--col md 6-->

        <div class="col-md-6">
          <h3>Datos de formación y capacitación</h3>
          @if(count($aspirants->courses)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Institución</th>
                <th scope="col">Duración</th>
                <th scope="col">Fecha</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->courses as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->hours}}</td>
                <td>{{$item->date}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>No posee información acerca de sus cursos</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->
      </div>

      <div class="text-center col-md-12 row">
        <div class="col-md-12">
          <h3>Experiencia laboral</h3>
          @if(count($aspirants->workExperience)>0)
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Empresa</th>
                <th scope="col">Dirección</th>
                <th scope="col">Tel.Empresa	</th>
                <th scope="col">Contacto</th>
                <th scope="col">Cargo</th>
                <th scope="col">Des. de la funciones desempeñadas</th>
                <th scope="col">Desde</th>
                <th scope="col">Hasta</th>
                <th scope="col">Ultimo salario</th>
                <th scope="col">Motivo de terminación</th>
                <th scope="col">Comprobante</th>
                <th scope="col">Validado</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->workExperience as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->business_name}}</td>
                <td>{{$item->business_address}}</td>
                <td>{{$item->business_phone}}</td>
                <td>{{$item->business_contact_person}}</td>
                <td>{{$item->job_title}}</td>
                <td>{{$item->description_functions_performed}}</td>
                <td>{{$item->since}}</td>
                <td>{{$item->until}}</td>
                <td>{{$item->last_salary}}</td>
                <td>{{$item->reason_for_termination}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->business_name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
                <td class="text-center">
                  @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')
                    <input type="checkbox" onchange="updateWork({{$item->id}})" @if($item->confirmed) checked @endif>
                  @endif
                </input>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @else
        <table class="table table-bordered">
          <td colspan="5" class="text-center"> <strong>No posee información acerca de su experiencia laboral</strong> </td>
        </table>
        @endif
      </div><!--col md 6-->

      <div class="row">
        <div class="col-md-6">
          <h3>Habilidades</h3>
          @if(is_array($aspirants->options) && count($aspirants->options)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->options as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>No ingreso habilidades</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->

        <div class="col-md-6">
          <h3>Carga familiar</h3>
          @if(count($aspirants->families)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombres</th>
                <th>Apellidos</th>
                <th>Cédula de identidad</th>
                <th>Fecha de nacimiento</th>
                <th>Parentesco</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->families as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->last_name}}</td>
                <td>{{$item->identification}}</td>
                <td>{{$item->birthday}}</td>
                <td>{{$item->relationship}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>Aun no ha registrado su carga familiar</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->
      </div>


    </div>
  </div>

  <!-- Modal Egreso -->
  <div class="modal fade" id="modalContratado" tabindex="-1" role="dialog" aria-labelledby="modalContratadoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content modal-lg">
        <div class="modal-header" style="background-color:#dd4b39; color:#fff">
          <h2 class="modal-title text-center" id="exampleModalLabel">Reintegrar trabajador</h2>
          <small>Recuerde que para poder reintegrar un trabajador debe ser autorizada por la Presidencia, Vicepresidencia o dirección general, de lo contrario no procede el movimiento.</small>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="">Seleccione la empresa</label>
              <select class="form-control" name="business_id" id="business_id" onchange="getUnit()">
              </select>
            </div>

            <div class="form-group col-md-6">
              <label for="">Seleccione el cargo</label>
              <select class="form-control" name="unity_id" id="unity_id" >
                <option value="0">Seleccione el cargo</option>

              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> -->
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

</div>



</div>

@push('scriptsBackend')
<script type="text/javascript">
$( "#status" ).change(function() {
  var status = $( "#status" ).val();
  if (status == 10) {
    $("#modalContratado").modal("show");
  }
  });

  function getBusiness(){
    $.ajax({
      url:"{{route('business.businesses')}}",
      type:'GET',
      headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
      dataType:"json",
      data:{state_id:$('#state_id').val()},
      success:function(result){
        var business=result;
        var html="";
        html+='<option value="0">Seleccione la empresas</option>';
        for(var i=0;i<business.length;i++)
          html+='<option value="'+business[i].id+'">'+business[i].name+'</option>';
          $('#business_id').html(html);
      },
      error:function(error){
        console.log(error);
      }
    });//ajax
  }

  function getUnit(){
    var business_id=  $('#business_id').val();
    $.ajax({
      url:"{{url('unitBusiness')}}"+'/'+business_id,
      type:'GET',
      headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
      dataType:"json",
      data:{},
      success:function(result){
        var unit=result;
        console.log(unit);
        var html="";
        html+='<option value="0">Seleccione el cargo</option>';
        for(var i=0;i<unit.length;i++)
          html+='<option value="'+unit[i].unit.id+'">'+unit[i].unit.name+'</option>';
          $('#unity_id').html(html);
      },
      error:function(error){
        console.log(error);
      }
    });//ajax
  }


  $(function() {
    getBusiness();
});
</script>
@endpush
