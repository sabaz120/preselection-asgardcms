<div class="box-body">
  <style media="screen">
    .line{
      line-height: 11px !important;
    }
  </style>
  <div class="col-lg-4 text-center col-md-offset-4 ">
    <img  src="{{url($aspirant->curriculum_file)}}"  style="height:225px;width:225px;border-radius: 14vh;">
    <br>
    <h3 class="line">{{$aspirant->name}} {{$aspirant->last_name}}</h3>
    <p class="line">{{$aspirant->unit->name}}</p>
    <p>{{$aspirant->business->name}}</p>
  </div>
  <br><br>
  <div class="col-lg-12 col-md-12">
    <hr><br><br>
    <h3 class="text-center">Datos a cambiar</h3>
    <br><br><br>
    <table  id="tableChange" class="data-table table table-bordered table-hover">
      <thead>
        <tr>
          <th>Campo</th>
          <th>Valor</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($datachangerequests as $item)
          <tr>
            <td>{{$item->field}}</td>
            <td>{{$item->value}}</td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>

        </tr>
      </tfoot>
    </table>
  </div>
</div>
