<div id="implementsBody" class="box-body">
  <div class="row">
    <div class="form-group col-md-6">
      <label for="">Empresa</label>
      <input type="text" name="" value="{{$solvency->aspirant->business->name}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">Cargo</label>
      <input type="text" name="" value="{{$solvency->aspirant->unit->name}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">C.I del trabajador</label>
      <input type="text" name="" value="{{$solvency->aspirant->identification}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">Nombre del trabajador</label>
      <input type="text" name="" value="{{$solvency->aspirant->name}} {{$solvency->aspirant->last_name}}" class="form-control" disabled>
    </div>
  </div>
  <br><br>

@if($solvency->status_payment == 0)
<hr>
<input type="hidden" name="status_payment" value="1">
<input type="hidden" name="auth_payment_generator_id" value="{{Auth::user()->id}}">
<div class="text-center">
  <label for="" class="text-center">Generar Pago</label>
</div>

  <div class="form-row">
    <input type="hidden" name="date_payment_generate" value="{{date('Y-m-d')}}" class="form-control">
    <div class="form-group col-md-6">
      <label for="">Tipo de pago</label>
      <select class="form-control" name="type_payment" id="type_payment" required>
        <option value="0">Seleccione</option>
        <option value="1">Transferencia</option>
        <option value="2">Efectivo</option>
        <option value="3">Cheque</option>
        <option value="4">Otros</option>
      </select>
    </div>

    <div class="form-group col-md-6" id="amount">
      <label for="">Monto</label>
      <input type="number" name="settlement_amount" class="form-control" placeholder="0.000">
    </div>

    <div class="form-group col-md-6" id="bank_name_div">
      <label>Ingrese el banco con que desea realizar el pago</label>
      <input type="text" name="bank_name" class="form-control">
    </div>


    <div class="form-group col-md-6" id="payment_observation_div">
      <label for="">Observación del pago</label>
      <textarea name="payment_observation" rows="4" cols="80" class="form-control"></textarea>
    </div>
  </div>
@elseif($solvency->status_payment == 1)
<input type="hidden" name="status_payment" value="2">
<input type="hidden" name="auth_payer_id" value="{{Auth::user()->id}}">
<div class="text-center">
  <label for="" class="text-center">Realizar Pago</label>
</div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="">Fecha en que se generó el pago</label>
      <input type="text" name="" value="{{$solvency->date_payment_generate}} - {{$solvency->authPaymentGenerator->first_name}} {{$solvency->authPaymentGenerator->last_name}}" disabled class="form-control">
    </div>
    <div class="form-group col-md-6">
      <label for="">Tipo de pago</label>
      @if($solvency->type_payment ==1)
        <input type="text" class="form-control" value="Transferencia" readonly>
      @elseif($solvency->type_payment ==2)
        <input type="text" class="form-control" value="Efectivo" readonly>
      @elsif($solvency->type_payment ==3)
        <input type="text" class="form-control" value="Cheque" readonly>
      @elseif($solvency->type_payment ==4)
        <input type="text" class="form-control" value="Otro" readonly>
      @endif
    </div>

    <div class="form-group col-md-6">
      <label for="">¿Quien entrego el pago?</label>
      <select class="form-control" name="payer_id" required>
        <option value="0">Seleccione</option>
        @foreach($humanTalentCoordinator as $coord)
          <option value="{{$coord->id}}">{{$coord->name}} {{$coord->last_name}} - {{$coord->unit->name}} </option>
        @endforeach
      </select>
    </div>

    <div class="form-group col-md-6">
      <label for="">Fecha de entrega del pago </label>
      <input type="date" class="form-control" name="date_payment_delivered">
    </div>

  @if($solvency->type_payment ==1)
    <div id="bank_info">
      <div class="form-group col-sm-6">
        <label>Emitido por:</label>
      <input type="text" name="bank_name" readonly value="{{$solvency->bank_name}}" class="form-control">        
      </div>

      <div class="form-group col-sm-6">
        <label>Indique la referencia bancarias:</label>
      <input type="text" name="bank_reference"  class="form-control">        
      </div>
      
    </div>
    @endif

    @if($solvency->settlement_amount !=0)
    <div class="form-group col-md-4">
      <label for="">Monto</label>
      <input type="number" name="settlement_amount" class="form-control" value="{{$solvency->settlement_amount}}" readonly>
    </div>
    @elseif($solvency->payment_observation !=null)
    <div class="form-group col-md-12">
      <label for="">Observación del pago</label>
      <textarea name="name" rows="4" cols="80" class="form-control" disabled>{{$solvency->payment_observation}}</textarea>
    </div>
    @endif



    <!-- <div class="form-group col-md-12">
      <label for="">Observación</label>
      <textarea name="name" rows="4" cols="80" class="form-control"></textarea>
    </div> -->
  </div>
@elseif($solvency->status_payment == 2)
<div class="text-center">
  <h3 for="" class="text-center">Pago generado</h3>
</div>
<div class="form-row">
  <div class="form-group col-md-6">
    <label for="">Tipo de pago</label>
    @if($solvency->type_payment ==1)
      <input type="text" class="form-control" value="Transferencia" readonly>
    @elseif($solvency->type_payment ==2)
      <input type="text" class="form-control" value="Efectivo" readonly>
    @elsif($solvency->type_payment ==3)
      <input type="text" class="form-control" value="Cheque" readonly>
    @elseif($solvency->type_payment ==4)
      <input type="text" class="form-control" value="Otro" readonly>
    @endif
  </div>

  <div class="form-group col-md-6" id="m">
    <label for="">Monto</label>
    <input type="number" name="settlement_amount" class="form-control" value="{{$solvency->settlement_amount}}" readonly>
  </div>



  @if(Auth::user()->roles->first()->slug=='admin')


    <div class="form-group col-md-6">
        <label for="">Fecha en que se generó el pago</label>
        <input type="text" class="form-control" value="{{$solvency->date_payment_generate}}" readonly>
    </div>
    <div class="form-group col-md-6">
        <label for="">Fecha en que se entregó el pago</label>
        <input type="text" class="form-control" value="{{$solvency->date_payment_delivered}}" readonly>
    </div>
    <div class="form-group col-md-6">
        <label for="">¿Quien generó el pago?</label>
        <input type="text" class="form-control" value="{{$solvency->authPaymentGenerator->first_name}} {{$solvency->authPaymentGenerator->last_name}}" readonly>
    </div>

  @endif
  <div class="form-group col-md-6">
    <label for="">¿Quien entrego el pago? </label>
    <input type="text" class="form-control" value="{{$solvency->payer->name}} {{$solvency->payer->last_name}} - {{$solvency->payer->identification}}" readonly>
  </div>

   @if($solvency->type_payment ==1)
    <div class="form-group col-sm-6">
      <label>Banco emisor</label>
      <input type="text" class="form-control" readonly value="{{$solvency->bank_name}} ">
    </div>

    <div class="form-group col-sm-6">
      <label>Referencia</label>
      <input type="text" class="form-control" readonly value="{{$solvency->bank_reference}} ">
    </div>
    @endif



@endif

  <br><br>



</div>

<script type="text/javascript">
$( "#type_payment" ).change(function() {
  var type_payment = $( "#type_payment" ).val();
  if (type_payment == 4) {
    $( "#payment_observation_div" ).show();
    $( "#amount" ).hide();
  }else if(type_payment ==1){
    $( "#bank_name_div" ).show();
  }
  });

  $( document ).ready(function() {
    $( "#payment_observation_div" ).hide();
    $( "#bank_name_div" ).hide();
  });
</script>
