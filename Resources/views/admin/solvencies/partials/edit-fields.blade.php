<style media="screen">
textarea {
resize: none;
}
</style>
<div id="implementsBody" class="box-body">
  <div class="row">
    <div class="form-group col-md-6">
      <label for="">Empresa</label>
      <input type="text" name="" value="{{$solvency->aspirant->business->name}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">Cargo</label>
      <input type="text" name="" value="{{$solvency->aspirant->unit->name}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">C.I del trabajador</label>
      <input type="text" name="" value="{{$solvency->aspirant->identification}}" class="form-control" disabled>
    </div>

    <div class="form-group col-md-6">
      <label for="">Nombre del trabajador</label>
      <input type="text" name="" value="{{$solvency->aspirant->name}} {{$solvency->aspirant->last_name}}" class="form-control" disabled>
    </div>
  </div>
  <br><br>
  <div class="row">
    <hr>
    <div class="text-center">
      <label for="">¿Quien recibe los implementos?</label>
    </div>
    <br>
    <div class="" v-if="solvency.implement_attendant_id ==null">
      <div class="col-md-6">
        <label for="">Seleccione el cargo</label>
        <select class="form-control" v-model="unity_id" @change="getWorkers()">
          <option value="0">Seleccione</option>
          <option v-for="item in unit" v-bind:value="item.unit_id">
            @{{ item.unit.name }}
          </option>
        </select>
      </div>

      <div class="col-md-6">
        <label for="">Seleccione el trabajador</label>
        <select class="form-control" v-model="implement_attendant_id" name="implement_attendant_id">
          <option value="0">Seleccione</option>
          <option v-for="item in workers" v-bind:value="item.id">
            @{{ item.name }} @{{ item.last_name }}
          </option>
        </select>
      </div>
    </div>

    

    <div class="col-md-6">
      <br><br>
      <label for="" class="text-center">Implementos asignados</label> <br>
      <table  class="table table-bordered">
        <thead>
          <tr>
            <th>Implemento</th>
            <th>Acción</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(implements,index) in solvency_implements">
            <td>@{{implements.name}}</td>
            <td>
              <input type="checkbox" v-model="implements.status" true-value="1" false-value="0">
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-md-6" v-if="medical_exam_date>=29">
      <br>
      <div class="" v-if="solvency.medical_exam_voucher == null">
        <input type="hidden" name="medical_exam_date" :value="medical_exam_date">
        <label for="">Adjuntar comprobante de examen médico</label>
        <input type="file" name="medical_exam_voucher">
      </div>
    </div>

    <br><br><br>

    <div class="col-md-6" v-else>
      <label for="">El trabajador no cumple con las condiciones para realizarse el examen médico</label>
    </div>

  </div>
  <br><br>
  <div class="form-row">
    <hr>
    <input type="hidden" name="implements" id="solvency" :value="JSON.stringify(solvency_implements)" class="form-control ">

    <div class="form-group col-md-6">
      <label for="">Motivo de egreso</label>
      <textarea name="name" rows="4" cols="60" class="form-control" disabled>{{$solvency->reason}}</textarea>
    </div>

    <div class="form-group col-md-6">
      <label for="">Observación</label>
      <textarea name="name" rows="4" cols="60" class="form-control" disabled>{{$solvency->observation}}</textarea>
    </div>
  </div>
</div>


<script src="{{url('modules/preselection/js/vue.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/axios.min.js')}}" charset="utf-8"></script>
<script type="text/javascript">
const app=new Vue({
  el:"#implementsBody",
  data:{
    solvency_implements: {!! $solvency->aspirant->implements !!},
    solvency: {!! $solvency !!},
    medical_exam_date:'',
    business_id: {!! $solvency->aspirant->business_id !!},
    unit:[],
    workers:[],
    unity_id:0,
    implement_attendant_id:0
  },
  mounted(){
    this.getUnit();

  },
  methods:{
    getUnit(){
      axios.get("{{url('unitBusiness')}}"+'/'+this.business_id).then(response=>{
        this.unit=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },


    getWorkers: function(){
      axios.post("{{ route('admin.preselection.aspirants.getWorkers')}}",
      {
        unity_id:this.unity_id
      }).then(response => {
        this.workers=response.data;
      }).catch(error => {
        console.log(error);
      });
    },
  }
});
</script>
