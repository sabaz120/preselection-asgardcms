@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('preselection::solvencies.title.edit solvenciesGenerate') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.preselection.solvency.index') }}">{{ trans('preselection::solvencies.title.solvenciesGenerate') }}</a></li>
        <li class="active">{{ trans('preselection::solvencies.title.edit solvenciesGenerate') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.preselection.solvency.updateGenerated', $solvency->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('preselection::admin.solvencies.partials.edit-fields-generated', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.preselection.solvency.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')

    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.preselection.solvency.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
<!--
    <script type="text/javascript">
      var solvency={!! $solvency->aspirant->implements !!};
      function loadItemsTable(){
        var html2="";
        for(var i=0;i<solvency.length;i++){
          html2+="<tr>";
          html2+="<td>";
          html2+= solvency[i].name;
          html2+="</td>";
          html2+="<td>";
          if (solvency[i].status == 0) {
            html2+='<input type="checkbox" name="status" >';
          }else{
            html2+='<input type="checkbox" name="status" checked>';
          }
          html2+="</td>";
          html2+="</tr>";
          }//for

          $('#implements tbody').html(html2);

        }

        $( document ).ready(function() {
          loadItemsTable();
        });
    </script> -->




@endpush
