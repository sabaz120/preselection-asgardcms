@extends('layouts.master')

@section('content-header')
<style media="screen">
.badge-danger {
  background-color: #dc3545 !important;
}
.badge-primary {
  background-color: #007bff !important;
}
.badge-success {
  background-color: #28a745 !important;
}
</style>
    <h1>
        {{ trans('preselection::solvencies.title.solvenciesGenerate') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('preselection::solvencies.title.solvencies') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <!-- <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.preselection.solvency.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('preselection::solvencies.button.create solvency') }}
                    </a>
                </div>
            </div> -->
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Cargo</th>
                                <th>C.I</th>
                                <th>Nombre</th>
                                <th>Estado de solvencia</th>
                                <th>Estado del pago</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($solvencies)): ?>
                            <?php foreach ($solvencies as $solvency): ?>
                            <tr>
                              <td>{{$solvency->aspirant->business->name}}</td>
                              <td>{{$solvency->aspirant->unit->name}}</td>
                              <td>{{$solvency->aspirant->identification}}</td>
                              <td>{{$solvency->aspirant->name}} {{$solvency->aspirant->last_name}}</td>
                              <td>
                                @if($solvency->status == 0)
                                  En proceso
                                @elseif($solvency->status ==1)
                                 Generada
                                @endif
                              </td>
                              <td>
                                @if($solvency->status_payment == 0)
                                <label class="badge badge-danger">Pendiente</label>
                                @elseif($solvency->status_payment ==1)
                                 <label class="badge badge-primary">Generado</label>
                                @elseif($solvency->status_payment ==2)
                                  <label class="badge badge-success">Liquidado</label>
                                @endif
                              </td>
                                <td>
                                    <a href="{{ route('admin.preselection.solvency.edit', [$solvency->id]) }}">
                                        {{ $solvency->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.preselection.solvency.editSolvencyGenerated', [$solvency->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.preselection.solvency.destroy', [$solvency->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>Empresa</th>
                              <th>Cargo</th>
                              <th>C.I</th>
                              <th>Nombre</th>
                              <th>Estado de solvencia</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('preselection::solvencies.title.create solvency') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.preselection.solvency.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
