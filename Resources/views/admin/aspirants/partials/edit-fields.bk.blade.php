<div class="box-body">
  <div class="container-fluid">
    <div class="row padding-xl ">
      <div class="form-row">
        <div class="form-group">
          <div class="col-lg-4 text-center ">
            <img  src="{{url($aspirants->curriculum_file)}}" alt="{{$aspirants->name}}" style="height:225px;width:225px;border-radius: 14vh;">
            <br>
            <label for="">Porcentaje de recomendado</label>
            <table class="table table-bordered table-shape">
              <thead>
                <tr>
                  <th scope="col">N. De Instrucción</th>
                  <th scope="col">Experiencia Laboral</th>
                  <th scope="col"> Parroquia</th>
                  <th scope="col">Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$aspirants->percentage_studies}} %</td>
                  <td>{{$aspirants->percentage_work_experience}} %</td>
                  <td>{{$aspirants->percentage_address}} %</td>
                  <td>{{$aspirants->percentage_studies+$aspirants->percentage_work_experience+$aspirants->percentage_address}}%</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>

      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Cédula</label>
            <input type="text" class="form-control"  value="{{$aspirants->identification}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->name}}" readonly>
          </div>

        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->second_name}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->last_name}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->second_surname}}" readonly>
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Correo Electronico</label>
            <input type="text" class="form-control"  value="{{$aspirants->email}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Teléfono</label>
            <input type="text" class="form-control"  value="{{$aspirants->phone}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Cargo</label>
            <input type="text" class="form-control" value="{{$aspirants->unit->name}}" readonly>
          </div>
        </div>

      </div>

      <div class="form-row">
        <div class="form-group">
          @if($aspirants->cne_query)
          <div class="col-md-8">
            <label for="">Inscrito en CNE: @if($aspirants->cne_data) SI @else NO @endif</label>
            <input type="text" class="form-control" value="@if($aspirants->cne_data) {{$aspirants->cne_data}} @else NO POSEE @endif" readonly>
          </div>
          @endif
        </div>

        <div class="form-group">
          <div class="col-md-8" style="padding-top:15px;">
          <strong>  Antecedentes penales: </strong> (@if($aspirants->criminal_record) Verificado @else Sin verificar @endif)
            <select class="form-control" @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia') disabled @else name="criminal_record" @endif >
              <option value="0" @if(!$aspirants->criminal_record) selected @endif >Con antecedentes</option>
              <option value="1" @if($aspirants->criminal_record) selected @endif >Sin antecedentes</option>
            </select>
          </div>
        </div>
      </div>
      @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')

      <div class="form-group col-md-offset-7">
        @if($aspirants->identification_document)
        <a href="{{url($aspirants->identification_document)}}" class="btn btn-danger" download="{{$aspirants->identification}}" style="margin-top:20px">Descargar C.I</a>
        @endif
        @if($aspirants->passport_document)
        <a href="{{url($aspirants->passport_document)}}" class="btn btn-danger" download="{{$aspirants->passport_document}}" style="margin-top:20px">Descargar RIF</a>
        @endif
      </div>

      <div class="col-md-12 row text-center">
        <div class="col-md-12">
          <h3>Proceso del aspirante</h3>
        </div>

        @if($aspirants->status!=4)
        <div class="col-md-6">
          <label>Estado de solicitud</label>
          <select class="form-control" name="status" id="status_id" onchange="validateState()">
            <option @if($aspirants->status==1) selected @endif value="1">Pendiente</option>
            <option @if($aspirants->status==2) selected @endif value="2">Aprobado</option>
            <option @if($aspirants->status==3) selected @endif value="3">Rechazado</option>
            <option @if($aspirants->status==4) selected @endif value="4">Por aprobación de entrevista</option>
            <option @if($aspirants->status==5) selected @endif value="5">En proceso de entrevista</option>

            <option @if($aspirants->status==6) selected @endif value="6">Entrevista aprobada</option>
            <option @if($aspirants->status==7) selected @endif value="7">Entrevista no aprobada</option>
            <option @if($aspirants->status==8) selected @endif value="8">Por aprobar examen médico</option>
            <option @if($aspirants->status==9) selected @endif value="9">En proceso de examen médico</option>
            <option @if($aspirants->status==10) selected @endif value="10">Contratado</option>
          </select>
        </div>
        @if($aspirants->status==5)
        <div class="col-md-6 text-center">
          <a href="{{ route('admin.interviews.interview.edit', [$interview->id]) }}" class="btn btn-default btn-flat">
            Ir al formato de entrevista
          </a>
        </div>
        @endif
        @else
        @if(Auth::user()->roles->first()->slug!='coordinacion-de-talento-humano')
        <input type="hidden" name="status" value="5"></input>
        <input type="hidden" name="interview_id" value="{{$interview->id}}"></input>

        <div class="col-md-12 row">
          <div class="col-md-6">
            <label for="">Seleccione fecha de la entrevista</label>
            <input type="date" required name="date" class="form-control" value="{{$interview->date}}">
          </div>
          <div class="col-md-6">
            <label for="">Seleccione hora de la entrevista</label>
            <input type="time" required name="hour" class="form-control" value="14:00:00">
          </div>
        </div>
        @else
        <span>En proceso de Asignacion de entrevista</span>
        @endif
        @endif

        @if($aspirants->status>=9)
        <div class="col-md-6">
          <label for="">Examén Médico</label>
          <select @if($aspirants->status!=9) disabled @endif class="form-control" name="medical_exam">
            <option value="0" @if(!$aspirants->medical_exam) selected @endif >No Apto</option>
            <option value="1" @if($aspirants->medical_exam) selected @endif >Apto @if($aspirants->medical_exam) {{$aspirants->medical_exam_date->format('d-m-Y')}} @endif</option>
          </select>
        </div>
        @endif

        @if($aspirants->status>=10)
        <div class="col-md-6">
          <label>Inducción realizada</label>
          <select class="form-control" name="induction">
            <option value="0" @if(!$aspirants->induction) selected @endif >No</option>
            <option value="1" @if($aspirants->induction) selected @endif >Sí @if($aspirants->induction) {{$aspirants->induction_date->format('d-m-Y')}} @endif</option>
          </select>
        </div>
        <div class="col-md-6">
          <label>Implementos entregados</label>
          <select class="form-control" name="safety_equipment">
            <option value="0" @if(!$aspirants->safety_equipment) selected @endif >No</option>
            <option value="1" @if($aspirants->safety_equipment) selected @endif >Sí @if($aspirants->safety_equipment) {{$aspirants->safety_equipment_date->format('d-m-Y')}} @endif</option>
          </select>
        </div>
        <div class="col-md-6">
          <label>Carnet</label>
          <select class="form-control" name="identity_card">
            <option value="0" @if(!$aspirants->identity_card) selected @endif >Sin entregar</option>
            <option value="1" @if($aspirants->identity_card) selected @endif >Entregado @if($aspirants->identity_card) {{$aspirants->identity_card_date->format('d-m-Y')}} @endif</option>
          </select>
        </div>
        <div class="col-md-6">
          <label>Contrato de trabajo</label>
          <select class="form-control" name="work_contract">
            <option value="0" @if(!$aspirants->work_contract) selected @endif >Sin entregar</option>
            <option value="1" @if($aspirants->work_contract) selected @endif >Entregado @if($aspirants->work_contract) {{$aspirants->work_contract_date->format('d-m-Y')}} @endif</option>
          </select>
        </div>
        @endif

        <div class="col-md-12"></div>

      </div>

      @endif


      <div class="col-md-12 row text-center">
        <div class="col-md-12 text-center">
          <h3>Datos de dirección</h3>
        </div>

        <div class="col-md-4">
          <label for="">Estado</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->state->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Ciudad</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->city->city}}">
        </div>

        <div class="col-md-4">
          <label for="">Municipio</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Parroquia</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->name}}">
        </div>
        <div class="col-md-8">
          <label for="">Dirección</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->address}}">
        </div>
      </div>


      <div class="text-center col-md-12 row">
        <div class="col-md-6">
          <h3>Datos de estudio</h3>
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nivel de Instrucción</th>
                <th scope="col">Institución</th>
                <th scope="col">Culminación</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->studies as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->type_education}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->date_end}}</td>
                <td>{{$item->city}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->type_education}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--col md 6-->

        <div class="col-md-6">
          <h3>Datos de formación y capacitación</h3>
          @if(count($aspirants->courses)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Institución</th>
                <th scope="col">Duración</th>
                <th scope="col">Fecha</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->courses as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->hours}}</td>
                <td>{{$item->date}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>No posee información acerca de sus cursos</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->
      </div>

      <div class="text-center col-md-12 row">
        <div class="col-md-12">
          <h3>Experiencia laboral</h3>
          @if(count($aspirants->workExperience)>0)
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Empresa</th>
                <th scope="col">Dirección</th>
                <th scope="col">Tel.Empresa	</th>
                <th scope="col">Contacto</th>
                <th scope="col">Cargo</th>
                <th scope="col">Des. de la funciones desempeñadas</th>
                <th scope="col">Desde</th>
                <th scope="col">Hasta</th>
                <th scope="col">Ultimo salario</th>
                <th scope="col">Motivo de terminación</th>
                <th scope="col">Comprobante</th>
                <th scope="col">Validado</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->workExperience as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->business_name}}</td>
                <td>{{$item->business_address}}</td>
                <td>{{$item->business_phone}}</td>
                <td>{{$item->business_contact_person}}</td>
                <td>{{$item->job_title}}</td>
                <td>{{$item->description_functions_performed}}</td>
                <td>{{$item->since}}</td>
                <td>{{$item->until}}</td>
                <td>{{$item->last_salary}}</td>
                <td>{{$item->reason_for_termination}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->business_name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
                <td class="text-center">
                  @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')
                    <input type="checkbox" onchange="updateWork({{$item->id}})" @if($item->confirmed) checked @endif>
                  @endif
                </input>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @else
        <table class="table table-bordered">
          <td colspan="5" class="text-center"> <strong>No posee información acerca de sus cursos</strong> </td>
        </table>
        @endif
      </div><!--col md 6-->
      <div class="col-md-12">
        <h3>Habilidades</h3>
        @if(is_array($aspirants->options) && count($aspirants->options)>0)
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
            </tr>
          </thead>
          <tbody>
            @foreach($aspirants->options as $item)
            <tr>
              <th scope="row">{{$loop->iteration}}</th>
              <td>{{$item}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @else
        <table class="table table-bordered">
          <td colspan="5" class="text-center"> <strong>No ingreso habilidades</strong> </td>
        </table>
        @endif
      </div><!--col md 6-->
    </div>
  </div>
</div>
</div>


<!-- MODAL AGENDAR ENTREVISTA -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Día de entrevista</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">La entrevista quedo asignada para:</p>
        <div class="form-row">
          <div class="form-group col-lg-6">
            <label for="">Fecha:</label>
            <!-- <input type="date" name="date" class="form-control"> -->
          </div>

          <div class="form-group col-lg-6">
            <label for="">Hora:</label>
            <!-- <input type="time" name="hour" class="form-control"> -->
          </div>
        </div>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-primary" data-dismiss="modal">Agendar</button>
      </div>
    </div>

  </div>
</div>

@push('scriptsBackend')
<script type="text/javascript">
function validateState(){
  if ($("#status_id" ).val()==2) {
    // $('#myModal').modal('show');
  }
}
</script>
@endpush
