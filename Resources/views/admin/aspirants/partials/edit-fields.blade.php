<style media="screen">
textarea {
resize: none;
}
hr.hr{
    border: 1px solid #eaa29b !important;
}
</style>
<div class="box-body">
  <div class="container-fluid">
    <div class="row padding-xl ">
      <div class="form-row">
        <div class="form-group">
          <div class="col-lg-4 text-center ">
            <img  src="{{url($aspirants->curriculum_file)}}" alt="{{$aspirants->name}}" style="height:225px;width:225px;border-radius: 14vh;">
            <br>
            <label for="">Porcentaje de recomendado</label>
            <table class="table table-bordered table-shape">
              <thead>
                <tr>
                  <th scope="col">N. De Instrucción</th>
                  <th scope="col">Experiencia Laboral</th>
                  <th scope="col"> Parroquia</th>
                  <th scope="col">Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{$aspirants->percentage_studies}} %</td>
                  <td>{{$aspirants->percentage_work_experience}} %</td>
                  <td>{{$aspirants->percentage_address}} %</td>
                  <td>{{$aspirants->percentage_studies+$aspirants->percentage_work_experience+$aspirants->percentage_address}}%</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>

      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Cédula</label>
            <input type="text" class="form-control"  value="{{$aspirants->identification}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->name}}" readonly>
          </div>

        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Nombre</label>
            <input type="text" class="form-control" value="{{$aspirants->second_name}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Primer Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->last_name}}" readonly>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-2">
            <label for="">Segundo Apellido</label>
            <input type="text" class="form-control" value="{{$aspirants->second_surname}}" readonly>
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Correo Electronico</label>
            <input type="text" class="form-control"  value="{{$aspirants->email}}" readonly>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-2">
            <label for="">Teléfono</label>
            <input type="text" class="form-control"  value="{{$aspirants->phone}}" readonly>
          </div>
        </div>
        <div class="form-group">
          @if($aspirants->status==6)
          <div class="col-md-2">
            <label for="">Cargo</label>
            <select class="form-control" id="unit_id"  name="unit_id">
              <option value="0">Seleccione</option>
            </select>
            <input type="hidden" name="unit_aspirated" id="unit_aspirated" value="{{$aspirants->unit->name}}">
          </div>
          @else
          <div class="col-md-2">
            <label for="">Cargo</label>
            <input type="text" class="form-control" value="{{$aspirants->unit->name}}" readonly>
          </div>
          @endif
        </div>

      </div>

      <div class="form-row">
        <div class="form-group">
          @if($aspirants->cne_query)
          <div class="col-md-8">
            <label for="">Inscrito en CNE: @if($aspirants->cne_data) SI @else NO @endif</label>
            <input type="text" class="form-control" value="@if($aspirants->cne_data) {{$aspirants->cne_data}} @else NO POSEE @endif" readonly>
          </div>
          @endif
        </div>

        <div class="form-group">
          <div class="col-md-8" style="padding-top:15px;">
          <strong>  Antecedentes penales: </strong> (@if($aspirants->criminal_record_date) Verificado @else Sin verificar @endif)
            <select class="form-control" @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia') disabled @else name="criminal_record" @endif >
              <option value="0" @if(!$aspirants->criminal_record) selected @endif >Con antecedentes</option>
              <option value="1" @if($aspirants->criminal_record) selected @endif >Sin antecedentes</option>
            </select>
          </div>
        </div>
      </div>
      @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')

      <div class="form-group col-md-offset-7">
        @if($aspirants->identification_document)
        <a href="{{url($aspirants->identification_document)}}" class="btn btn-danger" download="{{$aspirants->identification}}" style="margin-top:20px">Descargar C.I</a>
        @endif
        @if($aspirants->passport_document)
        <a href="{{url($aspirants->passport_document)}}" class="btn btn-danger" download="{{$aspirants->passport_document}}" style="margin-top:20px">Descargar RIF</a>
        @endif
      </div>

      <div class="col-md-12 row text-center">
        <div class="col-md-12">
          <h3>Proceso del aspirante</h3>
        </div>

        @if($aspirants->status==2)
          <!-- Cuando esta aprobado -->
          <div class="col-md-6 text-center col-md-offset-3">
            <label>
              El aspirante esta en proceso de adjuntar los documentos, una vez
              adjunte los documentos requeridos se generará una fecha tentativa de entrevista.
            </label>
          </div>
        @elseif($aspirants->status==4)
          <!-- Cuando esta en proceso de entrevista -->
          <div class="col-md-6 text-center">
            @if(Auth::user()->roles->first()->slug!='coordinacion-de-talento-humano')
              <input type="hidden" name="status" value="5"></input>
              <input type="hidden" name="interview_id" value="{{$interview->id}}"></input>
              <div class="col-md-12 col-md-offset-6 row">
                <div class="col-md-6">
                  <label for="">Seleccione fecha de la entrevista</label>
                  <input type="date" required name="date" class="form-control" value="{{$interview->date}}">
                </div>
                <div class="col-md-6">
                  <label for="">Seleccione hora de la entrevista</label>
                  <input type="time" required name="hour" class="form-control" value="14:00:00">
                </div>
              </div>
            @else
              <span>En proceso de Asignacion de entrevista</span>
            @endif
          </div>
        @elseif($aspirants->status==5)
          @if($interview)
            <div class="col-md-6 text-center">
              <label>
                El aspirante esta pendiente por ser evaluado en su entrevista.
              </label>
            </div>
            <div class="col-md-6 text-center">
              <a href="{{ route('admin.interviews.interview.edit', [$interview->id]) }}" class="btn btn-default btn-flat">
                Ir al formato de entrevista
              </a>
            </div>
          @else
            <div class="col-md-12 text-center">
              <label>
                Este aspirante no tiene una entrevista creada.
              </label>
            </div>
          @endif
        @else
          <div class="col-md-6 ">
            {{-- @if($aspirants->status==6 && $aspirants->criminal_record_date != null && $aspirants->criminal_record == false)--}}

          @if($aspirants->status==6 && $aspirants->criminal_record_date != null && $aspirants->criminal_record == false)
              @if($aspirants->criminal_record == 1)
              <div class="col-md-12 col-md-offset-6"> <label>Para continuar con el proceso de reclutamiento, se deben validar los antecedentes penales del aspirante</label></div>
              @else
              <div class="col-md-12 col-md-offset-6"> <label>Este aspirante posee antecedentes penales</label></div>
              @endif
            @else
            <label>Estado de solicitud</label>
            <select class="form-control" name="status" id="status_id" onchange="validateState()">
              @if($aspirants->status==1 || $aspirants->status==3 || $aspirants->status==7)
                <option @if($aspirants->status==1) selected @endif value="1">Pendiente</option>
                @if($aspirants->status!=7)
                  <option @if($aspirants->status==2) selected @endif value="2">Aprobado</option>
                  <option @if($aspirants->status==3) selected @endif value="3">Rechazado</option>
                @endif
                @if($aspirants->status==7)
                  <option @if($aspirants->status==7) selected @endif value="7">Entrevista no aprobada</option>
                @endif
              @endif
              <!-- <option @if($aspirants->status==4) selected @endif value="4">Por aprobación de entrevista</option> -->
              <!-- <option @if($aspirants->status==5) selected @endif value="5">En proceso de entrevista</option> -->
              @if($aspirants->status==6)
                <!-- Si el aspirante aprobo su entrevista -->
                <option @if($aspirants->status==6) selected @endif value="6">Entrevista aprobada</option>
                <option @if($aspirants->status==8) selected @endif value="8">Por aprobar examen médico</option>
              @endif
              @if($aspirants->status==8)
              <option @if($aspirants->status==8) selected @endif value="8">Por aprobar examen médico</option>
              <option @if($aspirants->status==9) selected @endif value="9">En proceso de examen médico</option>
              @endif
              @if($aspirants->status==9)
              <option @if($aspirants->status==9) selected @endif value="9">En proceso de examen médico</option>
              <option @if($aspirants->status==10) selected @endif value="10">Contratado</option>
              @endif
              @if($aspirants->status==10)
              <option @if($aspirants->status==10) selected @endif value="10">Contratado</option>
              @endif
            </select>
          @endif
          </div>
        @endif

        @if($aspirants->status==8)
          <div class="form-row" id="hideMedicalExam">
            <div class="form-group col-md-3">
              <label for="">Fecha de examen medico</label>
              <input type="date" name="medical_exam_date" id="medical_exam_date"  class="form-control" required>
            </div>

            <div class="form-group col-md-3">
              <label for="">Hora de examen medico</label>
              <input type="time" name="medical_exam_hour" id="medical_exam_hour" required class="form-control">
            </div>
          </div>
        @endif
        @if($aspirants->status==9)
        <!-- Sí el aspirante esta en proceso de examen médico, puede actualizar el checkmod si fue apto o no -->
          <div class="form-row col-md-6">
            <div class="form-group col-md-3">
              <label for="">Examén Médico</label>
              <select @if($aspirants->status!=9) disabled @endif class="form-control" name="medical_exam">
                <option value="0" @if(!$aspirants->medical_exam) selected @endif >No Apto</option>
                <option value="1" @if($aspirants->medical_exam) selected @endif >Apto @if($aspirants->medical_exam) {{$aspirants->medical_exam_date ? $aspirants->medical_exam_date->format('d-m-Y') : ''}} @endif</option>
              </select>
            </div>

            @if($aspirants->medical_exam_date ==null || $aspirants->medical_exam_date  =='')

            <div class="form-group col-md-3">
              <label for="">Fecha de examén médico</label>
              <input type="date" name="medical_exam_date" class="form-control">
            </div>
            @else

            <div class="form-group col-md-3">
              <label for="">Fecha de examén médico</label>
              <input type="text" class="form-control" readonly  value="{{$aspirants->medical_exam_date ? $aspirants->medical_exam_date->format('d-m-Y') : ''}}">
            </div>

            @endif


            <div class="form-group col-md-3">
              <label for="">Nombre del doctor</label>
              <input type="text" name="medical_exam_doctor" required class="form-control" value="{{$aspirants->medical_exam_doctor}}">
            </div>
            @if($aspirants->medical_exam_file ==null || $aspirants->medical_exam_file  =='')
            <div class="form-group col-md-3">
              <label for="">Adjuntar examen médico</label>
              <input type="file" name="medical_exam_file" accept="image/*" required class="form-control">
            </div>
            @else
            <div class="form-group col-md-3">
              <label for="">Examen médico</label>
              <a href="{{url($aspirants->medical_exam_file)}}" download="{{$aspirants->medical_exam_file}}">Descargar examén médico</a>
            </div>
            @endif

          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="">Observación del examen médico</label>
              <textarea name="medical_exam_observation" class="form-control" rows="8" cols="80">{{$aspirants->medical_exam_observation}}</textarea>
            </div>
          </div>
        @endif
        @if($aspirants->status>=10)
        <!-- Si el aspirante es contratado le mostrara estos checkbox -->
          <div class="form-row col-md-6">
            <div id="inductionDiv" class="form-group col-md-4">
                <label>Inducción realizada</label>
                @if($aspirants->induction == 0)
                <select class="form-control" name="induction" id="induction" onchange="showAndHideInduction()" >
                  <option value="0" @if(!$aspirants->induction) selected @endif >No</option>
                  <option value="1" @if($aspirants->induction) selected @endif >Sí @if($aspirants->induction) {{$aspirants->induction_date->format('d-m-Y')}} @endif</option>
                </select>
                @elseif($aspirants->induction ==1)
                <select class="form-control" name="induction" id="induction" disabled>
                  <option value="0" @if(!$aspirants->induction) selected @endif >No</option>
                  <option value="1" @if($aspirants->induction) selected @endif >Entregado el día: @if($aspirants->induction) - {{$aspirants->induction_date->format('d-m-Y')}} - Por: {{$aspirants->induction_attendant}} @endif</option>
                </select>
                @endif
            </div>

            @if(!$aspirants->induction)
            <div class="form-group col-md-4 hideInduction">
                <label for="">Fecha de la inducción</label>
                <input type="date" name="induction_date" class="form-control">

            </div>

            <div class="form-group col-md-4 hideInduction">
                <label for="">¿Quien aplicó la inducción?</label>
                <input type="text" name="induction_attendant" class="form-control" placeholder="Pedro Perez">
            </div>
            @endif
          </div>

          <div class="form-row col-md-6">
            <div id="implementsDiv" class="form-group col-md-4">
              <label>Implementos entregados</label>
              @if($aspirants->safety_equipment == 0)
              <select class="form-control" name="safety_equipment" id="safety_equipment" onchange="showAndHideDivSafety()">
                <option value="0" @if(!$aspirants->safety_equipment) selected @endif >No</option>
                <option value="1" @if($aspirants->safety_equipment) selected @endif >Sí @if($aspirants->safety_equipment) {{$aspirants->safety_equipment_date->format('d-m-Y')}} @endif</option>
              </select>
              @elseif($aspirants->safety_equipment == 1)
              <select class="form-control" name="safety_equipment" id="safety_equipment" disabled>
                <option value="0" @if(!$aspirants->safety_equipment) selected @endif >No</option>
                <option value="1" @if($aspirants->safety_equipment) selected @endif >Entregado el día: @if($aspirants->safety_equipment) - {{$aspirants->safety_equipment_date->format('d-m-Y')}} - Por: {{$aspirants->safety_equipment_attendant}} @endif</option>
              </select>
              @endif
            </div>

              @if(!$aspirants->safety_equipment)
            <div class="form-group col-md-4 hideImplements">
              <label for="">Fecha de entrega de Implementos</label>
              <input type="date" name="safety_equipment_date"  class="form-control">
            </div>

            <div class="form-group col-md-4 hideImplements">
              <label for="">¿Quien entrego los implementos?</label>
              <input type="text" name="safety_equipment_attendant"  class="form-control">
            </div>
            @endif
          </div>

          <div class="form-row col-md-6">
            <div id="carnetDiv" class="form-group col-md-4">
              <label>Carnet</label>
              @if($aspirants->identity_card == 0)
              <select class="form-control" name="identity_card" id="identity_card" onchange="showAndHideIdentity()">
                <option value="0" @if(!$aspirants->identity_card) selected @endif >Sin entregar</option>
                <option value="1" @if($aspirants->identity_card) selected @endif >Entregado @if($aspirants->identity_card) {{$aspirants->identity_card_date->format('d-m-Y')}} @endif</option>
              </select>
              @elseif($aspirants->identity_card == 1)
              <select class="form-control" name="identity_card" id="identity_card" disabled>
                <option value="0" @if(!$aspirants->identity_card) selected @endif >Sin entregar</option>
                <option value="1" @if($aspirants->identity_card) selected @endif >Entregado el día: @if($aspirants->identity_card) - {{$aspirants->identity_card_date->format('d-m-Y')}} - Por: {{$aspirants->identity_card_attendant}} @endif</option>
              </select>
              @endif
            </div>
              @if(!$aspirants->identity_card)
            <div class="form-group col-md-4 hideCarnet" >
              <label for="">Fecha de entrega del carnet</label>
              <input type="date" name="identity_card_date"  class="form-control">
            </div>

            <div class="form-group col-md-4 hideCarnet">
              <label for="">¿Quien entrego el carnet?</label>
              <input type="text" name="identity_card_attendant"  class="form-control">
            </div>
            @endif

          </div>


          <div class="form-row col-md-12">
            <div  id="contractDiv" class="form-group">
              <label>Contrato de trabajo</label>
              @if($aspirants->work_contract == 0)
              <select class="form-control" name="work_contract" id="work_contract" onchange="showAndHideDivWork()">
                <option value="0" @if(!$aspirants->work_contract) selected @endif >Sin entregar</option>
                <option value="1" @if($aspirants->work_contract) selected @endif >Entregado @if($aspirants->work_contract) {{$aspirants->work_contract_date->format('d-m-Y')}} @endif</option>
              </select>
              @elseif($aspirants->work_contract == 1)
              <select class="form-control" name="work_contract" id="work_contract" disabled>
                <option value="0" @if(!$aspirants->work_contract) selected @endif >Sin entregar</option>
                <option value="1" @if($aspirants->work_contract) selected @endif >Entregado el día: @if($aspirants->work_contract) - {{$aspirants->work_contract_date->format('d-m-Y')}} - Por: {{$aspirants->work_contract_attendant}} @endif</option>
              </select>
              @endif
            </div>
            @if(!$aspirants->work_contract)
            <div class="form-group col-md-4 hideContract" >
              <label>Fecha de entrega del contrato</label>
              <input type="date" name="work_contract_date"  class="form-control">
            </div>

            <div class="form-group col-md-4 hideContract">
              <label for="">¿Quien entrego el contrato?</label>
              <input type="text" name="work_contract_attendant"  class="form-control">
            </div>
            @endif
          </div>

        @endif

        <div class="col-md-12"></div>

      </div>

      @endif

      <div class="col-md-12 row text-center" >
        <hr class="hr">
        <div class="col-md-12 text-center">
            <h3>Datos de dirección</h3>
        </div>

        <div class="col-md-4">
          <label for="">Estado</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->state->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Ciudad</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->city->city}}">
        </div>

        <div class="col-md-4">
          <label for="">Municipio</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->municipality->name}}">
        </div>

        <div class="col-md-4">
          <label for="">Parroquia</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->parish->name}}">
        </div>
        <div class="col-md-8">
          <label for="">Dirección</label>
          <input type="text" class="form-control" readonly value="{{$aspirants->address}}">
        </div>


      </div>


      <div class="text-center col-md-12 row">
        <hr class="hr">
        <div class="col-md-6">
          <h3>Datos de estudio</h3>
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nivel de Instrucción</th>
                <th scope="col">Institución</th>
                <th scope="col">Titulo</th>
                <th scope="col">Culminación</th>
                <th scope="col">Ciudad</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->studies as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->type_education}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->date_end}}</td>
                <td>{{$item->city}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->type_education}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div><!--col md 6-->

        <div class="col-md-6">
          <h3>Datos de formación y capacitación</h3>
          @if(count($aspirants->courses)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Institución</th>
                <th scope="col">Duración</th>
                <th scope="col">Fecha</th>
                <th scope="col">Comprobante</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->courses as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->institution}}</td>
                <td>{{$item->hours}}</td>
                <td>{{$item->date}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>No posee información acerca de sus cursos</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->
      </div>

      <div class="text-center col-md-12 row">
        <hr class="hr">
        <div class="col-md-12">
          <h3>Experiencia laboral</h3>
          @if(count($aspirants->workExperience)>0)
          <table class="table table-bordered table-shape">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Empresa</th>
                <th scope="col">Dirección</th>
                <th scope="col">Tel.Empresa	</th>
                <th scope="col">Contacto</th>
                <th scope="col">Cargo</th>
                <th scope="col">Des. de la funciones desempeñadas</th>
                <th scope="col">Desde</th>
                <th scope="col">Hasta</th>
                <th scope="col">Ultimo salario</th>
                <th scope="col">Motivo de terminación</th>
                <th scope="col">Comprobante</th>
                <th scope="col">Validado</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->workExperience as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->business_name}}</td>
                <td>{{$item->business_address}}</td>
                <td>{{$item->business_phone}}</td>
                <td>{{$item->business_contact_person}}</td>
                <td>{{$item->job_title}}</td>
                <td>{{$item->description_functions_performed}}</td>
                <td>{{$item->since}}</td>
                <td>{{$item->until}}</td>
                <td>{{$item->last_salary}}</td>
                <td>{{$item->reason_for_termination}}</td>
                <td>
                  @if($item->document)
                  <a href="{{url($item->document)}}" download="{{$aspirants->identification}}-{{$item->business_name}}">Descargar</a>
                  @else
                  <label for="">Sin archivos adjuntos</label>
                  @endif
                </td>
                <td class="text-center">
                  @if(Auth::user()->roles->first()->slug!='seguridad-y-vigilancia')
                    <input type="checkbox" onchange="updateWork({{$item->id}})" @if($item->confirmed) checked @endif>
                  @endif
                </input>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @else
        <table class="table table-bordered">
          <td colspan="5" class="text-center"> <strong>No posee información acerca de su experiencia laboral</strong> </td>
        </table>
        @endif
        <hr class="hr">
      </div><!--col md 6-->

      <div class="row">
        <hr class="hr">
        <div class="col-md-6">
          <h3>Habilidades</h3>
          @if(is_array($aspirants->options) && count($aspirants->options)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->options as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>No ingreso habilidades</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->

        <div class="col-md-6">
          <h3>Carga familiar</h3>
          @if(count($aspirants->families)>0)
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombres</th>
                <th>Apellidos</th>
                <th>Cédula de identidad</th>
                <th>Fecha de nacimiento</th>
                <th>Parentesco</th>
              </tr>
            </thead>
            <tbody>
              @foreach($aspirants->families as $item)
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->last_name}}</td>
                <td>{{$item->identification}}</td>
                <td>{{$item->birthday}}</td>
                <td>{{$item->relationship}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
          <table class="table table-bordered">
            <td colspan="5" class="text-center"> <strong>Aun no ha registrado su carga familiar</strong> </td>
          </table>
          @endif
        </div><!--col md 6-->

      </div>


    <div class="row">
      <hr class="hr">
      @if($aspirants->status==10)
      <div class="col-md-12">
        <h3>Asignar implementos</h3>
        <div class="form-group col-md-6 col-md-offset-3">
          <input type="hidden" name="implements" id="implements" value="">
          <!-- <label>Por favor ingresa los implementos que van hacer asignados al trabajador</label> -->
          <div class="input-group">
            <input type="text" id="inputImplements" class="form-control">
            <span type="button" onclick="addImplements()" class="input-group-addon btn btn-success">Agregar</span>
          </div>
        </div>

        <div class="col-md-12 text-center table-responsive">
          <table id="tableImplements" class="table table-bordered table-shape">
            <thead>
              <tr>
                <td>#</td>
                <td>Implementos</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
      @endif
    </div>

    </div>
  </div>
</div>
</div>


<!-- MODAL AGENDAR ENTREVISTA -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Día de entrevista</h4>
      </div>
      <div class="modal-body">
        <p class="text-center">La entrevista quedo asignada para:</p>
        <div class="form-row">
          <div class="form-group col-lg-6">
            <label for="">Fecha:</label>
            <!-- <input type="date" name="date" class="form-control"> -->
          </div>

          <div class="form-group col-lg-6">
            <label for="">Hora:</label>
            <!-- <input type="time" name="hour" class="form-control"> -->
          </div>
        </div>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-primary" data-dismiss="modal">Agendar</button>
      </div>
    </div>

  </div>
</div>

@push('scriptsBackend')
<script type="text/javascript">
var implements={!! $implements !!};


function validateState(){
  if ($("#status_id" ).val() ==9) {
    $("#hideMedicalExam").show();
  }else {
    $("#hideMedicalExam").hide();
  }
}
function showAndHideInduction(){
  /***INDUCCIÓN****/
   if ($("#induction" ).val()==1) {
     $(".hideInduction").show();
     $( "#inductionDiv" ).removeClass( "col-md-12" ).addClass( "col-md-4" );
   }else if($("#induction" ).val()==0) {
     $(".hideInduction").hide();
     $( "#inductionDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
   }
}

function addImplements(){
  var html2="";
  var descImplements=$('#inputImplements').val();
  if(descImplements=="")
  alert("Debe escribir el nombre del implemento.");
  implements.push({name:descImplements,status:0});
  $('#inputImplements').val("");
  loadItemsTable();
}//addImplements()

function loadItemsTable(){
  var html2="";
  for(var i=0;i<implements.length;i++){
    html2+="<tr>";
    html2+="<td>";
    html2+=i+1;
    html2+="</td>";
    html2+="<td>";
    html2+= implements[i].name;
    html2+="</td>";
    html2+="<td>";
    html2+='<button type="button" class="btn btn-danger" onclick="deleteImplements('+i+')" name="button"><i class="fa fa-trash"></i></button>';
    html2+="</td>";
    html2+="</tr>";
  }//for
  if(implements.length==0){
    html2+="<tr>";
    html2+="<td colspan='3'>No se han ingresado implementos";
    html2+="</td>";
    html2+="</tr>";
  }
  $('#tableImplements tbody').html(html2);
  $('#implements').val(JSON.stringify(implements));
}

function deleteImplements(index){
  implements.splice(index,1);
  loadItemsTable();
}

function showAndHideDivSafety(){
  /****IMPLEMENTOS DE SEGURIDAD****/
  if ($("#safety_equipment" ).val() == 1 ) {
    $(".hideImplements").show();
    $( "#implementsDiv" ).removeClass( "col-md-12" ).addClass( "col-md-4" );
  }else if ($("#safety_equipment" ).val() ==0) {
    $(".hideImplements").hide();
    $( "#implementsDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
  }
}

function showAndHideIdentity(){
  /*****CARNET*****/
  if ($("#identity_card" ).val() == 1 ) {
    $(".hideCarnet").show();
    $( "#carnetDiv" ).removeClass( "col-md-12" ).addClass( "col-md-4" );
  }else if ($("#identity_card" ).val() == 0) {
    $(".hideCarnet").hide();
    $( "#carnetDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
  }
}

function showAndHideDivWork(){
  if ($("#work_contract" ).val() == 1 ) {
    $(".hideContract").show();
    $( "#contractDiv" ).removeClass( "col-md-12" ).addClass( "col-md-4" );
  }else if ($("#work_contract" ).val() == 0) {
    $(".hideContract").hide();
    $( "#contractDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
  }
}

function getUnitBusiness(){
  var business_id={{$aspirants->business_id}}
  $.ajax({
    url:"{{url('/')}}"+'/unitBusiness/'+business_id,
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{},
    success:function(result){
      var unit=result;
      var unit_id={{$aspirants->unit->id}}
      var html2="";
      for(var i=0;i<unit.length;i++)
        html2+='<option value="'+unit[i].unit.id+'">'+unit[i].unit.name+'</option>';
      $('#unit_id').html(html2);
      if(unit_id!=0)
        $('#unit_id option[value='+unit_id+']').attr('selected','selected');
    },
    error:function(error){
      console.log(error);
    }
  });//ajax




}



$(function() {
    getUnitBusiness();
    loadItemsTable();
    $(".hideInduction").hide();
    $( "#inductionDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
    $(".hideImplements").hide();
    $( "#implementsDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
    $(".hideCarnet").hide();
    $( "#carnetDiv" ).removeClass( "col-md-4" ).addClass( "col-md-12" );
    $(".hideContract").hide();
    $("#hideMedicalExam").hide();
});

</script>
@endpush
