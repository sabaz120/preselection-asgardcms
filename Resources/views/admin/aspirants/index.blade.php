@extends('layouts.master')

@section('content-header')
<link rel="stylesheet" href="{{url('modules/preselection/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('modules/preselection/css/buttons.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('modules/preselection/css/bootstrap-select.min.css')}}">


<h1>
  {{ trans('preselection::aspirants.title.aspirants') }}
</h1>
<ol class="breadcrumb">
  <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
  <li class="active">{{ trans('preselection::aspirants.title.aspirants') }}</li>
</ol>
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
<div class="box box-primary">
  <div class="box-header">
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="form-row">
        <div class="form-group col-lg-4">
          <label >Fecha inicial</label>
          <input type="date" id="date" name="date" class="form-control">
        </div>

        <div class="form-group col-lg-4">
          <label>Fecha Finalizar</label>
          <input type="date" id="date_end" name="date_end" class="form-control">
        </div>

        <div class="form-group col-lg-4">
          <label>Estado de solicitud</label>
          <select class="form-control" id="status_id"  name="status_id">
            <option value="0">Seleccione</option>
            @foreach(preselection__getStatus()->lists() as $key => $val)
            <option value="{{$key}}">{{$val}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group col-lg-6">
          <label>Empresa</label>
          <select class="form-control" id="business_id"  name="business_id" onchange="getUnitBusiness()">
          </select>
        </div>
        <div class="form-group col-lg-6">
          <label>Cargos</label>
          <select class="form-control" id="unit_id"  name="unit_id">
            <option value="0">Seleccione</option>
          </select>
        </div>

        <div class="form-group col-lg-2">
          <label for="">Recomendado por:</label><br>
          <select class="selectpicker" multiple id="recommended_by" name="recommended_by" data-title="Seleccione" >
            <option value="1">Nivel de instrucción</option>
            <option value="2">Experiencia laboral</option>
            <option value="3">Parroquia</option>
          </select>
        </div>

        <div class="form-group col-lg-3">
          <label for="">Estado</label>
          <select class="form-control" name="state_id" id="state_id" onchange="loadMunicipalities()">
            <option value="0">Seleccione un estado</option>
          </select>
        </div>

        <div id="divMunicipalities" class="form-group col-lg-3">
          <label for="">Municipio</label>
          <select class="form-control" name="municipality_id" id="municipality_id" onchange="loadParishes()">
            <option value="0">Seleccione un municipio</option>
          </select>
        </div>

        <div id="divParishes" class="form-group col-lg-3">
          <label for="">Parroquia</label>
          <select class="form-control" name="parish_id" id="parish_id" >
            <option value="0">Seleccione una parroquia</option>
          </select>
        </div>



        <div class="form-group col-lg-12 col-md-offset-6">
          <br>
          <button type="button" name="button" class="btn btn-primary" onclick="getAspirants()">Buscar</button>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="box-header">
      <h3 class="box-title">Lista de aspirantes</h3>
      <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 250px; margin-bottom: 10px">
              <input type="text"  id="search" name="q" class="form-control pull-right" placeholder="Buscar persona por nombre">
              <div class="input-group-btn">
                  <button type="submit" onclick="getAspirants()" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
          </div>
      </div>
  </div>
  <div class="box-body">
    <div class="table-responsive">
      <table id="tableAspirants" class="data-table table table-bordered table-hover">
        <thead>
          <tr>
            <th>Porcentaje</th>
            <th>Cédula</th>
            <th>Aspirante</th>
            <th>Correo electrónico</th>
            <th>Parroquia</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>Estado de solicitud</th>
            <th>{{ trans('core::core.table.created at') }}</th>
            <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
        <tfoot>
          <tr>
            <th>Porcentaje</th>
            <th>Cédula</th>
            <th>Aspirante</th>
            <th>Correo electrónico</th>
            <th>Parroquia</th>
            <th>Empresa</th>
            <th>Cargo</th>
            <th>Estado de solicitud</th>
            <th>{{ trans('core::core.table.created at') }}</th>
            <th>{{ trans('core::core.table.actions') }}</th>
          </tr>
        </tfoot>
      </table>
      <!-- /.box-body -->
    </div>
    <div class="row">
        <div class="col-md-6 text-left" id="footerTable">
          Mostrando 1 registro de 10 de un total de 10 registros
        </div>
        <div class="col-md-6 text-right" id="footerTable">
          <button type="button" onclick="backPage()" class="btn btn-info" name="button">
            Anterior
          </button>
          <button type="button" onclick="nextPage()" class="btn btn-info" name="button">
            Siguiente
          </button>
        </div>
    </div>
  </div>
  <!-- /.box -->
</div>
</div>
</div>
@include('core::partials.delete-modal')
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
  <dt><code>c</code></dt>
  <dd>{{ trans('preselection::aspirants.title.create aspirants') }}</dd>
</dl>
@stop

@push('js-stack')
<script src="{{url('modules/preselection/js/dataTables.buttons.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/dataTables.bootstrap.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/jszip.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/pdfmake.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/vfs_fonts.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.html5.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.print.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.colVis.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/buttons.bootstrap.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/bootstrap-select.min.js')}}" charset="utf-8"></script>


<script type="text/javascript">
$( document ).ready(function() {
  $(document).keypressAction({
    actions: [
      { key: 'c', route: "<?= route('admin.preselection.aspirants.create') ?>" }
    ]
  });
});
</script>
<?php $locale = locale(); ?>
<script type="text/javascript">
var totalPages=0;
var page=1;
var total=0;
var oldSearch="";
function loadFooterHtml(){
  var html="";
  var lastRow=10*page;
  var initialRow=lastRow-10;
  if(initialRow==0)
  initialRow=1;
  html+="Mostrando registros del "+initialRow+" al "+lastRow+" de un total de "+total+" registros";
  $('#footerTable').html(html);
}
function nextPage(){
  if(page+1>totalPages){
    alert("No hay mas páginas disponibles");
  }else{
    page=page+1;
    getAspirants();
  }
}
function backPage(){
  if(page>1){
    page=page-1;
    getAspirants();
  }else
  alert("Ya estas en la página 1");
}
function getAspirants(){
  var filter={
    'status_id':$("#status_id").val(),
    'date':$("#date").val(),
    'date_end':$("#date_end").val(),
    'state_id':$("#state_id").val(),
    'recommended_by':$("#recommended_by").val(),
    'business_id':$("#business_id").val(),
    'unit_id':$("#unit_id").val(),
    page:page
  };
  var search=$('#search').val();
  if(search!=""){
    if(oldSearch!=search){
      page=1;
      oldSearch=search;
    }
    filter.search=search;
  }
  $.ajax({
    url:"{{url('/')}}"+'/backend/preselection/getAspirants2',
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:filter,
    success:function(result){
      var aspirants = result.aspirants.data;
      var paginate = result.paginate;
      console.log("paginate");
      console.log(paginate);
      page=1;
      totalPages=paginate.lastPage;
      page=paginate.currentPage;
      total=paginate.total;
      var html="";
      for(var i=0;i<aspirants.length;i++){
        var total_percentage= aspirants[i].percentage_studies + aspirants[i].percentage_address + aspirants[i].percentage_work_experience;
        html+="<tr>";
        html+='<td>'+total_percentage+'%</td>';
        html+='<td>'+aspirants[i].identification+'</td>';
        var surName="";
        if(aspirants[i].second_surname){
          surname=aspirants[i].second_surname;
        }
        if (aspirants[i].second_name !=null ) {
          html+='<td>'+aspirants[i].name+' '+aspirants[i].second_name+' '+aspirants[i].last_name+' '+surName+ '</td>';
        }else if (aspirants[i].second_surname !=null) {
          html+='<td>'+aspirants[i].name+' '+aspirants[i].second_name+' '+aspirants[i].last_name+' '+surName+ '</td>';
        }else {
           html+='<td>'+aspirants[i].name+' '+aspirants[i].last_name+'</td>';
        }
        html+='<td>'+aspirants[i].email+'</td>';
        html+='<td>'+aspirants[i].parish.name+'</td>';
        html+='<td>'+aspirants[i].business.name+'</td>';
        html+='<td>'+aspirants[i].unit.name+'</td>';
        if (aspirants[i].status==1) {
          html+='<td>Pendiente</td>';
        }else if (aspirants[i].status==2) {
            html+='<td>Aprobado</td>';
        }else if (aspirants[i].status==3) {
          html+='<td>Rechazado</td>';
        }else if (aspirants[i].status==4) {
          html+='<td>Por aprobación de entrevista</td>';
        }else if (aspirants[i].status==5) {
          html+='<td>En proceso de entrevista</td>';
        }else if (aspirants[i].status==6) {
          html+='<td>Entrevista aprobada</td>';
        }else if (aspirants[i].status==7) {
          html+='<td>Entrevista rechazada</td>';
        }else if (aspirants[i].status==8) {
          html+='<td>Por aprobar examen médico</td>';
        }else if (aspirants[i].status==9) {
          html+='<td>En proceso de examen médico</td>';
        }else if (aspirants[i].status==10) {
          html+='<td>Contratado</td>';
        }else if (aspirants[i].status==11) {
          html+='<td>Liquidado</td>';
        }
        html+='<td>'+aspirants[i].created_at+'</td>';
        html+='<td><a href="{{ url('/es/backend/preselection/aspirants/')}}/'+aspirants[i].id+'/edit/" class="btn btn-default btn-flat"> <i class="fa fa-pencil"></i></a> <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ url('/es/backend/preselection/aspirants')}}/'+aspirants[i].id+'"><i class="fa fa-trash"></i> </button> </td>';

        html+="</tr>";
      }//for
      // if ( $.fn.DataTable.isDataTable('#tableAspirants') ) {
      //   $('#tableAspirants').DataTable().destroy();
      // }
      $('#tableAspirants tbody').html(html);
      loadFooterHtml();

      // $('#tableAspirants').DataTable({
      //   dom: 'Bfrtip',
      //   buttons: [
      //       {
      //           extend: 'excelHtml5',
      //           exportOptions: {
      //               columns: ':visible'
      //           }
      //       },
      //       {
      //           extend: 'pdfHtml5',
      //           orientation: 'landscape',
      //           pageSize: 'LEGAL',
      //           exportOptions: {
      //               columns: [ 0, ':visible']
      //           }
      //       },
      //       'colvis'
      //   ],
      //   "paginate": true,
      //   "lengthChange": true,
      //   "filter": true,
      //   "sort": true,
      //   "info": true,
      //   "autoWidth": true,
      //   "order": [[ 8, "desc" ]],
      //   "language": {
      //     "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
      //   }
      // });
    },
    error:function(error){
    }
  });//ajax
}
// function getAspirants(){
//   $.ajax({
//     url:"{{url('/')}}"+'/backend/preselection/getAspirants',
//     type:'GET',
//     headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
//     dataType:"json",
//     data:{
//       'status_id':$("#status_id").val(),
//       'date':$("#date").val(),
//       'date_end':$("#date_end").val(),
//       'state_id':$("#state_id").val(),
//       'recommended_by':$("#recommended_by").val(),
//       'business_id':$("#business_id").val(),
//       'unit_id':$("#unit_id").val(),
//   },
//     success:function(result){
//       var aspirants = result;
//       var html="";
//       for(var i=0;i<aspirants.length;i++){
//         var total_percentage= aspirants[i].percentage_studies + aspirants[i].percentage_address + aspirants[i].percentage_work_experience;
//         html+="<tr>";
//         html+='<td>'+total_percentage+'%</td>';
//         html+='<td>'+aspirants[i].identification+'</td>';
//         var surName="";
//         if(aspirants[i].second_surname){
//           surname=aspirants[i].second_surname;
//         }
//         if (aspirants[i].second_name !=null ) {
//           html+='<td>'+aspirants[i].name+' '+aspirants[i].second_name+' '+aspirants[i].last_name+' '+surName+ '</td>';
//         }else if (aspirants[i].second_surname !=null) {
//           html+='<td>'+aspirants[i].name+' '+aspirants[i].second_name+' '+aspirants[i].last_name+' '+surName+ '</td>';
//         }else {
//            html+='<td>'+aspirants[i].name+' '+aspirants[i].last_name+'</td>';
//         }
//         html+='<td>'+aspirants[i].email+'</td>';
//         html+='<td>'+aspirants[i].parish.name+'</td>';
//         html+='<td>'+aspirants[i].business.name+'</td>';
//         html+='<td>'+aspirants[i].unit.name+'</td>';
//         if (aspirants[i].status==1) {
//           html+='<td>Pendiente</td>';
//         }else if (aspirants[i].status==2) {
//             html+='<td>Aprobado</td>';
//         }else if (aspirants[i].status==3) {
//           html+='<td>Rechazado</td>';
//         }else if (aspirants[i].status==4) {
//           html+='<td>Por aprobación de entrevista</td>';
//         }else if (aspirants[i].status==5) {
//           html+='<td>En proceso de entrevista</td>';
//         }else if (aspirants[i].status==6) {
//           html+='<td>Entrevista aprobada</td>';
//         }else if (aspirants[i].status==7) {
//           html+='<td>Entrevista rechazada</td>';
//         }else if (aspirants[i].status==8) {
//           html+='<td>Por aprobar examen médico</td>';
//         }else if (aspirants[i].status==9) {
//           html+='<td>En proceso de examen médico</td>';
//         }else if (aspirants[i].status==10) {
//           html+='<td>Contratado</td>';
//         }else if (aspirants[i].status==11) {
//           html+='<td>Liquidado</td>';
//         }
//         html+='<td>'+aspirants[i].created_at+'</td>';
//         html+='<td><a href="{{ url('/es/backend/preselection/aspirants/')}}/'+aspirants[i].id+'/edit/" class="btn btn-default btn-flat"> <i class="fa fa-pencil"></i></a> <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ url('/es/backend/preselection/aspirants')}}/'+aspirants[i].id+'"><i class="fa fa-trash"></i> </button> </td>';
//
//         html+="</tr>";
//       }//for
//       if ( $.fn.DataTable.isDataTable('#tableAspirants') ) {
//         $('#tableAspirants').DataTable().destroy();
//       }
//       $('#tableAspirants tbody').html(html);
//       $('#tableAspirants').DataTable({
//         dom: 'Bfrtip',
//         buttons: [
//             {
//                 extend: 'excelHtml5',
//                 exportOptions: {
//                     columns: ':visible'
//                 }
//             },
//             {
//                 extend: 'pdfHtml5',
//                 orientation: 'landscape',
//                 pageSize: 'LEGAL',
//                 exportOptions: {
//                     columns: [ 0, ':visible']
//                 }
//             },
//             'colvis'
//         ],
//         "paginate": true,
//         "lengthChange": true,
//         "filter": true,
//         "sort": true,
//         "info": true,
//         "autoWidth": true,
//         "order": [[ 8, "desc" ]],
//         "language": {
//           "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
//         }
//       });
//     },
//     error:function(error){
//     }
//   });//ajax
// }

function loadStates(){
  $.ajax({
    url:"{{url('/')}}"+'/states',
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{},
    success:function(result){
      var cities=result;
      var html="";
      html+='<option value="0">Seleccione un estado</option>';
      for(var i=0;i<cities.length;i++)
        html+='<option value="'+cities[i].id+'">'+cities[i].name+'</option>';
      $('#state_id').html(html);
    },
    error:function(error){
      console.log(error);
    }
  });//ajax
}

function loadMunicipalities(){
    $('#divMunicipalities').show();
    $('#divParishes').hide();
    state_id=$('#state_id').val()
  $.ajax({
    url:"{{url('/')}}"+'/municipalities',
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{state_id:state_id},
    success:function(result){
      var municipalities=result;
      var html="";
      html+='<option value="0">Seleccione un municipio</option>';
      for(var i=0;i<municipalities.length;i++)
        html+='<option value="'+municipalities[i].id+'">'+municipalities[i].name+'</option>';
      $('#municipality_id').html(html);


    },
    error:function(error){
      console.log(error);
    }
  });//ajax
}//loadMunicipalities()

function loadParishes(){
    $('#divParishes').show();
    municipality_id=$('#municipality_id').val();
  $.ajax({
    url:"{{url('/')}}"+'/parishes',
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{municipality_id:municipality_id},
    success:function(result){
      var parishes=result;
      var html="";
      for(var i=0;i<parishes.length;i++){
        if(i==0)
        html+='<option value="'+parishes[i].id+'" selected>'+parishes[i].name+'</option>';
        else
        html+='<option value="'+parishes[i].id+'">'+parishes[i].name+'</option>';
      }
      $('#parish_id').html(html);
    },
    error:function(error){
      console.log(error);
    }
  });//ajax
}//loadParishes()
function getBusiness(){
  $.ajax({
    url:"{{route('business.businesses')}}",
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{},
    success:function(result){
      var business=result;
      var html="";
        html+='<option value="0">Seleccione</option>';
      for(var i=0;i<business.length;i++)
        html+='<option value="'+business[i].id+'">'+business[i].name+'</option>';
      $('#business_id').html(html);
    },
    error:function(error){
      console.log(error);
    }
  });//ajax
}//getBusiness()

function getUnitBusiness(){
  var business_id=$('#business_id').val()
  $.ajax({
    url:"{{url('/')}}"+'/unitBusiness/'+business_id,
    type:'GET',
    headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
    dataType:"json",
    data:{},
    success:function(result){
      console.log(result);
      var unit=result;
      var html2="";
      html2+='<option value="0">Seleccione</option>';
      for(var i=0;i<unit.length;i++)
        html2+='<option value="'+unit[i].unit.id+'">'+unit[i].unit.name+'</option>';
      $('#unit_id').html(html2);
    },
    error:function(error){
      console.log(error);
    }
  });//ajax
}



$(function () {
  getAspirants();
  loadStates();
  getBusiness();
  $('#divParishes').hide();
  $('#divMunicipalities').hide();
});




</script>
@endpush
