@extends('layouts.master2')
@section('content')
<!--START HOME-->
<link rel="stylesheet" href="{{url('modules/preselection/css/loadingModal.css')}}">
<style media="screen">
.container-fluid {
  width: 80% !important;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}

.file {  display: none}

.file_label {
  display: block;
  margin: 50px auto;
  font-size: 20px;
  font-family: Arial;
  /* width: 250px; */
  /* height: 40px; */
  line-height: 40px;
  background-color: #dc3545;
  color: #FFF;
  text-align: center;
  padding: 5px;
  border: 1px solid #CCC;
  border-radius: 5px;
  transition: all 0.3s ease-in-out
}

.file_label:hover {
  background-color: #dc3545;
  color: #ffffff;
}
.fa {
  margin-right: 5px
}
.bg-overlay-aspirants {
  background: linear-gradient(to right, #fd6b00, #fd0054 100%);
  opacity: 0.9;
  position: absolute;
  height: 100%;
  width: 100%;
  right: 0;
  bottom: 0;
  left: 0;
  top: 0;
}
</style>
<div id="formulario" class="page" data-icontenttype="page" data-icontentid="1">
  <section class="section section-lg bg-web-desc" id="joinUs">
    <div class="bg-overlay-aspirants"></div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 text-center pt-5">
          <h1 class="section-title text-center text-white" >Trabaja con nosotros</h1>
          <div class="section-title-border margin-t-20"></div>

          <div  class="custom-form mt-4 pt-5">
            <div id="message"></div>
            <div id="contact-form">
              <!-- CARGO A ASPIRAR  -->
              <fieldset>
                <div class="container">
                  <div class="row">
                    <div class="mx-auto">
                      <div class="form-row">
                        <div class="col-lg-4">
                          <div class="form-group mt-2">
                            <div class="input-file-container">
                              <img src="{{url($aspirant->curriculum_file)}}" class="img-fluid" style="height:225px;width:225px;border-radius: 14vh;">
                              <h3 class="text-white">Estado de solicitud: {{preselection__getStatus()->get($aspirant->status)}}</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                <div class="container">
                  <div class="row">
                    <div class="mr-auto">
                      <!-- <h4 class="text-white">Datos del cargo a aspirar</h4> -->
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col-lg-6">
                    <div class="form-group mt-2">
                      <label for="" class="text-white" >Empresa</label>
                      <input type="text" class="form-control" value="{{$aspirant->business->name}}" readonly>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group mt-2">
                      <label class="text-white" for="">Cargo</label>
                      <input type="text" class="form-control" value="{{$aspirant->unit->name}} " readonly>
                    </div>
                  </div>
                </div>
              </fieldset>   <!-- CARGO A ASPIRAR  -->

              <!-- DATOS DE PERSONALES DEL ASPIRANTE  -->
              <fieldset>
                <div class="container">
                  <div class="row">
                    <div class="mr-auto">
                      <!-- <h4 class="text-white">Datos Básicos</h4> -->
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Cédula</label>
                      <input class="form-control" :value="aspirants.identification"  readonly type="text">
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Primer Nombre</label>
                      <input class="form-control" :value="aspirants.name"  readonly type="text">

                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Primer Apellido</label>
                      <input readonly type="text" class="form-control" :value="aspirants.second_name"  >
                    </div>
                  </div>
                </div> <!--form-row-->

                <div class="form-row">
                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Sexo</label>

                      <input v-if="aspirants.sexper =='M'" readonly type="text" class="form-control" value="Masculino" >
                      <input v-else readonly type="text" class="form-control" value="Femenino" >
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Correo electrónico</label>
                      <input readonly type="email" class="form-control" :value="aspirants.email"  >
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="form-group mt-2">
                      <label for="" class="text-white">Teléfono de contacto</label>
                      <input readonly type="number" class="form-control" :value="aspirants.phone">
                    </div>
                  </div>
                </div> <!--form-row-->
                <div class="form-row">
                  <div class="col-lg-3">
                    <label for="" class="text-white">Estado</label>
                    <input type="text" class="form-control" readonly value="{{$aspirant->parish->municipality->state->name}}">
                  </div>

                  <div class="col-lg-3">
                    <label for="" class="text-white">Ciudad</label>
                    <input type="text" class="form-control" readonly :value="aspirants.city.city">
                  </div>
                  <div class="col-lg-3">
                    <label for="" class="text-white">Municipio</label>
                    <input type="text" class="form-control" readonly value="{{$aspirant->parish->municipality->name}}">
                  </div>

                  <div class="col-lg-3">
                    <label for="" class="text-white">Parroquia</label>
                    <input type="text" class="form-control" readonly :value="aspirants.parish.name">
                  </div>

                  <div class="col-lg-12">
                    <label for="" class="text-white">Calle</label>
                    <input readonly type="text" class="form-control" :value="aspirants.address"  >
                  </div>
                </div> <!--form-row-->
              </fieldset>
              <!--Datos de estudio-->
              <fieldset>
                <hr class="bg-danger">
                <div class="row">
                  <div class="col-12">
                    <div class="container">
                      <div class="row">
                        <div class="mr-auto">
                          <h4 class="text-white">Datos de estudio</h4>
                        </div>
                        <div class="ml-auto pb-3">
                          <button v-if="!viewStudy" type="button" name="button" class="btn btn-danger" @click="viewStudy = true">Ver</button>
                          <button v-if="viewStudy" type="button" name="button" class="btn btn-danger" @click="viewStudy = false">Ocultar</button>
                        </div>
                      </div>
                    </div>
                    <div v-if="viewStudy"  id="no-more-tables">
                      <table  class="table table-bordered table-striped table-condensed cf text-white">
                        <thead>
                          <tr>
                            <th>Instrucción</th>
                            <th>Institución</th>
                            <th>Título Obtenido</th>
                            <th>Culminación </th>
                            <th>Ciudad</th>
                            <th>Adjuntar.Comprobante</th>
                          </tr>
                        </thead>
                        <tbody>

                          <tr v-for="(item,index) in aspirants.studies">
                            <td data-title="Nivel del Instrucción">
                              @{{item.type_education}}
                            </td>
                            <td data-title="Institución">
                              @{{item.institution}}
                            </td>
                            <td data-title="Título Obtenido">
                              @{{item.title}}
                            </td>
                            <td data-title="Culminación">
                              @{{item.date_end}}
                            </td>
                            <td data-title="Ciudad">
                              @{{item.city}}
                            </td>
                            <td>
                              <div class="input_file" v-if="item.document == null  ">
                                <label for="fileStudies" class="file_label">
                                  <i class="fa fa-upload" aria-hidden="true"></i>
                                </label>
                                <input   id="fileStudies" type="file" class="file" name="file"  accept="application/images,application/pdf"   @change="onChangeFileUploadStore($event,item.id,'studies',item.aspirant_id,index)"/>
                                <p>Archivo: @{{fileName}} </p>
                              </div>
                              <div v-else>
                                <label>Ya adjunto un comprobante</label>
                              </div>
                            </td>
                          </tr>
                        </div>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </fieldset><!-- DATOS DE ESTUDIO   -->

            <!-- FORMACION DEL ASPIRANTE-->
            <fieldset>
              <hr class="bg-danger">
              <div class="row">
                <div class="col-12">
                  <div class="container">
                    <div class="row">
                      <div class="mr-auto">
                        <h4 class="text-white">Formación y capacitación</h4>
                      </div>
                      <div class="ml-auto pb-3">
                        <button v-if="!viewCourses" type="button" name="button" class="btn btn-danger" @click="viewCourses = true">Ver</button>
                        <button v-if="viewCourses" type="button" name="button" class="btn btn-danger" @click="viewCourses = false">Ocultar</button>
                      </div>
                    </div>
                  </div>
                  <div v-if="viewStudy"  id="no-more-tables">
                    <table  class="table table-bordered table-striped table-condensed cf text-white">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Institución</th>
                          <th>Duración </th>
                          <th>Fecha</th>
                          <th>Adjuntar.Comprobante</th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr v-for="(item,index) in aspirants.courses">
                          <td data-title="Nivel del Instrucción">
                            @{{item.name}}
                          </td>
                          <td data-title="Institución">
                            @{{item.institution}}
                          </td>
                          <td data-title="Duración">
                            @{{item.hours}}
                          </td>
                          <td data-title="Fecha">
                            @{{item.date}}
                          </td>
                          <td>
                            <div class="input_file" v-if="item.document == null   ">
                              <label for="fileCourses" class="file_label">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                              </label>
                              <input class="file"  id="fileCourses" type="file" name="file"   accept="application/images,application/pdf"   @change="onChangeFileUploadStore($event,item.id,'courses',item.aspirant_id,index)"/>
                              <p>Archivo: @{{fileName}} </p>
                            </div>
                            <div v-else>
                              <label>Ya adjunto un comprobante</label>
                            </div>
                          </td>
                        </tr>
                      </div>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </fieldset>

          <!-- EXPERIENCIA LABORAL DEL ASPIRANTE-->
          <fieldset>
            <hr class="bg-danger">
            <div class="row">
              <div class="col-12">
                <div class="container">
                  <div class="row">
                    <div class="mr-auto">
                      <h4 class="text-white">Experiencia Laboral</h4>
                    </div>
                    <div class="ml-auto pb-3">
                      <button v-if="!viewWork" type="button" name="button" class="btn btn-danger" @click="viewWork = true">Ver</button>
                      <button v-if="viewWork" type="button" name="button" class="btn btn-danger" @click="viewWork = false">Ocultar</button>
                    </div>
                  </div>
                </div>
                <div v-if="viewStudy"  id="no-more-tables">
                  <table  class="table table-bordered table-striped table-condensed cf text-white">
                    <thead>
                      <tr>
                       <th scope="col">Empresa</th>
                       <th scope="col">Dirección</th>
                       <th scope="col">Tel.Empresa </th>
                       <th scope="col">Contacto</th>
                       <th scope="col">Cargo</th>
                       <th scope="col">Des. de la funciones desempeñadas</th>
                       <th scope="col">Desde</th>
                       <th scope="col">Hasta</th>
                       <th scope="col">Ultimo salario</th>
                       <th scope="col">Motivo de terminación</th>
                       <th scope="col">Acción</th>
                     </tr>
                   </thead>
                   <tbody>

                    <tr v-for="(item,index) in aspirants.work_experience">
                      <td data-title="Empresa">
                        @{{item.business_name}}
                      </td>
                      <td data-title="Dirección">
                        @{{item.business_address}}
                      </td>
                      <td data-title="Teléfono">
                        @{{item.business_phone}}
                      </td>
                      <td>@{{item.business_contact_person}}</td>
                      <td>@{{item.job_title}}</td>
                      <td>@{{item.description_functions_performed}}</td>
                      <td>@{{item.since}}</td>
                      <td>@{{item.until}}</td>
                      <td>@{{item.last_salary}}</td>
                      <td>@{{item.reason_for_termination}}</td>
                      <td>
                        <div class="input_file" v-if="item.document == null">
                          <label for="fileWork" class="file_label">
                            <i class="fa fa-upload" aria-hidden="true"></i>
                          </label>
                          <input   id="fileWork" type="file" name="file" class="file"   accept="application/images,application/pdf"  @change="onChangeFileUploadStore($event,item.id,'workExperience',item.aspirant_id,index)"/>
                          <p>Archivo: @{{fileName}} </p>
                        </div>
                        <div v-else>
                          <label>Ya adjunto un comprobante</label>
                        </div>
                      </td>
                    </tr>
                  </div>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </fieldset>




      <!-- CARGA FAMILIAR  -->
      <fieldset>
        <hr class="bg-danger">
        <div class="container">
          <div class="row">
            <div class="mr-auto">
              <h4 class="text-white">Carga Familiar</h4>
            </div>

        </div>

        <div class="form-row">
          <div class="form-group col-4">
            <label>Nombre</label>
            <input type="text" class="form-control" v-model="name">
          </div>

          <div class="form-group col-4">
            <label>Apellido</label>
            <input type="text" class="form-control" v-model="last_name">
          </div>

          <div class="form-group col-4">
            <label>Fecha de nacimiento</label>
            <input type="date" class="form-control" v-model="birthday">
          </div>

          <div class="form-group col-3">
            <label>Parentesco</label>
            <select class="form-control" v-model="relationship">
              <option value="0">Seleccione el parentesco</option>
              <option value="Padre">Padre</option>
              <option value="Madre">Madre</option>
              <option value="Hijo">Hijo</option>
              <option value="Hija">Hija</option>
              <option value="Hermano">Hermano</option>
              <option value="Hermana">Hermana</option>
              <option value="Esposa">Esposa</option>
              <option value="Esposo">Esposo</option>
            </select>
          </div>

          <div class="form-group col-2" >
            <label>Posee Cédula</label><br>
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger" @click="haveIdentification = true">Si</button>
              <button type="button" class="btn btn-warning text-white" @click="haveIdentification = false">No</button>
            </div>
          </div>

          <div v-if="haveIdentification" class="form-group col-2">
            <label>Tipo de documento</label>
            <select class="form-control" v-model="typeDocument">
              <option value="V-">V</option>
              <option value="E-">E</option>

            </select>

          </div>

          <div v-if="haveIdentification" class="form-group col-5">
            <label>C.I</label>
            <input type="text" class="form-control" v-model="identification">
          </div>

          <div class="container">
            <div class="mx-auto pb-3">
              <button class="btn btn-danger"  @click="addFamiliy" title="Agregar carga familiar">Agregar</button>
            </div>
          </div>
        </div>

        <div id="no-more-tables" v-if="aspirants.families.length>0">
          <table   class="table table-bordered table-striped table-condensed cf text-white" >
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Fecha de nacimiento</th>
                <th>Parentesco </th>
                <th>C.I</th>
              </tr>
            </thead>
            <tbody>
              <tr  v-for="(item,indexFamily) in aspirants.families">
                <td data-title="Nombre">
                  @{{item.name}}
                </td>
                <td data-title="Apellido">
                  @{{item.last_name}}
                </td>
                <td data-title="Fecha de nacimiento">
                  @{{item.birthday}}
                </td>
                <td data-title="Parentesco">
                  @{{item.relationship}}
                </td>
                <td data-title="C.I" v-if="item.identification !=null">
                  @{{item.identification}}
                </td>
                <td data-title="C.I" v-else>
                  No posee
                </td>
              </tr>

            </tbody>
          </table>
        </div>

        <div v-else>
          <table   class="table table-bordered table-striped table-condensed cf text-white" >
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Fecha de nacimiento</th>
                <th>Parentesco </th>
                <th>C.I</th>
              </tr>
            </thead>
            <tbody>
              <tr >
                <td>Aun no ha registrado carga familiar</td>
              </tr>

            </tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
</div>



</fieldset><!-- CARGA FAMILIAR -->






        <!-- <div class="row">
          <div class="col-lg-12 text-center">
            <button type="button" name="button" class="submitBnt btn btn-custom" @click="uploadFile()">Registrar datos</button>
            <div id="simple-msg"></div>
          </div>
        </div> -->




      </div> <!--contact-form-->

      <!-- Modal  Cargar datos personales-->
      <div class="modal" id="modalPersonalFile">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-danger text-center">
              <div class="mx-auto">
                <h3 class="modal-title text-white ">¡Atención!</h3>
                <h4 class="text-white">Antes de continuar debe adjuntar los siguientes archivos, es requisito obligatorio para trabajar en la Fundadora,cédula, rif,certificado de salud y certificado de alimentos</h4>
              </div>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <div class="form-row">
                <div class="form-group col-6">
                  <label class="text-dark font-weight-bold">RIF</label>
                  <div  v-if="aspirants.passport_document == null && !passport_document " class="input_file" >
                    <label for="fileRif" class="file_label">
                      Adjuntar RIF
                      <i class="fa fa-upload" aria-hidden="true"></i>
                    </label>
                    <input id="fileRif" class="file" type="file" name="file"   @change="onChangeFileUploadStore($event,aspirants.id,'passport',aspirants.id)" />
                  </div>
                  <div v-else>
                    <label class="text-dark">Ya adjunto el comprobante  </label>
                  </div>
                </div>

                <div class="form-group col-6">
                  <label class="text-dark font-weight-bold">Cédula de identidad</label>
                  <div  v-if="aspirants.identification_document == null && !documentIdentification " class="input_file">
                    <label for="fileIdentification" class="file_label">
                      Adjuntar C.I
                      <i class="fa fa-upload" aria-hidden="true"></i>
                    </label>
                    <input id="fileIdentification" class="file" type="file" name="file"    @change="onChangeFileUploadStore($event,aspirants.id,'identification',aspirants.id)"/>
                  </div>
                  <div v-else>
                    <label class="text-dark">Ya adjunto el comprobante</label>
                  </div>
                </div>
              </div>

              <div class="form-row">
               <div class="form-group col-6">
                <label class="text-dark font-weight-bold">Certificado  de Alimentos</label>
                <div class="input_file" v-if="aspirants.food_handling_document == null && !document_food_handling ">
                  <label for="fileFood" class="file_label">
                    Adjuntar
                    <i class="fa fa-upload" aria-hidden="true"></i>
                  </label>
                  <input id="fileFood" type="file" class="file" name="file"    @change="onChangeFileUploadStore($event,aspirants.id,'foodHandling',aspirants.id)" />

                </div>

                <div v-else>
                  <label class="text-dark">Ya adjunto el comprobante</label>
                </div>
              </div>

              <div class="form-group col-6">
                <label class="text-dark font-weight-bold">Carta de Buena conducta</label>
                <div class="input_file" v-if="aspirants.good_conduct_letter_document ==null && !document_good_conduct_letter ">
                  <label for="fileGoodConduct" class="file_label">
                    Adjuntar
                    <i class="fa fa-upload" aria-hidden="true"></i>
                  </label>
                  <input id="fileGoodConduct" type="file" class="file" name="file"    @change="onChangeFileUploadStore($event,aspirants.id,'goodConductLetter',aspirants.id)"/>

                </div>
                <div v-else>
                  <label class="text-dark">Ya adjunto el comprobante</label>

                </div>
              </div>
            </div>

            <div class="form-row">

              <div class="form-group col-12">
                <label class="text-dark font-weight-bold">Certificado de salud</label>
                <div class="input_file" v-if="aspirants.health_certificate == null && !health_certificate_document ">
                  <label for="healthCertificate" class="file_label">
                    Adjuntar
                    <i class="fa fa-upload" aria-hidden="true"></i>
                  </label>
                  <input id="healthCertificate" type="file" class="file" name="file"    @change="onChangeFileUploadStore($event,aspirants.id,'healthCertificate',aspirants.id)"/>

                </div>
                <div v-else>
                  <label class="text-dark">Ya adjunto el comprobante</label>

                </div>
              </div>
            </div>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer" v-if="documentIdentification ">
            <button  type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>

        </div>
      </div>
    </div>


    <!-- Modal Loading-->
    <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel" aria-hidden="true">
      <div class="modal-dialog loadingDialog" role="document">
        <div class="modal-content loadingContent">
          <div class="modal-body loadingBody">
            <h2 class="loading-message">Por favor espere</h2>
            <h4 class="text-center pt-3">Se está procesando su solicitud</h4>
            <hr>
            <div class="flower-spinner">
              <span class="loading">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              </span>
            </div>

          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>


    <div class="container">
      <div class="row">
        <div class="col-12 mainDiv">

        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
</section>







</div>

@section('scripts')
<!-- SCRIPT FILE -->
<script type="text/javascript">

  const app3=new Vue({
    el:"#formulario",
    data:{
      families:[],
      name:'',
      last_name:'',
      identification:'',
      relationship:0,
      birthday:'',
      viewStudy:true,
      viewWork:true,
      viewCourses:true,
      fileStore:'',
      fileName:'',
      images:'',
      identificationFamilies:'',
      typeDocument:'V-',
      documentWork:false,
      documentCourses:false,
      documentStudies:false,
      haveIdentification:false,
      documentRif:false,
      documentIdentification:false,
      document_food_handling:false,
      health_certificate_document:false,
      passport_document:false,
      document_good_conduct_letter:false,
      aspirants:{!!$aspirant!!}
    },
    mounted(){
      if (this.aspirants.identification_document == null || this.aspirants.food_handling_document ==null || this.aspirants.good_conduct_letter_document ==null  || this.aspirants.passport_document ==null ) {
        $('#modalPersonalFile').show().modal({backdrop: 'static', keyboard: false})
      }

      if(this.aspirants.identification_document !=null){
        this.documentIdentification =true;
      }
      if( this.aspirants.food_handling_document !=null){
        this.document_food_handling=true;
      }
      if( this.aspirants.good_conduct_letter_document !=null){
        this.document_good_conduct_letter=true;
      }
      if(this.aspirants.passport_document !=null){
        this.documentRif=true;
      }
      if(this.aspirants.healthCertificate !=null){
        this.health_certificate_document=true;
      }



    },
    methods:{
      clearFamilies(){
        this.identification='';
        this.relationship='';
        this.last_name='';
        this.birthday='';
        this.aspirant_id='';
        this.typeDocument='V-';
        this.haveIdentification=false;
      },
      addFamiliy(){
        var flagValidation=true;
        if (this.relationship ==0) {
          flagValidation=false;
          toastr.error('Debes seleccionar el parentesco familiar');
        }else if (this.name =='') {
          flagValidation=false;
          toastr.error('Debes ingresar el nombre');
        }else if (this.last_name =='') {
          flagValidation=false;
          toastr.error('Debes ingresar el apellido');
        }else if (this.birthday=='') {
          toastr.error('Debes ingresar la fecha de nacimiento');
          flagValidation=false;
        }


        if (this.haveIdentification == false) {
          this.identification=null;
          console.log(this.identification);
        }else if(this.haveIdentification == true){
          this.identification=this.typeDocument+this.identification;
          console.log(this.identification);
        }

        if (flagValidation) {
          $('#loadingModal').show().modal({backdrop: 'static', keyboard: false})
          axios.post("{{ url('storeFamily')}}",
          {
            'relationship':this.relationship,
            'name':this.name,
            'last_name':this.last_name,
            'birthday':this.birthday,
            'identification':this.identification,
            'aspirant_id':this.aspirants.id
          }).then(response => {
           $('#loadingModal').modal('toggle');
           toastr.success(response.data.msg);
           this.clearFamilies();
           this.aspirants.families.push(response.data.data);
         }).catch(error => {
           $('#loadingModal').modal('toggle');
           toastr.error(error.response.data.msg)
         });
       }

     },
     onChangeFileUploadStore(e,id,type,aspirant_id,index){
       console.log(type);
       console.log(index);
      this.fileStore = e.target.files[0];
      this.fileName = this.fileStore.name;

      var input = e.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.images = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }

      let formData = new FormData();
      formData.append('file', this.fileStore);
      formData.append('type', type);
      formData.append('aspirant_id', aspirant_id);
      formData.append('entity_id', id);

      formData.append('Content-Type', "application/image/png,application/pdf");

      if(confirm("¿Está seguro de subir este archivo? Tenga en cuenta que no podrá subirlo nuevamente.")){
        $('#loadingModal').show().modal({backdrop: 'static', keyboard: false})
        axios.post("{{route('media.upload')}}",
          formData,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
            }
          }
          ).then(response=>{
            $('#loadingModal').modal('toggle');
            toastr.success(response.data.data);
            this.fileStore ="";
            this.fileName = "";
            /*VALIDACIONES BANDERA*/
            if (type =='workExperience') {
              this.aspirants.work_experience[index].document=true;
            }else if(type=='courses'){
              this.aspirants.courses[index].document=true;
            }else if(type=='studies'){
              this.documentStudies=true;
              this.aspirants.studies[index].document=true;
            }else if(type=='passport'){
              this.documentRif = true;

            }else if(type =='identification'){
              console.log('entro a identification')
              this.documentIdentification=true;
            }else if(type =='foodHandling'){
              this.document_food_handling=true;
            }else if(type =="goodConductLetter"){
              this.document_good_conduct_letter=true;
            }else if (type=='healthCertificate') {
              this.health_certificate_document=true;

            }

            /*VALIDACION  PARA CERRAR EL MODAL PERSONAL FILE*/
            if (  this.documentIdentification == true && this.documentRif == true && this.document_food_handling == true && this.document_good_conduct_letter ==true) {
              $('#modalPersonalFile').modal('hide');

            }



          }).catch(err=>{
            $('#loadingModal').modal('toggle');
            $.each(err.data.errors, function( index, value ) {
              toastr.error(value);
            });
    }); //CATCH
        }
      },

      loadFamilyData(){
        this.families.push({'relationship':this.relationship,'name':this.name,'last_name':this.last_name,'birthday':this.birthday,'aspirant_id':this.aspirants.id });
      },


    },

  });
</script>

@stop
