<link rel="stylesheet" href="{{url('modules/preselection/css/loadingModal.css')}}">
<link rel="stylesheet" href="{{url('modules/preselection/css/vue-form-wizard.min.css')}}">

<style media="screen">
.bg-overlay {
    background: linear-gradient(to right, #9BCB37 0%, #FF6402 100%);
    opacity: 0.9;
    position: absolute;
    height: 100%;
    width: 100%;
    right: 0;
    bottom: 0;
    left: 0;
    top: 0;
}
</style>

<div id="formulario" class="custom-form mt-4 pt-4">
  <div id="message"></div>
  <form-wizard  @on-complete="onComplete" ref="wizard" >
  <tab-content title="Cargo a aspirar" :before-change="() => validateStep('unit')">
    <div class="form-row">
      <div class="col-lg-6">
        <div class="form-group mt-2">
          <label for="" class="text-white" >Empresa</label>
          <select class="form-control" name="business_id" id="business_id" v-model="business_id" @change="getUnitBusiness">
            <option value="0">Seleccione la empresa</option>
            <option v-for="item in company" v-bind:value="item.id">
              @{{ item.name }}
            </option>
          </select>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group mt-2">
          <label class="text-white" for="">Cargo</label>
          <select class="form-control" name="unity_id" id="office" v-model="unity_id" @change="getHabilities" >
            <option value="0">Seleccione un cargo/oficio</option>
            <option  v-for="item in unitBusiness" v-bind:value="item.unit_id">
              @{{ item.unit.name }}
            </option>
          </select>
        </div>
      </div>

      <div class="ml-auto" v-if="profileUnit !=''&& descriptionUnit !=''  ">
        <button data-toggle="modal" data-target="#myModalUnit" type="button" name="button" class="btn btn-warning text-white">Ver Perfil requerido</button>
      </div>
    </div>
  </tab-content>

  <tab-content title="Datos Básicos" :before-change="() => validateStep('basicData')">
    <div class="container">
      <div class="row">
        <div class="mx-auto">
          <div class="form-row">
            <div class="col-lg-4">
              <div class="form-group mt-2">
                <div class="input-file-container">
                  <input name="curriculum_file" class="input-file" id="my-file" type="file" accept="image/*"    @change="onChangeFileUploadStore">
                  <img v-model="images"  :src="images" class="img-fluid" style="height:225px;width:225px;border-radius: 14vh;">
                  <label tabindex="0" for="my-file" class="input-file-trigger">
                    <i class="fas fa-camera"></i>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-row">
      <div class="col-lg-1">
        <div class="form-group mt-2">
          <label for="" class="text-white">T.Documento</label>
          <select class="form-control" v-model="typeDocument">
            <option value="V">V</option>
            <option value="E">E</option>
          </select>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="form-group mt-2">
          <label for="" class="text-white">Cédula</label>
          <input name="identification" id="cedula" type="number" class="form-control" placeholder="12345678" v-model="identification" @change="validateIdentification"  >
        </div>
      </div>



      <div class="col-lg-2">
        <div class="form-group mt-2">
          <label for="" class="text-white">Primer Nombre</label>
          <input name="name" id="name" type="text" class="form-control" placeholder="Primer nombre" v-model="name" >
        </div>
      </div>

      <div class="col-lg-2">
        <div class="form-group mt-2">
          <label for="" class="text-white">Seg. Nombre</label>
          <input name="last_name"  type="text" class="form-control" placeholder="Segundo nombre" v-model="second_name" >
        </div>
      </div>

      <div class="col-lg-2">
        <div class="form-group mt-2">
          <label for="" class="text-white">Primer Apellido</label>
          <input name="last_name"  type="text" class="form-control" placeholder="Primer apellido" v-model="surname" >
        </div>
      </div>

      <div class="col-lg-3">
        <div class="form-group mt-2">
          <label for="" class="text-white">Seg. Apellido</label>
          <input name="last_name" type="text" class="form-control" placeholder="Segundo apellido" v-model="second_surname" >
        </div>
      </div>
    </div> <!--form-row-->

    <div class="form-row">
      <div class="col-lg-4">
        <div class="form-group mt-2">
          <label for="" class="text-white">Sexo</label>
          <select class="form-control" v-model="sexper">
            <option value="0">Seleccione su sexo</option>
            <option value="M">Masculino</option>
            <option value="F">Femenino</option>
          </select>
        </div>
      </div>

      <div class="col-lg-4 mt-2">
        <label for="" class="text-white">Correo electrónico (Solo gmail.com)</label>
        <div class="input-group mb-3">
          <input v-on:blur="isEmailValid()" name="email" id="email" type="text" class="form-control" placeholder="Correo electrónico" v-model="email" >
          <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">@gmail.com</span>
          </div>
        </div>
      </div>



      <div class="col-lg-4">
        <div class="form-group mt-2">
          <label for="" class="text-white">Teléfono de contacto</label>
          <input name="phone" id="telefono" type="number" class="form-control" placeholder="Teléfono de contacto" v-model="phone" maxlength="11" >
        </div>
      </div>


    </div> <!--form-row-->




    <div class="form-row">
      <div class="col-lg-3">
        <label for="" class="text-white">Estado</label>
        <select class="form-control" v-model="state_id"  @change="changeValue">
          <option value="0">Seleccione su estado</option>
          <option v-for="item in states" v-bind:value="item.id">
            @{{ item.name }}
          </option>
        </select>
      </div>

      <div class="col-lg-3">
        <label for="" class="text-white">Municipio</label>
        <select :disabled="flagMunicipalities == false"  class="form-control" v-model="municipalities_id" @click="flagParishes =true">
          <option value="0">Seleccione su municipio</option>
          <option v-if="state_id == item.state_id"  v-for="item in municipalities" v-bind:value="item.id">
            @{{ item.name }}
          </option>
        </select>
      </div>

      <div class="col-lg-3">
        <label for="" class="text-white">Parroquia</label>
        <select :disabled="flagParishes == false" name="parish_id"   class="form-control" v-model="parish_id" >
          <option value="0">Seleccione su parroquia</option>
          <option v-if="municipalities_id == item.municipality_id"  v-for="item in parishes" v-bind:value="item.id">
            @{{ item.name }}
          </option>
        </select>
      </div>

      <div class="col-lg-3">
        <label for="" class="text-white">Ciudad</label>
        <select name="city_id"   class="form-control" v-model="city_id" >
          <option value="0">Seleccione su ciudad</option>
          <option v-if="state_id == item.state_id"  v-for="item in cities" v-bind:value="item.id">
            @{{ item.city }}
          </option>
        </select>
      </div>

      <div class="col-lg-12">
        <label for="" class="text-white">Calle</label>
        <input type="text" name="address" id="direccion" value="" placeholder="Calle/Av.." class="form-control" v-model="address">
      </div>
    </div> <!--form-row-->

   </tab-content>
   <tab-content title="Datos de estudio" :before-change="() => validateStep('studyData')">
     <h3 class="text-white">Ingrese su nivel académico</h3>
         <div class="form-row">
           <div class="col-lg-3">
             <div class="form-group mt-2">
               <label for="" class="text-white">N.Instrucción</label>
               <select class="form-control" v-model="type_education">
                 <option value="0">Seleccione su nivel de instrucción</option>
                 <option value="Primaria">Primaria</option>
                 <option value="Secundaria">Secundaria</option>
                 <option value="Técnico medio">Técnico medio</option>
                 <option value="Programa Nacional de Aprendizaje(PNA)">Programa Nacional de Aprendizaje(PNA)</option>
                 <option value="Técnico Superior Universitario (TSU)">Técnico Superior Universitario (TSU)</option>
                 <option value="Universitaria (pregrado)">Universitaria (pregrado)</option>
                 <option value="Postgrado-Especialización">Postgrado - Especialización</option>
                 <option value="Postgrado-Maestría">Postgrado - Maestría</option>
                 <option value="Postgrado-Doctorado">Postgrado - Doctorado</option>
               </select>
             </div>
           </div>

           <div class="col-lg-3">
             <div class="form-group mt-2">
               <label for="" class="text-white">Institución</label>
               <input type="text" class="form-control" v-model="NameInstitution" placeholder="Nombre de la Institución">
             </div>
           </div>

           <div   class="col-lg-3">
             <div class="form-group mt-2">
               <label for="" class="text-white">Título Obtenido</label>
               <input type="text" class="form-control" v-model="title" placeholder="Título Obtenido">
             </div>
           </div>


           <div class="col-lg-3">
             <div class="form-group mt-2">
               <label for="" class="text-white">Año de culminación</label>
               <vuejs-datepicker :bootstrap-styling="true" v-model="YearCompletion"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
                 <div slot="beforeCalendarHeader" class="form-control">
                 </div>
               </vuejs-datepicker>
             </div>
           </div>
           <div class="col-lg-3">
             <div class="form-group mt-2">
             <label for="" class="text-white">Ciudad</label>
               <input type="text" class="form-control" class="form-control" v-model="city" placeholder="Ciudad de la institución" aria-label="Ciudad de la institución" aria-describedby="basic-addon2">

             </div>
           </div>

           <div class="container pb-4">
             <div class="row">
               <div class="mx-auto">
                   <button type="button" name="button" class="btn btn-danger" title="Agregar" @click="loadStudyData">
                     Agregar
                   </button>
               </div>
             </div>
           </div>

         </div> <!--form-row-->

         <div id="no-more-tables">
             <table v-if="studies.length>0" class="table table-bordered table-striped table-condensed cf text-white">
               <thead >
                 <tr >
                   <th>Instrucción</th>
                   <th>Título Obtenido</th>
                   <th>Institución</th>
                   <th>Culminación </th>
                   <th>Ciudad</th>
                   <th>Acción</th>
                 </tr>
               </thead>
               <tbody>
                 <tr v-for="(study,indexStudy) in studies">
                   <td data-title="Nivel del Instrucción">
                     <select class="form-control" v-model="study.type_education">
                       <option value="0">Seleccione su nivel de instrucción</option>
                       <option value="Primaria">Primaria</option>
                       <option value="Secundaria">Secundaria</option>
                       <option value="Técnico medio">Técnico medio</option>
                       <option value="Programa Nacional de Aprendizaje(PNA)">Programa Nacional de Aprendizaje(PNA)</option>
                       <option value="Técnico Superior Universitario (TSU)">Técnico Superior Universitario (TSU)</option>
                       <option value="Universitaria (pregrado)">Universitaria (pregrado)</option>
                       <option value="Postgrado-Especialización">Postgrado - Especialización</option>
                       <option value="Postgrado-Maestría">Postgrado - Maestría</option>
                       <option value="Postgrado-Doctorado">Postgrado - Doctorado</option>
                     </select>
                   </td>
                   <td>
                       <input type="text" class="form-control" v-model="study.title">
                   </td >
                   <td data-title="Institución">
                     <input type="text" class="form-control" v-model="study.institution">
                   </td>
                   <td data-title="Culminación">
                     <vuejs-datepicker :bootstrap-styling="true" v-model="study.date_end"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
                       <div slot="beforeCalendarHeader" class="form-control">
                       </div>
                     </vuejs-datepicker>
                   </td>
                   <td data-title="Ciudad">
                     <input type="text" class="form-control" v-model="study.city">
                   </td>
                   <td data-title="Acción">
                     <button type="button" name="button" class="btn btn-danger" @click="studies.splice(indexStudy,1)">X</button>
                   </td>
                 </tr>
               </tbody>
             </table>

             <table v-else class="table table-bordered text-white">
               <thead>
                 <tr>
                   <th>Instrucción</th>
                   <th>Institución</th>
                   <th>Culminación </th>
                   <th>Ciudad</th>
                   <th>Acción</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <td colspan="5" class="text-white">Sin datos de estudio</td>
                 </tr>
               </tbody>
             </table>
           </div>

   </tab-content>

   <tab-content title="Formación y capacitación" :before-change="() => validateStep('courses')">
     <h3 class="text-white">Por favor registre la formación y capacitación realizada, acorde al Cargo que seleccionó</h3>
     <div class="form-row">
       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Nombre</label>
           <input type="text" class="form-control" v-model="CourseName">
         </div>
       </div>
       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Institución</label>
           <input type="text" class="form-control" v-model="CourseInstitution">

         </div>
       </div>

       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Duración (en horas)</label>
           <input type="number" class="form-control" v-model="HourCourse">
         </div>
       </div>

       <div class="col-lg-3">
         <label for="" class="text-white">Fecha</label>
         <div class="form-group mt-2">
           <vuejs-datepicker :bootstrap-styling="true" v-model="DateCourse"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
             <div slot="beforeCalendarHeader" class="form-control">
             </div>
           </vuejs-datepicker>

         </div>
       </div>
       <div class="container pb-4">
         <div class="row">
           <div class="mx-auto">
               <button type="button" name="button" class="btn btn-danger" title="Agregar" @click="loadTrainingData">
                 Agregar
               </button>
           </div>
         </div>
       </div>
     </div> <!--form-row-->

     <div id="no-more-tables">
     <table v-if="courses.length>0"  class="table table-bordered table-striped table-condensed cf text-white" >
       <thead>
         <tr>
           <th>Nombre</th>
           <th>Institución</th>
           <th>Duración(en horas) </th>
           <th>Fecha</th>
           <th>Acción</th>
         </tr>
       </thead>
       <tbody>
         <tr v-for="(course,indexCourse) in courses">
           <td data-title="Nombre">
             <input type="text" class="form-control" v-model="course.name">
           </td>
           <td data-title="Institución">
             <input type="text" class="form-control" v-model="course.institution">
           </td>
           <td data-title="Duración">
             <input type="number" class="form-control" v-model="course.hours">
           </td>
           <td data-title="Fecha">
             <vuejs-datepicker :bootstrap-styling="true" v-model="course.date"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
               <div slot="beforeCalendarHeader" class="form-control">
               </div>
             </vuejs-datepicker>
           </td>
           <td data-title="Acción">
             <button type="button" name="button" class="btn btn-danger" @click="courses.splice(indexCourse,1)">X</button>
           </td>
         </tr>

       </tbody>
     </table>
     <table v-else class="table table-bordered text-white">
       <thead>
         <tr>
           <th>Nombre</th>
           <th>Institución</th>
           <th>Duración </th>
           <th>Fecha</th>
           <th>Acción</th>
         </tr>
       </thead>
       <tbody>
         <tr>
           <td colspan="5" class="text-white">Sin formación y capacitación</td>
         </tr>
       </tbody>
     </table>
   </div>
   </tab-content>

   <tab-content title="Experiencia Laboral" :before-change="() => validateStep('workExperience')" >
     <h3 class="text-white">Por favor registe su experiencia laboral, acorde al Cargo que seleccionó</h3>

     <div class="form-row">
       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Empresa</label>
           <input type="text" class="form-control" v-model="businessName" placeholder="Nombre de la empresa">
         </div>
       </div>
       <div class="col-lg-4">
         <div class="form-group mt-2">
           <label for="" class="text-white">Dir. Empresa</label>
           <input type="text" class="form-control" v-model="businessAddress" placeholder="Dirección de la empresa">
         </div>
       </div>

       <div class="col-lg-2">
         <div class="form-group mt-2">
           <label for="" class="text-white">Teléfono de la empresa</label>
           <input type="number" class="form-control" v-model="businessPhone" maxlength="11">
         </div>
       </div>

       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Persona de contacto</label>
           <input type="text" class="form-control" v-model="business_contact_person" placeholder="Persona de contacto">
         </div>
       </div>
     </div> <!--form-row-->

     <div class="form-row">
       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Desde</label>

           <vuejs-datepicker :bootstrap-styling="true" v-model="since"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
             <div slot="beforeCalendarHeader" class="form-control">
             </div>
           </vuejs-datepicker>
         </div>
       </div>
       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Hasta</label>
           <vuejs-datepicker :bootstrap-styling="true" v-model="until"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
             <div slot="beforeCalendarHeader" class="form-control">
             </div>
           </vuejs-datepicker>
         </div>
       </div>

       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Último salario</label>
           <input type="number" class="form-control" v-model="last_salary">
         </div>
       </div>

       <div class="col-lg-3">
         <div class="form-group mt-2">
           <label for="" class="text-white">Cargo</label>
           <input type="text" class="form-control" v-model="jobTitle" placeholder="Cargo">
         </div>
       </div>
     </div>

     <div class="form-row">
       <div class="col-lg-6">
         <div class="form-group mt-2">
           <label for="" class="text-white">Funciones desempeñadas</label>
           <textarea class="form-control" rows="3" cols="80" v-model="description_functions_performed" placeholder="Descripción de las funciones desempeñadas"></textarea>
         </div>
       </div>

        <div class="col-lg-6">
         <div class="form-group mt-2">
           <label for="" class="text-white">Motivo de terminación</label>
           <textarea class="form-control" rows="3" cols="80" v-model="reason_for_termination" placeholder="Motivo de terminación"></textarea>
         </div>
       </div>

       <div class="container pb-4">
         <div class="row">
           <div class="mx-auto">
             <button type="button" name="button" class="btn btn-danger" title="Agregar" @click="loadWorkData">
                 Agregar
               </button>
           </div>
         </div>
       </div>

     </div>
     <div id="no-more-tables" class="form-row">
       <table v-if="workExperience.length>0"  class="table table-bordered table-striped table-condensed cf text-white">
         <thead>
           <tr>
             <th>Empresa</th>
             <th>Dirección</th>
             <th>Tel.Empresa  </th>
             <th>Contacto</th>
             <th>Cargo</th>
             <th>Desde</th>
             <th>Hasta</th>
             <th>Último salario</th>
             <th>Des. de la funciones desempeñadas</th>
             <th>Motivo de terminación</th>
             <th>Acción</th>
           </tr>
         </thead>
         <tbody>
           <tr v-for="(work,indexWork) in workExperience">
             <td data-title="Empresa">
               <input type="text" class="form-control" v-model="work.business_name">
             </td>
             <td data-title="Dirección">
               <input type="text" class="form-control" v-model="work.business_address">
             </td>
             <td data-title="Tel.Empresa">
               <input type="number" class="form-control" v-model="work.business_phone" maxlength="11">
             </td>
             <td data-title="Contacto">
               <input type="text" class="form-control" v-model="work.business_contact_person">
             </td>
             <td data-title="Cargo">
               <input type="text" class="form-control" v-model="work.job_title">
             </td>
             <td data-title="Desde">
               <vuejs-datepicker :bootstrap-styling="true" v-model="work.since"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
                 <div slot="beforeCalendarHeader" class="form-control">
                 </div>
               </vuejs-datepicker>
             </td>
             <td data-title="Hasta">
               <vuejs-datepicker :bootstrap-styling="true" v-model="work.until"  :language="es" format="dd-MM-yy"  :bootstrap-styling="true"  :disabled-dates="disabledDates" >
                 <div slot="beforeCalendarHeader" class="form-control">
                 </div>
               </vuejs-datepicker>
             </td>
             <td data-title="Ultimo salario">
               <input type="number" class="form-control" v-model="work.last_salary">
             </td>
             <td data-title="Desc.del Cargo">
               <input type="text" class="form-control" v-model="work.description_functions_performed">
             </td>
             <td data-title="Motivo de terminación">
               <input type="text" class="form-control" v-model="work.reason_for_termination">
             </td>
             <td data-title="Acción">
               <button type="button" name="button" class="btn btn-danger" @click="workExperience.splice(indexWork,1)">X</button>
             </td>
           </tr>
         </tbody>
       </table>

       <table v-else class="table table-bordered text-white">
         <thead>
           <tr>
             <th>Empresa</th>
             <th>Dirección</th>
             <th>Tel.Empresa  </th>
             <th>Contacto</th>
             <th>Cargo</th>
             <th>Desde</th>
             <th>Hasta</th>
             <th>Ultimo salario</th>
             <th>Funciones desempeñadas</th>
             <th>Motivo de terminación</th>
             <th>Acción</th>
           </tr>
         </thead>
         <tbody>
           <tr>
             <td colspan="11">Sin experiencia laboral</td>
           </tr>
         </tbody>
       </table>
     </div>
   </tab-content>


   <tab-content title="Habilidades y destrezas" :before-change="() => validateStep('habilities')" >
      <h3 class="text-white">Seleccione las habilidades que Usted posee de acuerdo al cargo que seleccionó</h3>
     <div class='container-fluid cf2'>
       <ul class="">
         <div class="checkbox" v-for="(items,index) in habilities">
           <input type="checkbox" :value="items.description" v-bind:id="items.description" v-model="habilitiesArray">
           <label for='choice_1_1'>@{{items.description}}</label>
         </div>
       </div>
     </ul>


   </tab-content>

</form-wizard>


<!-- MODAL CON LAS DESCRIPCION DE CARGOS -->
<div class="modal" id="myModalUnit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header bg-danger text-center">
        <h4 class="modal-title text-white">Por favor lea cuidadosamente</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="descriptionUnit">
          <h3>Descripción del cargo:</h3>
          <p class="text-justify">@{{descriptionUnit}}</p>
        </div>

        <div class="profileUnit pt-3">
          <h3>Perfil Requerido</h3>
          <div v-html="profileUnit"></div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>



</div>

<!-- Modal Loading-->
<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel" aria-hidden="true">
  <div class="modal-dialog loadingDialog" role="document">
    <div class="modal-content loadingContent">
      <div class="modal-body loadingBody">
        <h2 class="loading-message">Por favor espere</h2>
        <h4 class="text-center pt-3">Se está procesando su solicitud</h4>
        <hr>
        <div class="flower-spinner">
          <span class="loading">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </span>
        </div>

      </div>

    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 mainDiv">

    </div>
  </div>
</div>

</div>



@section('scripts')
<script src="{{url('modules/preselection/js/vue-form-wizard.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/vuejs-datepicker.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/es.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/moment.min.js')}}" charset="utf-8"></script>
<script src="{{url('modules/preselection/js/momentEs.js')}}" charset="utf-8"></script>


<script type="text/javascript">
Vue.use(VueFormWizard)


const app3=new Vue({
  el:"#formulario",
  components: {
  	vuejsDatepicker
  },
  data:{
    disabledDates:{
       from:new Date(2019,10,11)
     },
    es:vdp_translation_es.js,
    summary:'',
    name:'',
    second_name:'',
    surname:'',
    second_surname:'',
    fileStore:'',
    address:'',
    unity_id:0,
    type_education:0,
    phone:'',
    NameInstitution:'',
    city:'',
    YearCompletion:'',
    CourseName:'',
    CourseInstitution:'',
    DateCourse:'',
    HourCourse:'',
    email:'',
    identification:'',
    fileName:'',
    sexper:'0',
    typeDocument:'V',
    nationality:'Venezolano',
    studies:[],
    courses:[],
    businessName:'',
    businessAddress:'',
    businessPhone:'',
    business_contact_person:'',
    jobTitle:'',
    description_functions_performed:'',
    since:'',
    last_second_name:'',
    until:'',
    reason_for_termination:'',
    last_salary:'',
    workExperience:[],
    company:[],
    unitBusiness:[],
    habilitiesArray:[],
    images:"{{url('modules/preselection/img/user-avatar.png')}}",
    states:[],
    municipalities:[],
    parishes:[],
    cities:[],
    habilities:[],
    city_id:0,
    state_id:0,
    business_id:0,
    municipalities_id:0,
    parish_id:0,
    flagMunicipalities:false,
    flagParishes:false,
    profileUnit:'',
    descriptionUnit:'',
    title:'',
    yearActually:new Date().getFullYear(),
    // reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/
    reg:/^[0-9a-zA-Z]+$/

  },
  mounted(){
    this.getStates();
    this.getMunicipalities();
    this.getParishes();
    this.getCity();
    this.getCompany();
    var initDate = moment();
    var limitDate=initDate.add(1,"days");
    this.disabledDates.from=new Date(limitDate.year(), limitDate.month(), limitDate.date());




  },
  methods:{
    validateIdentification(){
      if (this.identification.length<6) {
        toastr.error("La cédula debe tener mas de 6 digitos")
        this.identification='';
      }
    },
    restartWizard: function() {
      this.$refs.wizard.navigateToTab(0)
    },

    clearInput(){
      this.municipalities_id=0;
      this.NameInstitution='';
      this.type_education=0;
      this.city_id=0;
      this.parish_id=0;
      this.city='';
      this.summary='';
      this.sexper=0;
      this.state_id=0;
      this.fileStore='';
      this.address='';
      this.unity_id='';
      this.phone='';
      this.profileUnit='';
      this.descriptionUnit='';
      this.last_name='';
      this.second_surname='';
      this.name='';
      this.second_name='';
      this.email='';
      this.business_id='';
      this.identification='';
      this.YearCompletion='';
      this.fileName='';
      this.workExperience=[];
      this.courses=[];
      this.studies=[];
      this.typeDocument='V';
      this.images="{{url('modules/preselection/img/user-avatar.png')}}";
    },
    cleanStudyData(){
      this.type_education=0;
      this.NameInstitution='';
      this.YearCompletion='';
      this.title='';
      this.city='';
    },
    cleanCoursesData(){
      this.CourseName='';
      this.CourseInstitution='';
      this.DateCourse='';
      this.HourCourse='';
    },
    cleanWorkData(){
      this.businessName='';
      this.businessAddress='';
      this.businessPhone='';
      this.business_contact_person='';
      this.jobTitle='';
      this.description_functions_performed='';
      this.since='';
      this.until='';
      this.reason_for_termination='';
      this.last_salary='';
    },
    isEmailValid: function() {
      //return (this.email == "")? "" : (this.reg.test(this.email)) ? 'has-success' : 'has-error';
      if (this.reg.test(this.email) !=true) {
        toastr.error("Debe ingresar un correo valido");
        this.email="";
      }
    },

    validateStep(type){
      if (type =='unit') {
        if (this.unity_id && this.business_id !=null) {
          if(confirm("¿Usted Cumple con el perfil del cargo seleccionado?")){
            return true;
          }else {
            return false;
          }
        }else {
          toastr.error('Debe completar todos los campos')
          return false;
        }
      }else if (type=='basicData') {
        if (this.identification =="" || this.identification == null) {
          toastr.error("Debe ingresar su cedúla");
          return false;
        }else if (this.name=="" || this.name==null) {
          toastr.error("Debe ingresar su primer nombre");
          return false;
        }else if (this.surname== "" || this.surname == null) {
          toastr.error("Debe ingresar su primer apellido");
          return false;
        }else if (this.sexper ==0) {
          toastr.error("Debe seleccionar su sexo");
          return false;
        }else if ( this.email =="" ||  this.email ==null) {
          toastr.error("Debe ingresar su correo");
          return false;
        }else if(this.reg.test(this.email)==false){
          toastr.error("Debe ingresar un email valido");
          return false;
        }else if (this.phone =="" || this.phone==null) {
          toastr.error("Debe ingresar su teléfono");
          return false;
        }else if (this.parish_id ==0) {
          toastr.error("Debe seleccionar la parroquia");
          return false;
        }else if (this.address=="" || this.address ==null) {
          toastr.error("Debe ingresar su dirección");
          return false;
        }else if (this.city_id ==0) {
          toastr.error("Debe selecionar la ciudad");
          return false;
        }else if (this.fileStore =="" || this.fileStore ==null) {
          toastr.error("Debe ingresar su foto");
          return false;
        }else {
          if(confirm("Si Usted queda seleccionado para una futura entrevista, nos comunicaremos por medio de su dirección de correo electrónico.- Por favor verifique cuidadosamente que sus datos básicos están correctos.")){
            return true;
          }else {
            return false;
          }
        }


      }else if (type =='studyData') {
        if (this.studies.length>0) {
          for (var i = 0; i < this.studies.length; i++) {
            if (this.studies[i].type_education =='' || this.studies.type_education ==0) {
              toastr.error('Debes seleccionar el nivel de instrucción');
              return false;
              break;
            }else if (this.studies[i].institution =='') {
              toastr.error('Debes seleccionar la Institución');
              return false;
              break;
            }else if (this.studies[i].date_end =='') {
              toastr.error('Debes ingresar la fecha de culminación');
              return false;
              break;
            }else if (this.studies[i].city=='') {
              toastr.error('Debes ingresar la ciudad');
              return false;
              break;
            }
          }
          if(confirm("Si Usted es seleccionado para una posible entrevista recibirá un enlace por correo electrónico mediante el cual se le solicitará que adjunte los documentos que soporten los estudios que mencionó. Por favor, verifique que toda la información aportada pueda ser comprobada, de lo contrario usted quedaría rechazado para cualquier proceso de reclutamiento del Grupo La Fundadora.")){
            return true;
          }else {
            return false;
          }
        }else {
          toastr.error('Debes completar todos los campos');
          return false;
        }
      }else if (type =='courses') {
        if (this.courses.length>0) {
          for (var i = 0; i < this.courses.length; i++) {
            if (this.courses[i].name =='') {
              toastr.error('Debes escribir el nombre');
              return false;
              break;
            }else if (this.courses[i].institution =='') {
              toastr.error('Debes escribir la Institución');
              return false;
              break;
            }else if (this.courses[i].date=='') {
              toastr.error('Debes ingresar la fecha del curso');
              return false;
              break;
            }else if (this.courses[i].hours =='') {
              toastr.error('Debes ingresar la Duración del curso');
              return false;
              break;
            }
          }
          if(confirm("Verifique que la información suministrada está correcta. Recuerde que toda información aportada debe tener los respectivos documentos que la soporten ¿Desea continuar?")){
            return true;
          }else {
            return false;
          }
        }
        return  true;
      }else if (type =='workExperience') {
          if (this.since>this.until) {
            toastr.error("La fecha inicial (Desde) no puede ser mayor a la fecha final(Hasta)");
            return false;

          }

        if (this.workExperience<=0) {
          toastr.error("Debes ingresar tu experiencia laboral.")
          return false;
        }
        if (this.workExperience.length>0) {
          for (var i = 0; i < this.workExperience.length; i++) {
            if (this.workExperience[i].name =='') {
              toastr.error('Debes ingresar el nombre de la empresa');
              return false;
              break;
            }else if (this.workExperience[i].business_address =='') {
              toastr.error('Debes ingresar la dirección de la empresa');
              return false;
              break;
            }else if (this.workExperience[i].business_phone=='') {
              toastr.error('Debes ingresar el teléfono de la empresa');
              return false;
              break;
            }else if (this.workExperience[i].business_contact_person =='') {
              toastr.error('Debes ingresar el nombre de la persona de contacto');
              return false;
              break;
            }else if (this.workExperience[i].job_title =='') {
              toastr.error('Debes ingresar el cargo que desempeño');
              return false;
              break;
            }else if (this.workExperience[i].description_functions_performed =='') {
              toastr.error('Debes ingresar la descripción del cargo que desempeño');
              return false;
              break;
            }else if (this.workExperience[i].since =='') {
              toastr.error('Debes ingresar fecha de inicio del trabajo');
              return false;
              break;
            }else if (this.workExperience[i].until =='') {
              toastr.error('Debes ingresar fecha de culminación del trabajo');
              return false;
              break;
            }else if(this.workExperience[i].since>this.workExperience[i].until){
              toastr.error("La fecha inicial (Desde) no puede ser mayor a la fecha final(Hasta)");
              return false;
              break;
            }else if (this.workExperience[i].reason_for_termination =='') {
              toastr.error('Debes especificar el motivo porque el cual se fue de la empresa');
            }else if (this.workExperience[i].last_salary =='') {
              toastr.error('Debes ingresar su ultimo salario');
              return false;
              break;
            }
          }
          if(confirm("Verifique que la información suministrada está correcta. Recuerde que toda información aportada debe tener los respectivos documentos que la soporten ¿Desea continuar?")){
            return true;
          }else {
            return false;
          }
        } //if
        return  true;
      } else if (type =='habilities') {
        if (this.habilitiesArray == null || this.habilitiesArray.length<3) {
          toastr.error("Debe seleccionar por lo menos 3 habilidades")
          return false;
        }else {
          if(confirm("Verifique las habilidades que posee con respecto al cargo seleccionado. Es probable que si Usted queda seleccionado, pudiera ser evaluado para comprobar tal habilidad o destreza.")){
            return true;
          }else {
            return false;
          }
        }

      }
    },


    // validateHabilitiesArray(){
    //   if (this.habilitiesArray.length>1) {
    //     toastr.error('Solo puedes selecionar dos habilidades');
    //     this.habilitiesArray=[];
    //   }
    // },
    onComplete: function() {
      this.uploadFile();
    },
    isLastStep() {
     if (this.$refs.wizard) {

       return this.$refs.wizard.isLastStep
     }
     return false
   },
    getCompany(){
      axios.get("{{url('businesses')}}",{
        params:{
          filters:{
            status:1
          }
        }
      }).then(response=>{
        this.company=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getStates(){
      axios.get("{{url('states')}}").then(response=>{
        this.states=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getMunicipalities(){
      axios.get("{{url('municipalities')}}").then(response=>{
        this.municipalities=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getParishes(){
      axios.get("{{url('parishes')}}").then(response=>{
        this.parishes=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getCity(){
      axios.get("{{url('cities')}}").then(response=>{
        this.cities=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getCompany(){
      axios.get("{{url('businesses')}}",{
        params:{
          filters:{
            status:1
          }
        }
      }).then(response=>{
        this.company=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getUnitBusiness(){
      axios.get("{{url('unitBusiness')}}"+'/'+this.business_id,{
        params:{
          filters:{status:1}
        }
      }).then(response=>{
        this.unitBusiness=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    getHabilities(){
      for (var i = 0; i < this.unitBusiness.length; i++) {
        if (this.unity_id == this.unitBusiness[i].unit_id ) {
          this.profileUnit=this.unitBusiness[i].unit.required_profile;
          this.descriptionUnit=this.unitBusiness[i].unit.summary;
        }
      }

      $('#myModalUnit').show().modal({backdrop: 'static', keyboard: false})

      axios.get("{{url('randomHabilities')}}"+'/'+this.unity_id).then(response=>{
        this.habilities=response.data;
      }).catch(err=>{
        $('#loadingModal').modal('toggle');
      });
    },
    changeValue(){
      this.flagMunicipalities=true;
      this.flagParishes=false;
      this.municipalities_id=0;
      this.parish_id=0;
    },

    /***VALIDA EL FORMATO DE LA CEDULA***/
    /*****CARGOR DATOS BASICOS DEL ASPIRANTE*****/
    loadStudyData(){
      var studiesArray =false;
      for (var i = 0; i < this.studies.length; i++) {
        if (this.studies[i].type_education == this.type_education) {
          studiesArray =true;
        }
      }
      if (studiesArray ===false) {
        this.studies.push({'title':this.title,'type_education':this.type_education,'institution':this.NameInstitution,'date_end':this.YearCompletion,'city':this.city});
        this.cleanStudyData();
      }else {
        toastr.error('Ya este nivel de estudio fue ingresado');
      }
    },
    /***** CARGAR DATOS DE FORMACION Y CAPACITACION DEL ASPIRANTE ****/
    loadTrainingData(){
      var trainingArray =false;
      for (var i = 0; i < this.courses.length; i++) {
        if (this.courses[i].name == this.CourseName) {
          trainingArray =true;
        }
      }
      if (trainingArray ===false) {
        this.courses.push({'name':this.CourseName,'institution':this.CourseInstitution,'date':this.DateCourse,'hours':this.HourCourse});
        this.cleanCoursesData();
      }else {
        toastr.error('Ya este curso fue ingresado');
      }
    },

    loadWorkData(){
      var loadWorkData =false;
      for (var i = 0; i < this.workExperience.length; i++) {
        if (this.workExperience[i].businessName == this.businessName) {
          loadWorkData =true;
        }
      }
      if (loadWorkData ===false) {
        this.workExperience.push({'business_name':this.businessName,'business_address':this.businessAddress,'business_phone':this.businessPhone,'business_contact_person':this.business_contact_person,'job_title':this.jobTitle,'description_functions_performed':this.description_functions_performed,'since':this.since,'until':this.until,'reason_for_termination':this.reason_for_termination,'last_salary':this.last_salary});
        this.cleanWorkData();
      }else {
        toastr.error('Ya agrego esta experiencia laboral');
      }
    },


    uploadFile(){
      var flagValidation=true;
      let formData = new FormData();
      formData.append('curriculum_file', this.fileStore);
      formData.append('name', this.name);
      formData.append('second_name', this.second_name);
      formData.append('last_name', this.surname);
      formData.append('second_surname', this.second_surname);
      formData.append('summary', this.summary);
      formData.append('phone', this.phone);
      formData.append('email', this.email+'@gmail.com');
      formData.append('identification',this.typeDocument+'-'+this.identification);
      formData.append('unity_id', this.unity_id);
      formData.append('sexper', this.sexper);
      formData.append('nationality', this.nationality);
      formData.append('address', this.address);
      formData.append('studies', JSON.stringify(this.studies));
      formData.append('habilities', JSON.stringify(this.habilitiesArray));
      formData.append('courses', JSON.stringify(this.courses));
      formData.append('business_id', JSON.stringify(this.business_id));
      formData.append('city_id', JSON.stringify(this.city_id));
      formData.append('parish_id', JSON.stringify(this.parish_id));
      formData.append('status', JSON.stringify(10));
      formData.append('workExperience', JSON.stringify(this.workExperience));
      formData.append('Content-Type', "application/image/*");



      if (this.identification == ''  || this.identification ==null) {
        toastr.error('Debe ingresar la cédula de identidad');
      }else if (this.name == ''  || this.name ==null) {
        toastr.error('Debe ingresar tu primer nombre');
      }else if (this.surname == ''  || this.surname ==null) {
        toastr.error('Debe ingresar tu primer apellido');
      }else if (this.email == ''  || this.email ==null) {
        toastr.error('Debe ingresar tu correo electronico');
      }else if (this.phone == ''  || this.phone ==null) {
        toastr.error('Debe ingresar tu número de teléfono');
      }else if (this.unity_id == ''  || this.unity_id ==null) {
        toastr.error('Debe seleccionar un cargo');
      }else if (this.address == ''  || this.address ==null) {
        toastr.error('Debe escribir su dirección');
      }else if (this.fileStore == ''  || this.fileStore ==null) {
        toastr.error('Debe insertar una foto actual');
      }else if (this.parish_id=='' || this.parish_id ==0) {
        toastr.error('Debe seleccionar la parroquia');
      }else if (this.habilitiesArray =='' || this.habilitiesArray == null) {
        toastr.error("Debes seleccionar las habilidades")
      }
      else {
      if (this.studies.length>0) {
        for (var i = 0; i < this.studies.length; i++) {
          if (this.studies[i].type_education =='' || this.studies.type_education ==0) {
            flagValidation=false;
            toastr.error('Debes seleccionar el nivel de instrucción');
            break;
          }else if (this.studies[i].institution =='') {
            flagValidation=false;
            toastr.error('Debes seleccionar la Institución');
            break;
          }else if (this.studies[i].date_end =='') {
            flagValidation=false;
            toastr.error('Debes ingresar la fecha de culminación');
            break;
          }else if (this.studies[i].city=='') {
            toastr.error('Debes ingresar la ciudad');
            break;
            flagValidation=false;
          }
        }
      }
        if (this.courses.length>0) {
          for (var i = 0; i < this.courses.length; i++) {
            if (this.courses[i].name =='') {
              flagValidation=false;
              toastr.error('Debes escribir el nombre');
              break;
            }else if (this.courses[i].institution =='') {
              flagValidation=false;
              toastr.error('Debes escribir la Institución');
              break;
            }else if (this.courses[i].date=='') {
              flagValidation=false;
              toastr.error('Debes ingresar la fecha del curso');
              break;
            }else if (this.courses[i].hours =='') {
              flagValidation=false;
              toastr.error('Debes ingresar la Duración del curso');
              break;
            }
          }
        }

      if (this.workExperience.length>0) {
        for (var i = 0; i < this.workExperience.length; i++) {
          if (this.workExperience[i].name =='') {
            flagValidation=false;
            toastr.error('Debes ingresar el nombre de la empresa');
            break;
          }else if (this.workExperience[i].business_address =='') {
            flagValidation=false;
            toastr.error('Debes ingresar la dirección de la empresa');
            break;
          }else if (this.workExperience[i].business_phone=='') {
            flagValidation=false;
            toastr.error('Debes ingresar el teléfono de la empresa');
            break;
          }else if (this.workExperience[i].business_contact_person =='') {
            flagValidation=false;
            toastr.error('Debes ingresar el nombre de la persona de contacto');
            break;
          }else if (this.workExperience[i].job_title =='') {
            flagValidation=false;
            toastr.error('Debes ingresar el cargo que desempeño');
            break;
          }else if (this.workExperience[i].description_functions_performed =='') {
            flagValidation=false;
            toastr.error('Debes ingresar la descripción del cargo que desempeño');
            break;
          }else if (this.workExperience[i].since =='') {
            flagValidation=false;
            toastr.error('Debes ingresar fecha de inicio del trabajo');
            break;
          }else if (this.workExperience[i].since =='') {
            flagValidation=false;
            toastr.error('Debes ingresar fecha de culminación del trabajo');
            break;
          }else if (this.workExperience[i].reason_for_termination =='') {
            flagValidation=false;
            toastr.error('Debes especificar el motivo porque el cual se fue de la empresa');
            break;
          }else if (this.workExperience[i].last_salary =='') {
            flagValidation=false;
            toastr.error('Debes ingresar su ultimo salario');
            break;
          }
        }
      }


        if (flagValidation) {
          $('#loadingModal').show().modal({backdrop: 'static', keyboard: false})
          axios.post("{{route('employee.store')}}",
          formData,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
            }
          }
        ).then(response=>{

          $('#loadingModal').modal('toggle');
          toastr.success(response.data.data);
          this.clearInput();
          this.cleanCoursesData();
          this.cleanWorkData();
          this.cleanStudyData();
          this.habilities=[];
          this.habilitiesArray=[];
          this.courses=[];
          this.workExperience=[];
          this.studies=[];
          this.business_id=0;
          this.unity_id=0;
          this.restartWizard();

        }).catch(err=>{
          $('#loadingModal').modal('toggle');
          toastr.error(err.response.data.error);
        });
      } //flagValidation

    }

  },
  onChangeFileUploadStore(e){
    this.fileStore = e.target.files[0];
    this.fileName = this.fileStore.name;

    var input = e.target;
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e) => {
        this.images = e.target.result;
      }
      reader.readAsDataURL(input.files[0]);
    }

  },



}
});
</script>
@endsection
