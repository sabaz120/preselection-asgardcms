@extends('layouts.master2')
@section('content')
<style media="screen">
section#joinUs2 {
  padding-bottom: 17.56rem;
}

.container-fluid {
    width: 100% !important;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
</style>
<section class="section section-lg bg-web-desc" id="joinUs2">
  <div class="bg-overlay"></div>
  <div class="container-fluid pb-5">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="section-title text-center text-white" >Por favor registra tus datos</h1>
        <div class="section-title-border margin-t-20"></div>
        @include('preselection::frontend.employee.partials.form-request-employee')
      </div>
    </div>
  </div>
</section>

@stop
