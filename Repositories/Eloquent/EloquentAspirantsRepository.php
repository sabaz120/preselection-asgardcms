<?php

namespace Modules\Preselection\Repositories\Eloquent;

use Modules\Preselection\Repositories\AspirantsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAspirantsRepository extends EloquentBaseRepository implements AspirantsRepository
{
}
