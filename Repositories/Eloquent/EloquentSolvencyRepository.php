<?php

namespace Modules\Preselection\Repositories\Eloquent;

use Modules\Preselection\Repositories\SolvencyRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSolvencyRepository extends EloquentBaseRepository implements SolvencyRepository
{
}
