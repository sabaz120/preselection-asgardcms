<?php

namespace Modules\Preselection\Repositories\Eloquent;

use Modules\Preselection\Repositories\AspirantFamilyRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAspirantFamilyRepository extends EloquentBaseRepository implements AspirantFamilyRepository
{
}
