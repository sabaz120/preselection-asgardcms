<?php

namespace Modules\Preselection\Repositories\Eloquent;

use Modules\Preselection\Repositories\DataChangeRequestRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentDataChangeRequestRepository extends EloquentBaseRepository implements DataChangeRequestRepository
{
}
