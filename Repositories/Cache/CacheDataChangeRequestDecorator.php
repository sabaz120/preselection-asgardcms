<?php

namespace Modules\Preselection\Repositories\Cache;

use Modules\Preselection\Repositories\DataChangeRequestRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheDataChangeRequestDecorator extends BaseCacheDecorator implements DataChangeRequestRepository
{
    public function __construct(DataChangeRequestRepository $datachangerequest)
    {
        parent::__construct();
        $this->entityName = 'preselection.datachangerequests';
        $this->repository = $datachangerequest;
    }
}
