<?php

namespace Modules\Preselection\Repositories\Cache;

use Modules\Preselection\Repositories\AspirantFamilyRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAspirantFamilyDecorator extends BaseCacheDecorator implements AspirantFamilyRepository
{
    public function __construct(AspirantFamilyRepository $aspirantfamily)
    {
        parent::__construct();
        $this->entityName = 'preselection.aspirantfamilies';
        $this->repository = $aspirantfamily;
    }
}
