<?php

namespace Modules\Preselection\Repositories\Cache;

use Modules\Preselection\Repositories\AspirantsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAspirantsDecorator extends BaseCacheDecorator implements AspirantsRepository
{
    public function __construct(AspirantsRepository $aspirants)
    {
        parent::__construct();
        $this->entityName = 'preselection.aspirants';
        $this->repository = $aspirants;
    }
}
