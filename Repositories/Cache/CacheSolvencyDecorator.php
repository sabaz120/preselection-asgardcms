<?php

namespace Modules\Preselection\Repositories\Cache;

use Modules\Preselection\Repositories\SolvencyRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSolvencyDecorator extends BaseCacheDecorator implements SolvencyRepository
{
    public function __construct(SolvencyRepository $solvency)
    {
        parent::__construct();
        $this->entityName = 'preselection.solvencies';
        $this->repository = $solvency;
    }
}
