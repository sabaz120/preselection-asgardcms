<?php

use Illuminate\Routing\Router;
$locale = LaravelLocalization::setLocale() ?: App::getLocale();

$router->post('aspirants', ['as' => 'aspirants.store', 'uses' => 'PublicController@storeAspirant']);
$router->post('employee', ['as' => 'employee.store', 'uses' => 'PublicController@storeEmployeeAspirant']);
$router->get('aspirants/{code}', ['as' => 'aspirants.show', 'uses' => 'PublicController@show']);
$router->get('employee', ['as' => 'employee.show', 'uses' => 'PublicController@showEmployee']);
$router->post('media/upload',['as'=>'media.upload','uses'=>'PublicController@mediaUpload']);
$router->post('storeFamily',['as'=>'store.family','uses'=>'PublicController@storeFamily']);
$router->post('workExperience/{id}', [
    'as' => 'preselection.aspirants.work.experience.edit',
    'uses' => 'PublicController@updateWorkExperience',
    'middleware' => 'can:preselection.aspirants.edit'
 ]);

//Login aspirants
$router->post('aspirant/login', [
    'as' => 'preselection.aspirants.login',
    'uses' => 'AspirantsController@loginPost'
 ]);
 # Logout
 $router->get('logout', ['as' => 'user.logout', 'uses' => 'AspirantsController@logoutGet']);


//************************************************************************************Api routes
/** @var Router $router */
$router->group(['prefix' => 'api'], function (Router $router) {
  ////////////////////////////////////////////////Work experience
  //-Update
  $router->put('workExperience/{id}', [
    'as' => 'preselection.aspirants.api.work.experience.update',
    'uses' => 'ApplicantWorkExperienceController@update',
    'middleware' => 'logged.in'
  ]);
  //-Delete
  $router->delete('workExperience/{id}', [
    'as' => 'preselection.aspirants.api.work.experience.delete',
    'uses' => 'ApplicantWorkExperienceController@delete',
    'middleware' => 'logged.in'
  ]);
  //-Create
  $router->post('workExperience', [
    'as' => 'preselection.aspirants.api.work.experience.create',
    'uses' => 'ApplicantWorkExperienceController@create',
    'middleware' => 'logged.in'
  ]);
  ///////////////Courses
  //-Update
  $router->put('courses/{id}', [
    'as' => 'preselection.aspirants.api.courses.update',
    'uses' => 'ApplicantCourseController@update',
    'middleware' => 'logged.in'
  ]);
  //-Delete
  $router->delete('courses/{id}', [
    'as' => 'preselection.aspirants.api.courses.delete',
    'uses' => 'ApplicantCourseController@delete',
    'middleware' => 'logged.in'
  ]);
  //-Create
  $router->post('courses', [
    'as' => 'preselection.aspirants.api.courses.create',
    'uses' => 'ApplicantCourseController@create',
    'middleware' => 'logged.in'
  ]);
  ///////////////Studies
  //-Update
  $router->put('studies/{id}', [
    'as' => 'preselection.aspirants.api.studies.update',
    'uses' => 'ApplicantStudiesController@update',
    'middleware' => 'logged.in'
  ]);
  //-Delete
  $router->delete('studies/{id}', [
    'as' => 'preselection.aspirants.api.studies.delete',
    'uses' => 'ApplicantStudiesController@delete',
    'middleware' => 'logged.in'
  ]);
  //-Create
  $router->post('studies', [
    'as' => 'preselection.aspirants.api.studies.create',
    'uses' => 'ApplicantStudiesController@create',
    'middleware' => 'logged.in'
  ]);

  $router->put('aspirant/update', [
      'as' => 'preselection.aspirants.api.update',
      'uses' => 'AspirantsController@update'
   ]);

});
