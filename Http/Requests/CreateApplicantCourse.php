<?php

namespace Modules\Preselection\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateApplicantCourse extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name'=>'required',
          'institution'=>'required',
          'hours'=>'required|integer',
          'date'=>'required|date'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
