<?php

namespace Modules\Preselection\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateAspirantFamilyRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
          'relationship'=>'required',
          'name'=>'required',
          'last_name'=>'required',
          'birthday'=>'required',
          'identification'=>'',
          'aspirant_id'=>'required|exists:preselection__aspirants,id'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
