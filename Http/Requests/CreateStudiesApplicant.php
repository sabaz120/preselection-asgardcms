<?php

namespace Modules\Preselection\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudiesApplicant extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'type_education'=>'required',
          'institution'=>'required',
          'date_end'=>'required|date',
          'city'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
