<?php

namespace Modules\Preselection\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateApplicantWorkExperience extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'business_name'=>'required',
          'business_address'=>'required',
          'business_phone'=>'required',
          'business_contact_person'=>'required',
          'job_title'=>'required',
          'description_functions_performed'=>'required',
          'since'=>'required|date',
          'until'=>'required|date',
          'until'=>'required|date',
          'reason_for_termination'=>'required',
          'last_salary'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
