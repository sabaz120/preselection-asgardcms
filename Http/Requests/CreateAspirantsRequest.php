<?php

namespace Modules\Preselection\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateAspirantsRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
          'name'=>'required',
          'last_name'=>'required',
          'second_name'=>'nullable',
          'second_surname'=>'nullable',
          'identification'=>'required',
          'email'=>'required',
          'phone'=>'required',
          'address'=>'required',
          'unity_id'=>'required|exists:business__units,id',
          //'summary'=>'required',
          'curriculum_file'=>'required|mimes:jpeg,png,jpg|max:10000',
          'workExperience'=>'required',
          'courses'=>'required',
          'studies'=>'required',
          'city_id'=>'required|exists:locations__cities,id',
          'parish_id'=>'required|exists:locations__parishes,id',
          'business_id'=>'required|exists:business__businesses,id',
          'habilities'=>'required'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
