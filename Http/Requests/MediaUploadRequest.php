<?php

namespace Modules\Preselection\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class MediaUploadRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
          'type'=>'required',
          'file'=>'required|mimes:jpeg,png,jpg,pdf|max:10000',
          'entity_id'=>'required',
          'aspirant_id'=>'required|exists:preselection__aspirants,id'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
