<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/preselection'], function (Router $router) {
    $router->bind('aspirants', function ($id) {
        return app('Modules\Preselection\Repositories\AspirantsRepository')->find($id);
    });
    $router->get('aspirants', [
        'as' => 'admin.preselection.aspirants.index',
        'uses' => 'AspirantsController@index',
        'middleware' => 'can:preselection.aspirants.index'
    ]);

    $router->get('aspirantsMedicalExam', [
        'as' => 'admin.preselection.aspirantsMedicalExam.index',
        'uses' => 'AspirantsController@indexDoctor',
        'middleware' => 'can:preselection.aspirants.indexDoctor'
    ]);


    /***CORREO PRUEBA ***/
    $router->get('test3', [
        'uses' => 'AspirantsController@testEmail',
    ]);



    $router->get('workers', [
        'as' => 'admin.preselection.workers.index',
        'uses' => 'AspirantsController@indexWorkers',
        'middleware' => 'can:preselection.aspirants.indexWorkers'
    ]);

    $router->get('retiredWorkers', [
        'as' => 'admin.preselection.retiredWorkers.index',
        'uses' => 'AspirantsController@retiredWorkers',
        'middleware' => 'can:preselection.aspirants.indexWorkers'
    ]);

    $router->get('getAspirants', [
        'uses' => 'AspirantsController@getAspirants',
        'middleware' => 'can:preselection.aspirants.index'
    ]);

    $router->get('getAspirants2', [
        'uses' => 'AspirantsController@getAspirants2',
        'middleware' => 'can:preselection.aspirants.index'
    ]);

    $router->get('getCoordinators', [
        'uses' => 'AspirantsController@getCoordinators',
        'middleware' => 'can:preselection.aspirants.index'
    ]);

    $router->get('aspirants/create', [
        'as' => 'admin.preselection.aspirants.create',
        'uses' => 'AspirantsController@create',
        'middleware' => 'can:preselection.aspirants.create'
    ]);
    $router->post('aspirants', [
        'as' => 'admin.preselection.aspirants.store',
        'uses' => 'AspirantsController@store',
        'middleware' => 'can:preselection.aspirants.create'
    ]);
    $router->get('aspirants/{aspirants}/edit', [
        'as' => 'admin.preselection.aspirants.edit',
        'uses' => 'AspirantsController@edit',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);
    $router->put('aspirants/{aspirants}', [
        'as' => 'admin.preselection.aspirants.update',
        'uses' => 'AspirantsController@update',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);

    $router->put('aspirants2/{aspirants}', [
        'as' => 'admin.preselection.aspirants.update2',
        'uses' => 'AspirantsController@update2',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);

    $router->put('aspirants3/{aspirants}', [
        'as' => 'admin.preselection.aspirants.updateRetiredWorkers',
        'uses' => 'AspirantsController@updateRetiredWorkers',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);

    $router->post('getWorkers', [
          'as' => 'admin.preselection.aspirants.getWorkers',
        'uses' => 'AspirantsController@getWorkers'
    ]);




    $router->delete('aspirants/{aspirants}', [
        'as' => 'admin.preselection.aspirants.destroy',
        'uses' => 'AspirantsController@destroy',
        'middleware' => 'can:preselection.aspirants.destroy'
    ]);

    $router->bind('aspirantfamily', function ($id) {
        return app('Modules\Preselection\Repositories\AspirantFamilyRepository')->find($id);
    });
    $router->get('aspirantfamilies', [
        'as' => 'admin.preselection.aspirantfamily.index',
        'uses' => 'AspirantFamilyController@index',
        'middleware' => 'can:preselection.aspirantfamilies.index'
    ]);
    $router->get('aspirantfamilies/create', [
        'as' => 'admin.preselection.aspirantfamily.create',
        'uses' => 'AspirantFamilyController@create',
        'middleware' => 'can:preselection.aspirantfamilies.create'
    ]);
    $router->post('aspirantfamilies', [
        'as' => 'admin.preselection.aspirantfamily.store',
        'uses' => 'AspirantFamilyController@store',
        'middleware' => 'can:preselection.aspirantfamilies.create'
    ]);
    $router->get('aspirantfamilies/{aspirantfamily}/edit', [
        'as' => 'admin.preselection.aspirantfamily.edit',
        'uses' => 'AspirantFamilyController@edit',
        'middleware' => 'can:preselection.aspirantfamilies.edit'
    ]);
    $router->put('aspirantfamilies/{aspirantfamily}', [
        'as' => 'admin.preselection.aspirantfamily.update',
        'uses' => 'AspirantFamilyController@update',
        'middleware' => 'can:preselection.aspirantfamilies.edit'
    ]);
    $router->delete('aspirantfamilies/{aspirantfamily}', [
        'as' => 'admin.preselection.aspirantfamily.destroy',
        'uses' => 'AspirantFamilyController@destroy',
        'middleware' => 'can:preselection.aspirantfamilies.destroy'

    ]);
    $router->get('aspirants/{aspirants}/edit2', [
        'as' => 'admin.preselection.aspirants.edit2',
        'uses' => 'AspirantsController@edit2',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);

    $router->get('aspirants/{aspirants}/edit3', [
        'as' => 'admin.preselection.aspirants.edit3',
        'uses' => 'AspirantsController@edit3',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);

    $router->get('aspirants/{aspirants}/editRetired', [
        'as' => 'admin.preselection.aspirants.editRetired',
        'uses' => 'AspirantsController@editRetired',
        'middleware' => 'can:preselection.aspirants.edit'
    ]);


    $router->bind('datachangerequest', function ($id) {
        return app('Modules\Preselection\Repositories\DataChangeRequestRepository')->find($id);
    });
    $router->get('datachangerequests', [
        'as' => 'admin.preselection.datachangerequest.index',
        'uses' => 'DataChangeRequestController@index',
        'middleware' => 'can:preselection.datachangerequests.index'
    ]);
    $router->get('datachangerequests/create', [
        'as' => 'admin.preselection.datachangerequest.create',
        'uses' => 'DataChangeRequestController@create',
        'middleware' => 'can:preselection.datachangerequests.create'
    ]);
    $router->post('datachangerequests', [
        'as' => 'admin.preselection.datachangerequest.store',
        'uses' => 'DataChangeRequestController@store',
        'middleware' => 'can:preselection.datachangerequests.create'
    ]);
    $router->get('datachangerequests/{datachangerequest}/edit', [
        'as' => 'admin.preselection.datachangerequest.edit',
        'uses' => 'DataChangeRequestController@edit',
        'middleware' => 'can:preselection.datachangerequests.edit'
    ]);
    $router->put('datachangerequests/{datachangerequest}', [
        'as' => 'admin.preselection.datachangerequest.update',
        'uses' => 'DataChangeRequestController@updateApi',
        'middleware' => 'can:preselection.datachangerequests.edit'
    ]);
    $router->delete('datachangerequests/{datachangerequest}', [
        'as' => 'admin.preselection.datachangerequest.destroy',
        'uses' => 'DataChangeRequestController@destroy',
        'middleware' => 'can:preselection.datachangerequests.destroy'
    ]);
    $router->bind('solvency', function ($id) {
        return app('Modules\Preselection\Repositories\SolvencyRepository')->find($id);
    });
    $router->get('solvencies', [
        'as' => 'admin.preselection.solvency.index',
        'uses' => 'SolvencyController@index',
        'middleware' => 'can:preselection.solvencies.index'
    ]);
    $router->get('solvencies_generated', [
        'as' => 'admin.preselection.solvency.generated',
        'uses' => 'SolvencyController@solvenciesGenerated',
        'middleware' => 'can:preselection.solvencies.index'
    ]);
    $router->get('solvencies/create', [
        'as' => 'admin.preselection.solvency.create',
        'uses' => 'SolvencyController@create',
        'middleware' => 'can:preselection.solvencies.create'
    ]);
    $router->post('solvencies', [
        'as' => 'admin.preselection.solvency.store',
        'uses' => 'SolvencyController@store',
        'middleware' => 'can:preselection.solvencies.create'
    ]);
    $router->get('solvencies/{solvency}/edit', [
        'as' => 'admin.preselection.solvency.edit',
        'uses' => 'SolvencyController@edit',
        'middleware' => 'can:preselection.solvencies.edit'
    ]);

    // SOLVENCIAS GENERADAS
    $router->get('solvencies/{solvency}/editSolvencyGenerated', [
        'as' => 'admin.preselection.solvency.editSolvencyGenerated',
        'uses' => 'SolvencyController@editSolvencyGenerated',
        'middleware' => 'can:preselection.solvencies.edit'
    ]);

    $router->put('solvenciesGenerated/{solvency}', [
        'as' => 'admin.preselection.solvency.updateGenerated',
        'uses' => 'SolvencyController@updateGenerated',
        'middleware' => 'can:preselection.solvencies.edit'
    ]);

    // SOLVENCIAS EN PROCESADO

    $router->put('solvencies/{solvency}', [
        'as' => 'admin.preselection.solvency.update',
        'uses' => 'SolvencyController@update',
        'middleware' => 'can:preselection.solvencies.edit'
    ]);
    $router->delete('solvencies/{solvency}', [
        'as' => 'admin.preselection.solvency.destroy',
        'uses' => 'SolvencyController@destroy',
        'middleware' => 'can:preselection.solvencies.destroy'
    ]);




// append





});
