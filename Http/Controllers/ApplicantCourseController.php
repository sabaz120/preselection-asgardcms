<?php

namespace Modules\Preselection\Http\Controllers;

use Illuminate\Http\Request;
// use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Ihelpers\Http\Controllers\Api\BaseApiController;
//Repository
use Modules\Preselection\Repositories\AspirantsRepository;
//Request
use Modules\Preselection\Http\Requests\CreateApplicantCourseRequest;
//Transactions & auth
use Auth;
use DB;
//Entities
use Modules\Preselection\Entities\Aspirants;
use Modules\Preselection\Entities\ApplicantCourse;
use Modules\Preselection\Entities\DataChangeRequest;
class ApplicantCourseController extends BaseApiController
{
  private $aspirants;
  public function __construct(AspirantsRepository $aspirants)
  {
    parent::__construct();
    $this->aspirants=$aspirants;
  }

  public function update($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant){

        throw new \Exception("No posee relación de aspirante",500);
      }
      $dataChangeRequests=DataChangeRequest::where('type','aspirant')->where('aspirant_id',$aspirant->id)->whereStatus(0)->get();
      if(count($dataChangeRequests)>0){
        throw new \Exception("Aún tienes algunas solicitudes de cambio de datos pendientes por validar.",500);
      }
      //Get data
      // $data=$request->all();
      $entity=ApplicantCourse::where('id',$id)->where('aspirant_id',$aspirant->id)->first();
      if(isset($request->institution)){
        if($request->institution!=$entity->institution){
          DataChangeRequest::create([
            'field'=>'institution',
            'type'=>'course',
            'value'=>$request->institution,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//name
      if(isset($request->hours)){
        if($request->hours!=$entity->hours){
          DataChangeRequest::create([
            'field'=>'hours',
            'type'=>'course',
            'value'=>$request->hours,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//hours
      if(isset($request->date)){
        if($request->date!=$entity->date){
          DataChangeRequest::create([
            'field'=>'date',
            'type'=>'course',
            'value'=>$request->date,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//date
      // $entity->update($data);
      DB::commit();
      return response()->json(["data"=>'','msg'=>'Solicitud de cambio de datos enviada exitosamente.'],200);
      // return response()->json(["data"=>'',"msg"=>"Datos del curso actualizados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//update()

  public function delete($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $entity=ApplicantCourse::where('id',$id)->where('aspirant_id',$aspirant->id)->delete();
      DB::commit();
      return response()->json(["data"=>'',"msg"=>"Curso eliminado exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

  public function create(Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $data=$request->all();
      $data['aspirant_id']=$aspirant->id;
      $course=ApplicantCourse::create($data);
      DB::commit();
      return response()->json(["data"=>$course,"msg"=>"Datos del curso registrados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

}
