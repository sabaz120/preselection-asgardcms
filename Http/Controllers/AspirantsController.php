<?php

namespace Modules\Preselection\Http\Controllers;

use Illuminate\Http\Request;
// use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Ihelpers\Http\Controllers\Api\BaseApiController;
//Repository
use Modules\Preselection\Repositories\AspirantsRepository;
//Request
use Modules\Preselection\Http\Requests\CreateAspirantsRequest;
//Transactions & auth
use Auth;
use DB;
//Entities
use Modules\Preselection\Entities\Aspirants;
use Modules\Preselection\Entities\DataChangeRequest;
// use Modules\Preselection\Entities\StudiesApplicant;
// use Modules\Preselection\Entities\ApplicantCourse;
// use Modules\Preselection\Entities\AspirantFamily;
class AspirantsController extends BaseApiController
{
  private $aspirants;
  public function __construct(AspirantsRepository $aspirants)
  {
    parent::__construct();
    $this->aspirants=$aspirants;
  }

  public function loginPost(Request $request){
    //Receive identification & password
    $entity=Aspirants::where('identification',$request->identification)->where('status',10)->first();
    if($entity){
      //Aspirant exists
      $credentials = [
        'email' => $entity->identification,
        'password' => $request->password,
      ];
      // dd($credentials);
      $error = $this->auth->login($credentials, false);
      if ($error) {
          return redirect()->back()->withInput()->withError($error);
      }
      return redirect()->intended(route("user.dashboard"))
              ->withSuccess(trans('user::messages.successfully logged in'));
    }else{
      return redirect()->back()->withInput()->withError("Este usuario no existe.");

    }

  }

  public function logoutGet(){
    $this->auth->logout();

    return redirect()->route('user.login');
  }

  public function update(Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $entity=Aspirants::where('identification',$user->email)->first();
      if(!$entity)
        throw new \Exception("No posee relación de aspirante",500);
      $dataChangeRequests=DataChangeRequest::where('type','aspirant')->where('aspirant_id',$entity->id)->whereStatus(0)->get();
      if(count($dataChangeRequests)>0){
        throw new \Exception("Aún tienes algunas solicitudes de cambio de datos pendientes por validar.",500);
      }
      //Get data
      // $data=$request->all();
      // $entity->update($data);
      //DataChangeRequest
      if(isset($request->name)){
        if($request->name!=$entity->name){
          DataChangeRequest::create([
            'field'=>'name',
            'type'=>'aspirant',
            'value'=>$request->name,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//name
      if(isset($request->last_name)){
        if($request->last_name!=$entity->last_name){
          DataChangeRequest::create([
            'field'=>'last_name',
            'type'=>'aspirant',
            'value'=>$request->last_name,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//last_name
      if(isset($request->second_name)){
        if($request->second_name!=$entity->second_name){
          DataChangeRequest::create([
            'field'=>'second_name',
            'type'=>'aspirant',
            'value'=>$request->second_name,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//second_name
      if(isset($request->second_surname)){
        if($request->second_surname!=$entity->second_surname){
          DataChangeRequest::create([
            'field'=>'second_surname',
            'type'=>'aspirant',
            'value'=>$request->second_surname,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//second_surname
      if(isset($request->phone)){
        if($request->phone!=$entity->phone){
          DataChangeRequest::create([
            'field'=>'phone',
            'type'=>'aspirant',
            'value'=>$request->phone,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//phone
      if(isset($request->address)){
        if($request->address!=$entity->address){
          DataChangeRequest::create([
            'field'=>'address',
            'type'=>'aspirant',
            'value'=>$request->address,
            'aspirant_id'=>$entity->id
          ]);
        }
      }//address
      DB::commit();
      return response()->json(["data"=>'','msg'=>'Solicitud de cambio de datos enviada exitosamente.'],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//update()

}
