<?php

namespace Modules\Preselection\Http\Controllers;

use Illuminate\Http\Request;
// use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Ihelpers\Http\Controllers\Api\BaseApiController;
use Modules\Preselection\Repositories\AspirantsRepository;
use Modules\Preselection\Http\Requests\CreateAspirantsRequest;
use Modules\Preselection\Http\Requests\MediaUploadRequest;
use Modules\Preselection\Http\Requests\CreateStudiesApplicant;
use Modules\Preselection\Http\Requests\CreateApplicantWorkExperience;
use Modules\Preselection\Http\Requests\CreateApplicantCourse;
use Modules\Preselection\Http\Requests\CreateAspirantFamilyRequest;
use DB;
use Modules\Preselection\Entities\Aspirants;
use Modules\Preselection\Entities\StudiesApplicant;
use Modules\Preselection\Entities\ApplicantWorkExperience;
use Modules\Preselection\Entities\ApplicantCourse;
use Modules\Preselection\Entities\AspirantFamily;
use Modules\Business\Entities\Business;
use Modules\Business\Entities\unitHability;
use Modules\Business\Entities\Unit;
use Modules\Setting\Contracts\Setting;
use Carbon\Carbon;
use Modules\Preselection\Emails\NotifyCheckingInterview;
use Illuminate\Contracts\Mail\Mailer;
class PublicController extends BaseApiController
{
  private $aspirants;
  private $setting;
  private $mail;
  public function __construct(AspirantsRepository $aspirants,Setting $setting,Mailer $mail)
  {
    parent::__construct();
      $this->mail = $mail;
    $this->aspirants=$aspirants;
    $this->setting = $setting;
  }



  //Ajax Receive in form home
  public function storeAspirant(Request $request){
    try {
      $this->validateRequestApi(new CreateAspirantsRequest($request->all()));
      $aspirant=Aspirants::where('identification',$request->identification)->first();
      if($aspirant)
        throw new \Exception("Ya te has registrado anteriormente",500);
      DB::beginTransaction();
      $data=$request->all();
      $file=$request->file('curriculum_file');
      //Filters to recommended aspirant
      $recommended=false;
      $percentage_studies=0;
      $percentage_address=0;
      $percentage_work_experience=0;
      $business=Business::find($request->business_id);
      $unit=Unit::find($request->unity_id);

      //$habilities=unitHability::where('unit_id',$request->unity_id)->whereIn('id',json_decode($request->habilities))->get();
      //if(count($habilities)==2 && $business->parish_id==$request->parish_id)
      //  $recommended=true;
      //Create
      if($business->parish_id==$request->parish_id){
        $percentage_address=(float)$this->setting->get('preselection::percentage_address');
        $recommended=true;
      }

      $aspirant=$this->aspirants->create([
        'name'=>$request->name,
        'identification'=>$request->identification,
        'last_name'=>$request->last_name,
        'second_surname'=>$request->second_surname,
        'second_name'=>$request->second_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        //'summary'=>$request->summary,
        'curriculum_file'=>"----",
        'unity_id'=>$request->unity_id,
        'sexper'=>$request->sexper,
        'nationality'=>$request->nationality,
        'parish_id'=>$request->parish_id,
        'business_id'=>$request->business_id,
        'city_id'=>$request->city_id,
        'recommended'=>$recommended,
        'options'=>json_decode($request->habilities)
      ]);
      $aspirant_id=$aspirant->id;
      $image=$request->file('curriculum_file');
      $document = "assets/preselection/aspirants/" . $aspirant->id . "/";
      \Storage::disk('publicmedia')->putFileAs($document, $request->file('curriculum_file'),$image->getClientOriginalName().".".$image->getClientOriginalExtension());
      // $this->aspirants->update($aspirant,['curriculum_file'=>$document.$image->getClientOriginalName().".".$image->getClientOriginalExtension()]);
      $studies=json_decode($request->studies);
      if(count($studies)==0)
        throw new \Exception("Debes registrar tu nivel de estudio",500);
      $courses=json_decode($request->courses);
      $workExperience=json_decode($request->workExperience);
      if(is_array($studies)){
        $b=0;
        foreach($studies as $study){
          $this->validateRequestApi(new CreateStudiesApplicant((array)$study));
          StudiesApplicant::create([
            "aspirant_id"=>$aspirant_id,
            "type_education"=>$study->type_education,
            "institution"=>$study->institution,
            "date_end"=>$study->date_end,
            "city"=>$study->city,
            "title"=>$study->title
          ]);
           if(calculateRecommendedStudy($unit->type_education,$study->type_education)){
            $b=1;
          }
        }//studies
        if($b==1){
          $recommended=true;
          $percentage_studies=(float)$this->setting->get('preselection::percentage_studies');
        }
      }
      if(is_array($courses)){
        foreach($courses as $course){
          $this->validateRequestApi(new CreateApplicantCourse((array)$course));
          ApplicantCourse::create([
            "aspirant_id"=>$aspirant_id,
            "name"=>$course->name,
            "institution"=>$course->institution,
            "hours"=>$course->hours,
            "date"=>$course->date
          ]);
        }//courses
      }
      if(is_array($workExperience)){
        $yearsWorkExperience=0;
        foreach($workExperience as $experience){
          $this->validateRequestApi(new CreateApplicantWorkExperience((array)$experience));
          ApplicantWorkExperience::create([
            "aspirant_id"=>$aspirant_id,
            "business_name"=>$experience->business_name,
            "business_address"=>$experience->business_address,
            "business_phone"=>$experience->business_phone,
            "business_contact_person"=>$experience->business_contact_person,
            "job_title"=>$experience->job_title,
            "description_functions_performed"=>$experience->description_functions_performed,
            "since"=>$experience->since,
            "until"=>$experience->until,
            "reason_for_termination"=>$experience->reason_for_termination,
            "last_salary"=>$experience->last_salary
          ]);
          $date=Carbon::parse($experience->since);
          $date2=Carbon::parse($experience->until);
          $yearsWorkExperience=$yearsWorkExperience+$date2->diffInYears($date);
        }//workExperience
        if($yearsWorkExperience>=$unit->minimal_experience){
          $percentage_work_experience=(float)$this->setting->get('preselection::percentage_work_experience');
          $recommended=true;
        }
      }//is_array(workExperience)
      $this->aspirants->update($aspirant,[
        'curriculum_file'=>$document.$image->getClientOriginalName().".".$image->getClientOriginalExtension(),
        "recommended"=>$recommended,
        "percentage_studies"=>$percentage_studies,
        "percentage_address"=>$percentage_address,
        "percentage_work_experience"=>$percentage_work_experience
      ]);
      //Email notification
      $subject="Grupo La Fundadora - Tu registro se ha procesado exitosamente.";
      $view = "preselection::emails.successfullRegistration";
      $this->mail->to($aspirant->email)->send(new NotifyCheckingInterview($aspirant,$subject,$view));
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["data"=>$request->all(),"error"=>$e->getMessage(),"line"=>$e->getLine(),"trace"=>$e->getTrace()],500);
      //return redirect()->intended(route(config('asgard.user.config.redirect_route_after_login')))->withError('Sistema no disponible actualmente.');
    }
    DB::commit();
    return response()->json(["data"=>'Solicitud registrada exitosamente'],200);
    //return redirect()->intended(route(config('asgard.user.config.redirect_route_after_login')))->withSuccess('Solicitud procesada exitosamente.');
  }//storeAspirant()

  //Ajax Receive in form home
  public function storeEmployeeAspirant(Request $request){
    try {
      $this->validateRequestApi(new CreateAspirantsRequest($request->all()));



      $aspirant=Aspirants::where('identification',$request->identification)->first();
      if($aspirant){
        throw new \Exception("Ya te has registrado anteriormente",500);
      }
      $ci_explode=explode("-",$request->identification);
      $typeIdentification=$ci_explode[0];
      $identification=$ci_explode[1];
      $identificationWithMask =number_format($identification, 0, '', '.');
      $snEmple=\Modules\Aspirantdashboard\Entities\internalProfit\Snemple::where('ci',$identification)->orWhere('ci',$identificationWithMask)->first();
      if(!$snEmple){
        throw new \Exception("No estas registrado en el sistema de nónima de la empresa.",500);
      }
      DB::beginTransaction();
      $data=$request->all();
      $file=$request->file('curriculum_file');
      //Filters to recommended aspirant
      $recommended=false;
      $percentage_studies=0;
      $percentage_address=0;
      $percentage_work_experience=0;
      $business=Business::find($request->business_id);
      $unit=Unit::find($request->unity_id);

      //$habilities=unitHability::where('unit_id',$request->unity_id)->whereIn('id',json_decode($request->habilities))->get();
      //if(count($habilities)==2 && $business->parish_id==$request->parish_id)
      //  $recommended=true;
      //Create
      if($business->parish_id==$request->parish_id){
        $percentage_address=(float)$this->setting->get('preselection::percentage_address');
        $recommended=true;
      }

      $aspirant=$this->aspirants->create([
        'name'=>$request->name,
        'identification'=>$request->identification,
        'last_name'=>$request->last_name,
        'second_surname'=>$request->second_surname,
        'second_name'=>$request->second_name,
        'email'=>$request->email,
        'phone'=>$request->phone,
        'address'=>$request->address,
        //'summary'=>$request->summary,
        'curriculum_file'=>"----",
        'unity_id'=>$request->unity_id,
        'sexper'=>$request->sexper,
        'nationality'=>$request->nationality,
        'parish_id'=>$request->parish_id,
        'business_id'=>$request->business_id,
        'city_id'=>$request->city_id,
        'recommended'=>$recommended,
        'options'=>json_decode($request->habilities),
        'status'=>10
      ]);
      $aspirant_id=$aspirant->id;
      $image=$request->file('curriculum_file');
      $document = "assets/preselection/aspirants/" . $aspirant->id . "/";
      \Storage::disk('publicmedia')->putFileAs($document, $request->file('curriculum_file'),$image->getClientOriginalName().".".$image->getClientOriginalExtension());
      // $this->aspirants->update($aspirant,['curriculum_file'=>$document.$image->getClientOriginalName().".".$image->getClientOriginalExtension()]);
      $studies=json_decode($request->studies);
      if(count($studies)==0)
        throw new \Exception("Debes registrar tu nivel de estudio",500);
      $courses=json_decode($request->courses);
      $workExperience=json_decode($request->workExperience);
      if(is_array($studies)){
        $b=0;
        foreach($studies as $study){
          $this->validateRequestApi(new CreateStudiesApplicant((array)$study));
          StudiesApplicant::create([
            "aspirant_id"=>$aspirant_id,
            "type_education"=>$study->type_education,
            "institution"=>$study->institution,
            "date_end"=>$study->date_end,
            "city"=>$study->city,
            "title"=>$study->title
          ]);
           if(calculateRecommendedStudy($unit->type_education,$study->type_education)){
            $b=1;
          }
        }//studies
        if($b==1){
          $recommended=true;
          $percentage_studies=(float)$this->setting->get('preselection::percentage_studies');
        }
      }
      if(is_array($courses)){
        foreach($courses as $course){
          $this->validateRequestApi(new CreateApplicantCourse((array)$course));
          ApplicantCourse::create([
            "aspirant_id"=>$aspirant_id,
            "name"=>$course->name,
            "institution"=>$course->institution,
            "hours"=>$course->hours,
            "date"=>$course->date
          ]);
        }//courses
      }
      if(is_array($workExperience)){
        $yearsWorkExperience=0;
        foreach($workExperience as $experience){
          $this->validateRequestApi(new CreateApplicantWorkExperience((array)$experience));
          ApplicantWorkExperience::create([
            "aspirant_id"=>$aspirant_id,
            "business_name"=>$experience->business_name,
            "business_address"=>$experience->business_address,
            "business_phone"=>$experience->business_phone,
            "business_contact_person"=>$experience->business_contact_person,
            "job_title"=>$experience->job_title,
            "description_functions_performed"=>$experience->description_functions_performed,
            "since"=>$experience->since,
            "until"=>$experience->until,
            "reason_for_termination"=>$experience->reason_for_termination,
            "last_salary"=>$experience->last_salary
          ]);
          $date=Carbon::parse($experience->since);
          $date2=Carbon::parse($experience->until);
          $yearsWorkExperience=$yearsWorkExperience+$date2->diffInYears($date);
        }//workExperience
        if($yearsWorkExperience>=$unit->minimal_experience){
          $percentage_work_experience=(float)$this->setting->get('preselection::percentage_work_experience');
          $recommended=true;
        }
      }//is_array(workExperience)
      $this->aspirants->update($aspirant,[
        'curriculum_file'=>$document.$image->getClientOriginalName().".".$image->getClientOriginalExtension(),
        "recommended"=>$recommended,
        "percentage_studies"=>$percentage_studies,
        "percentage_address"=>$percentage_address,
        "percentage_work_experience"=>$percentage_work_experience
      ]);
      //Email notification
      $subject="Grupo La Fundadora - Tu registro se ha procesado exitosamente.";
      $view = "preselection::emails.successfullRegistration";
      $this->mail->to($aspirant->email)->send(new NotifyCheckingInterview($aspirant,$subject,$view));

      $emailAspirant=$aspirant->identification;
      $user=User::where('email',$emailAspirant)->first();
      if(!$user){
        //Create user if not exists
        $randomPassword=\Illuminate\Support\Str::random(15);
        $this->user->createWithRoles([
          "first_name"=>$aspirant->name,
          "last_name"=>$aspirant->last_name,
          "email"=>$emailAspirant,
          "password"=>bcrypt($randomPassword)
        ], 2, 1);
        $subject="Grupo La Fundadora - Datos de acceso";
        $view = "preselection::emails.emailContratado";
        $this->mail->to($aspirant->email)->send(new NotifyCheckingInterview((object)[
          "first_name"=>$aspirant->name,
          "last_name"=>$aspirant->last_name,
          "email"=>$emailAspirant,
          "password"=>$randomPassword
        ],$subject,$view));
      }

    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["data"=>$request->all(),"error"=>$e->getMessage(),"line"=>$e->getLine(),"trace"=>$e->getTrace()],500);
      //return redirect()->intended(route(config('asgard.user.config.redirect_route_after_login')))->withError('Sistema no disponible actualmente.');
    }
    DB::commit();
    return response()->json(["data"=>'Solicitud registrada exitosamente'],200);
    //return redirect()->intended(route(config('asgard.user.config.redirect_route_after_login')))->withSuccess('Solicitud procesada exitosamente.');
  }//storeAspirant()

  public function show($code){
    $aspirant=Aspirants::where('code',$code)->with(['courses','families','studies','workExperience','unit','business','parish','city'])->firstOrFail();
    return view('preselection::frontend.AspirantSelected.create',['aspirant'=>$aspirant]);
  }//show(code)

  public function storeFamily(Request $request){
    try {
      $this->validateRequestApi(new CreateAspirantFamilyRequest($request->all()));
      DB::beginTransaction();
      $data=$request->all();
      if(isset($request->identification) && $request->identification){
        $aspirant=AspirantFamily::where('identification',$request->identification)->first();
        if(!$aspirant)
        $aspirant=AspirantFamily::create($data);
      }else
        $aspirant=AspirantFamily::firstOrCreate($data);
      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["data"=>$request->all(),"error"=>$e->getMessage(),"line"=>$e->getLine(),"trace"=>$e->getTrace()],500);
    }
    return response()->json(["msg"=>'Familiar agregado correctamente',"data"=>$aspirant],200);

  }

  public function mediauPload(Request $request){
    try {
      $this->validateRequestApi(new MediaUploadRequest($request->all()));
      //Validar que el estado del aspirante sea 2 (aprobado) o 4(Por asignar entrevista)
      $types=[
        'studies',
        'courses',
        'workExperience',
        'passport',
        'identification',
        'foodHandling',
        'goodConductLetter',
        'healthCertificate',
      ];
      if(in_array($request->type,$types)){
        DB::beginTransaction();
        ini_set('memory_limit', '2048M');
        $aspirant=Aspirants::where('id',$request->aspirant_id)->firstOrFail();
        if($request->type=="studies"){
          $entity=StudiesApplicant::where('id',$request->entity_id)->firstOrFail();
        }else if($request->type=="courses"){
          $entity=ApplicantCourse::where('id',$request->entity_id)->firstOrFail();
        }else if($request->type=="workExperience"){
          $entity=ApplicantWorkExperience::where('id',$request->entity_id)->firstOrFail();
        }
        $image=$request->file('file');
        $fileName=$image->getClientOriginalName().".".$image->getClientOriginalExtension();
        if($request->type!="studies" || $request->type!="courses" || $request->type!="workExperience")
          $document = "assets/preselection/aspirants/" . $request->aspirant_id . "/";
        else
          $document = "assets/preselection/aspirants/" . $request->aspirant_id . "/".$request->type."/";
        \Storage::disk('publicmedia')->putFileAs($document, $request->file('file'),$fileName);
        if($request->type=="studies" || $request->type=="courses" || $request->type=="workExperience"){
          $entity->document=$document.$fileName;
          $entity->update();
        }else{
          if($request->type=="identification"){
            $aspirant->identification_document=$document.$fileName;
            $aspirant->update();
          }else if($request->type=="passport"){
            $aspirant->passport_document=$document.$fileName;
            $aspirant->update();
          }else if($request->type=="foodHandling"){
            $aspirant->food_handling_document=$document.$fileName;
            $aspirant->update();
          }else if($request->type=="goodConductLetter"){
            $aspirant->good_conduct_letter_document=$document.$fileName;
            $aspirant->update();
          }else if($request->type=="healthCertificate"){
            $aspirant->health_certificate=$document.$fileName;
            $aspirant->update();
          }
        }
        DB::commit();
        $this->validateDocumentsToInterview($request->aspirant_id);
      }else
        throw new \Exception("Este tipo no es permitido",503);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["data"=>$request->all(),"error"=>$e->getMessage(),"line"=>$e->getLine(),"trace"=>$e->getTrace()],500);
    }
    return response()->json(["data"=>'Archivo cargado exitosamente'],200);

  }//mediaUpload()

  public function validateDocumentsToInterview($aspirant_id){
    $aspirant=Aspirants::find($aspirant_id);
    if($aspirant->status==2){
      $error=0;
      if(!$aspirant->identification){
        $error++;
      }
      $studies=0;
      foreach($aspirant->studies as $study){
        if($study->document)
          $studies++;
      }
      $work=0;
      foreach($aspirant->workExperience as $workEx){
        if($workEx->document)
          $work++;
      }
      if($error==0 && $studies>0 && $work>0){
        //Se cambia el estado del aspirante y se crea una fecha tentativa de la entrevista
        $aspirant->status=4;//Por asignar entrevista
        $date=\Carbon\Carbon::now();
        $date=interviews__getDateInterview($date);
        $interview=\Modules\Interviews\Entities\Interview::create([
          "date"=>$date,
          "aspirant_id"=>$aspirant_id
        ]);
        $aspirant->update();
        $aspirants=Aspirants::find($aspirant_id);
        $subject="Atención: En breve le notificaremos la fecha de entrevista";
        $view = "preselection::emails.checkingInterview";
        $this->mail->to($aspirants->email)->send(new NotifyCheckingInterview($aspirants,$subject,$view));

      }
    }//if status==2
  }//validateDocumentsToInterview()

  public function updateWorkExperience($id){
    $entity=ApplicantWorkExperience::where('id',$id)->firstOrFail();
    $msg="";
    if($entity->confirmed){
      $entity->confirmed=false;
      $msg="Confirmación de experiencia laboral eliminada";
    }
    else{
      $msg="Confirmación de experiencia laboral procesada exitosamente";
      $entity->confirmed=true;
    }
    $entity->update();
    return response()->json(["msg"=>$msg],200);
  }//updateWorkExperience()

  public function showEmployee(){
    return view('preselection::frontend.employee.index');
  }

}
