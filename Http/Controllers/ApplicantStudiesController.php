<?php

namespace Modules\Preselection\Http\Controllers;

use Illuminate\Http\Request;
// use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Ihelpers\Http\Controllers\Api\BaseApiController;
//Repository
use Modules\Preselection\Repositories\AspirantsRepository;
//Request
use Modules\Preselection\Http\Requests\CreateAspirantsRequest;
//Transactions & auth
use Auth;
use DB;
//Entities
use Modules\Preselection\Entities\Aspirants;
use Modules\Preselection\Entities\StudiesApplicant;
use Modules\Preselection\Entities\DataChangeRequest;
class ApplicantStudiesController extends BaseApiController
{
  private $aspirants;
  public function __construct(AspirantsRepository $aspirants)
  {
    parent::__construct();
    $this->aspirants=$aspirants;
  }

  public function update($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant){
        throw new \Exception("No posee relación de aspirante",500);
      }
      $dataChangeRequests=DataChangeRequest::where('type','aspirant')->where('aspirant_id',$aspirant->id)->whereStatus(0)->get();
      if(count($dataChangeRequests)>0){
        throw new \Exception("Aún tienes algunas solicitudes de cambio de datos pendientes por validar.",500);
      }
      //Get data
      // $data=$request->all();
      $entity=StudiesApplicant::where('id',$id)->where('aspirant_id',$aspirant->id)->first();
      if(isset($request->type_education)){
        if($request->type_education!=$entity->type_education){
          DataChangeRequest::create([
            'field'=>'type_education',
            'type'=>'study',
            'value'=>$request->type_education,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//type_education
      if(isset($request->institution)){
        if($request->institution!=$entity->institution){
          DataChangeRequest::create([
            'field'=>'institution',
            'type'=>'study',
            'value'=>$request->institution,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//type_education
      if(isset($request->date_end)){
        if($request->date_end!=$entity->date_end){
          DataChangeRequest::create([
            'field'=>'date_end',
            'type'=>'study',
            'value'=>$request->date_end,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//date_end
      if(isset($request->city)){
        if($request->city!=$entity->city){
          DataChangeRequest::create([
            'field'=>'city',
            'type'=>'study',
            'value'=>$request->city,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//city
      if(isset($request->title)){
        if($request->title!=$entity->title){
          DataChangeRequest::create([
            'field'=>'title',
            'type'=>'study',
            'value'=>$request->title,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//title
      // $entity->update($data);
      DB::commit();
      return response()->json(["data"=>'','msg'=>'Solicitud de cambio de datos enviada exitosamente.'],200);
      // return response()->json(["data"=>'',"msg"=>"Datos de estudio actualizados correctamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//update()

  public function delete($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $entity=StudiesApplicant::where('id',$id)->where('aspirant_id',$aspirant->id)->delete();
      DB::commit();
      return response()->json(["data"=>'',"msg"=>"Datos de estudio eliminados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

  public function create(Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $data=$request->all();
      $data['aspirant_id']=$aspirant->id;
      $entity=StudiesApplicant::create($data);
      DB::commit();
      return response()->json(["data"=>$entity,"msg"=>"Datos de estudio registrados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

}
