<?php

namespace Modules\Preselection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//Entities
use Modules\User\Entities\Sentinel\User;
use Modules\Preselection\Entities\Aspirants;
use Modules\Preselection\Entities\ApplicantCourse;
use Modules\Preselection\Entities\ApplicantWorkExperience;
use Modules\Preselection\Entities\StudiesApplicant;
use Modules\Preselection\Entities\Solvency;
use Modules\Preselection\Entities\AspirantStatus;
use Modules\Interviews\Entities\Interview;
//Requests
use Modules\Preselection\Http\Requests\CreateAspirantsRequest;
use Modules\Preselection\Http\Requests\UpdateAspirantsRequest;
//Repositories
use Modules\Preselection\Repositories\AspirantsRepository;
use Modules\User\Repositories\UserRepository;
//Controller
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
//Emails
use Modules\Preselection\Emails\NotifyPreSelected;
use Modules\Preselection\Emails\NotifyInterview;
use Modules\Preselection\Emails\NotifyCheckingInterview;
use Modules\Preselection\Emails\NotifyAspirantMedicalExam;
use Modules\Preselection\Emails\NotifySolvency;
//Facades
use Illuminate\Contracts\Mail\Mailer;
use Modules\Setting\Contracts\Setting;

class AspirantsController extends AdminBaseController
{
  /**
  * @var AspirantsRepository
  */
  private $aspirants;
  private $mail;
  private $setting;
  private $user;

  public function __construct(AspirantsRepository $aspirants,Mailer $mail,Setting $setting,UserRepository $user)
  {
    parent::__construct();
    $this->mail = $mail;
    $this->setting = $setting;

    $this->user = $user;
    $this->aspirants = $aspirants;
  }

  /**
  * Display a listing of the resource.
  *
  * @return Response
  */
  public function index()
  {
    $aspirants = Aspirants::with(['courses','families','studies','workExperience','unit','business','parish','city'])->get();
    return view('preselection::admin.aspirants.index', compact('aspirants'));
  }

  public function indexDoctor()
  {
    $aspirants = Aspirants::with(['courses','families','studies','workExperience','unit','business','parish','city'])->where('status',9)->get();
    return view('preselection::admin.aspirantsMedicalExam.index', compact('aspirants'));
  }

  public function indexWorkers()
  {
    $aspirants = Aspirants::with(['courses','families','studies','workExperience','unit','business','parish','city'])->where('status',10)->get();
    return view('preselection::admin.workers.index', compact('aspirants'));
  }
  public function retiredWorkers()
  {
    $aspirants = Aspirants::with(['courses','families','studies','workExperience','unit','business','parish','city'])
    ->where('status',11)->whereHas('solvencies',function($query){
      $query->where('status',1)->where('status_payment',2);
    })->get();
    return view('preselection::admin.retiredWorkers.index', compact('aspirants'));
  }
  public function getCoordinators(Request $request)
  {
    $aspirants = Aspirants::with(['courses','families','studies','workExperience','unit','business','parish','city'])
    ->where('business_id',$request->business_id)->where('unity_id',75)->get();
    return response()->json($aspirants,200);
  }

  public function getAspirants(Request $request){
    $query=Aspirants::query();
    if ($request->date !=null) {
      $query->whereDate('created_at','>=',$request->date);
    } if($request->date_end !=null){
      $query->whereDate('created_at','<=',$request->date_end);
    } if($request->status_id !=0){
      $query->where('status',$request->status_id);
    }
    if($request->business_id !=0 && $request->unit_id ==0){
      $query->where('business_id',$request->business_id);
    }

    if($request->unit_id !=0){
      $query->where('unity_id',$request->unit_id);
    }

    if ($request->recommended_by) {
      foreach($request->recommended_by  as $recommended){
        if ($recommended == 1) {
          $query->where('percentage_studies','!=',0.00);
        }
        if ($recommended ==2) {
          $query->where('percentage_work_experience','!=',0.00);
        }
        if ($recommended ==3) {
          $query->where('percentage_address','!=',0.00);
        }
      }
    }

    if ($request->state_id !=0) {
      $state_id = $request->state_id;
      $query->whereHas('parish',function($query) use($state_id){
        $query->whereHas('municipality',function($query) use($state_id){
          $query->whereHas('state',function($query) use($state_id){
            $query->where('id',$state_id);
          });
        });
      });
    }
    $query->with(['courses','families','studies','workExperience','unit','business','parish','city']);
    if(isset($request->select)){
      $query->select($request->select);
    }
    $aspirants=$query->get();

    return response()->json($aspirants,200);
  }

  public function getAspirants2(Request $request){
    $query=Aspirants::query();
    if ($request->date !=null) {
      $query->whereDate('created_at','>=',$request->date);
    } if($request->date_end !=null){
      $query->whereDate('created_at','<=',$request->date_end);
    } if($request->status_id !=0){
      $query->where('status',$request->status_id);
    }
    if($request->business_id !=0 && $request->unit_id ==0){
      $query->where('business_id',$request->business_id);
    }

    if($request->unit_id !=0){
      $query->where('unity_id',$request->unit_id);
    }

    if ($request->recommended_by) {
      foreach($request->recommended_by  as $recommended){
        if ($recommended == 1) {
          $query->where('percentage_studies','!=',0.00);
        }
        if ($recommended ==2) {
          $query->where('percentage_work_experience','!=',0.00);
        }
        if ($recommended ==3) {
          $query->where('percentage_address','!=',0.00);
        }
      }
    }

    if ($request->state_id !=0) {
      $state_id = $request->state_id;
      $query->whereHas('parish',function($query) use($state_id){
        $query->whereHas('municipality',function($query) use($state_id){
          $query->whereHas('state',function($query) use($state_id){
            $query->where('id',$state_id);
          });
        });
      });
    }

    if(isset($request->search)){
      $query->where('name',"LIKE","%".$request->search."%")
      ->orWhere('identification',"LIKE","%".$request->search."%")
      ->orWhere('last_name',"LIKE","%".$request->search."%");
    }

    $query->with(['unit','business','parish']);
    if(isset($request->select)){
      $query->select($request->select);
    }
    $aspirants=$query->paginate();
    $data=[
      "aspirants"=>$aspirants,
      "paginate"=>[
        "total" => $aspirants->total(),
        "lastPage" => $aspirants->lastPage(),
        "perPage" => $aspirants->perPage(),
        "currentPage" => $aspirants->currentPage()
      ],
    ];
    return response()->json($data,200);
  }
  /**
  * Show the form for creating a new resource.
  *
  * @return Response
  */
  public function create()
  {
    return view('preselection::admin.aspirants.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  CreateAspirantsRequest $request
  * @return Response
  */
  public function store(CreateAspirantsRequest $request)
  {
    $this->aspirants->create($request->all());

    return redirect()->route('admin.preselection.aspirants.index')
    ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('preselection::aspirants.title.aspirants')]));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  Aspirants $aspirants
  * @return Response
  */
  public function edit(Aspirants $aspirants)
  {
    $identification=explode("-",$aspirants->identification);
    $cneData=getCneData($identification[0],$identification[1]);
    if($aspirants->cne_query==false || $aspirants->cne_query==0){
      if($cneData!=null){
        if($cneData['error']==0)
        $this->aspirants->update($aspirants, ["cne_query"=>true,"cne_data"=>$cneData['nombres']]);
        else
        $this->aspirants->update($aspirants, ["cne_query"=>true]);
      }
    }
    $interview=null;
    // if($aspirants->status==4)
    $implements = json_encode($aspirants->implements);

    $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
    return view('preselection::admin.aspirants.edit', compact('aspirants','interview','implements'));
  }

  public function edit2(Aspirants $aspirants)
  {
    $identification=explode("-",$aspirants->identification);
    $cneData=getCneData($identification[0],$identification[1]);
    if($aspirants->cne_query==false || $aspirants->cne_query==0){
      if($cneData!=null){
        if($cneData['error']==0)
        $this->aspirants->update($aspirants, ["cne_query"=>true,"cne_data"=>$cneData['nombres']]);
        else
        $this->aspirants->update($aspirants, ["cne_query"=>true]);
      }
    }
    $interview=null;
    // if($aspirants->status==4)
    $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
    return view('preselection::admin.aspirantsMedicalExam.edit2', compact('aspirants','interview'));
  }

  public function edit3(Aspirants $aspirants)
  {
    $identification=explode("-",$aspirants->identification);
    $cneData=getCneData($identification[0],$identification[1]);
    if($aspirants->cne_query==false || $aspirants->cne_query==0){
      if($cneData!=null){
        if($cneData['error']==0)
        $this->aspirants->update($aspirants, ["cne_query"=>true,"cne_data"=>$cneData['nombres']]);
        else
        $this->aspirants->update($aspirants, ["cne_query"=>true]);
      }
    }
    $humanTalentCoordinator= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->whereIn('unity_id',[75,76])->get(); //GERENTE Y COORDINADOR DE TALENTO HUMANO
    $administration= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->whereIn('unity_id',[9,10])->get(); //GERENTE Y COORDINADOR DE ADMINISTRACION
    $treasury= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->where('unity_id',15)->get(); //TESORERIA
    $occupationalMedicine= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->where('unity_id',91)->get(); //MEDICINA OCUPACIONAL
    $interview=null;
    if ($aspirants->implements == []) {
      $implements = json_encode($aspirants->implements);
    }else{
      $implements = $aspirants->implements;
    }
    // if($aspirants->status==4)
    $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
    return view('preselection::admin.workers.edit3', compact('aspirants','interview','humanTalentCoordinator','administration','treasury','occupationalMedicine','implements'));
  }

  public function editRetired(Aspirants $aspirants)
  {
    $identification=explode("-",$aspirants->identification);
    $cneData=getCneData($identification[0],$identification[1]);
    if($aspirants->cne_query==false || $aspirants->cne_query==0){
      if($cneData!=null){
        if($cneData['error']==0)
        $this->aspirants->update($aspirants, ["cne_query"=>true,"cne_data"=>$cneData['nombres']]);
        else
        $this->aspirants->update($aspirants, ["cne_query"=>true]);
      }
    }
    $humanTalentCoordinator= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->whereIn('unity_id',[75,76])->get(); //GERENTE Y COORDINADOR DE TALENTO HUMANO
    $administration= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->whereIn('unity_id',[9,10])->get(); //GERENTE Y COORDINADOR DE ADMINISTRACION
    $treasury= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->where('unity_id',15)->get(); //TESORERIA
    $occupationalMedicine= Aspirants::with('unit')->where('status',10)->where('business_id',$aspirants->business_id)->where('unity_id',15)->get(); //MEDICINA OCUPACIONAL
    $interview=null;
    if ($aspirants->implements == []) {
      $implements = json_encode($aspirants->implements);
    }else{
      $implements = $aspirants->implements;
    }
    // if($aspirants->status==4)
    $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
    return view('preselection::admin.retiredWorkers.edit', compact('aspirants','interview','humanTalentCoordinator','administration','treasury','occupationalMedicine','implements'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  Aspirants $aspirants
  * @param  UpdateAspirantsRequest $request
  * @return Response
  */
  public function update(Aspirants $aspirants, UpdateAspirantsRequest $request)

  {
    $data=$request->all();
    /*if ($data['implements']) {
      $data['implements']= json_decode($data['implements']);
    }*/
    //Si el estado enviado es aprobado
    if($request->status==2 && $aspirants->status!=2){
      $subject="Solicitud # ".$aspirants->id."-".time();
      $view = "preselection::emails.selectionAspirants";
      $codeAccess=\Illuminate\Support\Str::random(20);
      $data['code']=$codeAccess;
      $this->mail->to($aspirants->email)->send(new NotifyPreSelected($aspirants,$subject,$view,$codeAccess));
    }
    //Si el estado es: Entrevista aprobada (6) o Rechazada (7)
    if($request->status==6 && $aspirants->status!=6){
      Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->update(["status"=>2]);
    }
    else if($request->status==7 && $aspirants->status!=7){

      $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->update(["status"=>3]);
    }

    if(isset($request->criminal_record)){
      if($aspirants->criminal_record_date){
        if($request->criminal_record!=$aspirants->criminal_record){
          $data['criminal_record_date']=\Carbon\Carbon::now();
        }
      }else{
        $data['criminal_record_date']=\Carbon\Carbon::now();
      }
    }//isset criminal_record

    if(isset($request->medical_exam_file)){
      $file = $request->file('medical_exam_file');
      $extension = $file->getClientOriginalExtension();
      $nameFile =\Illuminate\Support\Str::random(32);
      $allowedextensions = array('JPG', 'JPEG', 'PNG', 'GIF', 'ICO', 'BMP');
      $name = $nameFile . '.' . $extension;
      $destination_path = 'assets/preselection/aspirants/medical_exam/'.$name;
      $disk = 'publicmedia';
      if (!in_array(strtoupper($extension), $allowedextensions)) {
        throw new Exception("Extensión del archivo no permitida");
      }
      if (in_array(strtoupper($extension), ['JPG', 'JPEG'])) {
        $image = \Image::make($file);

        \Storage::disk($disk)->put($destination_path, $image->stream($extension, '90'));
      } else {

        \Storage::disk($disk)->put($destination_path, \File::get($file));
      }
      $data['medical_exam_file']=$destination_path;
    }
    $notifyMedicalExam=0;
    if((int)$request->status==9 && $aspirants->status==8){
      $notifyMedicalExam=1;
    }
    $this->aspirants->update($aspirants, $data);
    if($notifyMedicalExam){
      $aspirant=Aspirants::find($aspirants->id);
      //En proceso de examen médico
      /*
      * Correo al aspirante con información de:
      * Dirección del laboratorio (settings)
      * Fecha de el examen médico (medical_exam_date)
      * Hora del examén medico (medical_exam_hour)
      */
      $subject="Grupo La Fundadora - Información para examen médico.";
      $view = "preselection::emails.aspirantsMedicalExam";
      $laboratoryAddress=$this->setting->get('preselection::laboratory_address');
      $this->mail->to($aspirant->email)->send(new NotifyAspirantMedicalExam($aspirant,$subject,$view,$laboratoryAddress));
      //Correo al doctor con información del aspirante que se hará el examen médico.
      /*
      * Correo al doctor con información del aspirante:
      */
      $emailDoctor=$this->setting->get('preselection::doctor_email');
      $subject="Grupo La Fundadora - Aspirante por realizar examén médico";
      $view = "preselection::emails.doctorMedicalExam";
      $this->mail->to($emailDoctor)->send(new NotifyPreSelected($aspirant,$subject,$view,"1234"));

    }

    if(isset($request->date)){
      $interview=Interview::where('id',$request->interview_id)->update(["date"=>$request->date,"hour"=>$request->hour]);
      //Aqui debe enviar el correo al aspirante de su entrevista
      $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
      $subject="Grupo La Fundadora - Has sido seleccionado a entrevista";
      $view = "preselection::emails.interviewAspirants";
      $this->mail->to($aspirants->email)->send(new NotifyInterview($aspirants,$subject,$view,$interview->date,$interview->hour));
    }//
    // V-12496780
    // NEu6IpSV
    \Log::error($aspirants->status);
    \Log::error($request->status);
    if($aspirants->status==10 || $request->status==10){
      $emailAspirant=$aspirants->identification;
      $user=User::where('email',$emailAspirant)->first();
      if(!$user){
        //Create user if not exists
        $randomPassword=\Illuminate\Support\Str::random(8);
        $this->user->createWithRoles([
          "first_name"=>$aspirants->name,
          "last_name"=>$aspirants->last_name,
          "email"=>$emailAspirant,
          "password"=>(string)$randomPassword
        ], 2, 1);
        \Log::error((string)$emailAspirant);
        \Log::error((string)$randomPassword);
        $subject="Grupo La Fundadora - Datos de acceso";
        $view = "preselection::emails.emailContratado";
        $this->mail->to($aspirants->email)->send(new NotifyCheckingInterview((object)[
          "first_name"=>$aspirants->name,
          "last_name"=>$aspirants->last_name,
          "email"=>$emailAspirant,
          "password"=>$randomPassword
        ],$subject,$view));
      }//if no exist user
    }//if aspirant status == 10

    return redirect()->route('admin.preselection.aspirants.index')
    ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::aspirants.title.aspirants')]));
  }


  public function update2(Aspirants $aspirants, UpdateAspirantsRequest $request)
  {
    $data=$request->all();
    if ($data['implements']) {
      $data['implements']= json_decode($data['implements']);
    }
    //Si el estado enviado es aprobado
    if($request->status==2 && $aspirants->status!=2){
      $subject="Solicitud # ".$aspirants->id."-".time();
      $view = "preselection::emails.selectionAspirants";
      $codeAccess=\Illuminate\Support\Str::random(20);
      $data['code']=$codeAccess;
      $this->mail->to($aspirants->email)->send(new NotifyPreSelected($aspirants,$subject,$view,$codeAccess));
    }
    //Si el estado es: Entrevista aprobada (6) o Rechazada (7)
    if($request->status==6 && $aspirants->status!=6){
      Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->update(["status"=>2]);
    }
    else if($request->status==7 && $aspirants->status!=7){

      $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->update(["status"=>3]);
    }

    if(isset($request->criminal_record)){
      if($aspirants->criminal_record_date){
        if($request->criminal_record!=$aspirants->criminal_record){
          $data['criminal_record_date']=\Carbon\Carbon::now();
        }
      }else{
        $data['criminal_record_date']=\Carbon\Carbon::now();
      }
    }//isset criminal_record

    if(isset($request->medical_exam_file)){
      $file = $request->file('medical_exam_file');
      $extension = $file->getClientOriginalExtension();
      $nameFile =\Illuminate\Support\Str::random(32);
      $allowedextensions = array('JPG', 'JPEG', 'PNG', 'GIF', 'ICO', 'BMP');
      $name = $nameFile . '.' . $extension;
      $destination_path = 'assets/preselection/aspirants/medical_exam/'.$name;
      $disk = 'publicmedia';
      if (!in_array(strtoupper($extension), $allowedextensions)) {
        throw new Exception("Extensión del archivo no permitida");
      }
      if (in_array(strtoupper($extension), ['JPG', 'JPEG'])) {
        $image = \Image::make($file);

        \Storage::disk($disk)->put($destination_path, $image->stream($extension, '90'));
      } else {

        \Storage::disk($disk)->put($destination_path, \File::get($file));
      }
      $data['medical_exam_file']=$destination_path;
    }
    $notifyMedicalExam=0;
    if((int)$request->status==9 && $aspirants->status==8){
      $notifyMedicalExam=1;
    }
    if($request->status ==11){
      if(!$request->user1_id){
        return redirect()->back()->withError('Debes seleccionar una persona de talento humano');
      }
    }
    $this->aspirants->update($aspirants, $data);
    if($notifyMedicalExam){
      $aspirant=Aspirants::find($aspirants->id);
      //En proceso de examen médico
      /*
      * Correo al aspirante con información de:
      * Dirección del laboratorio (settings)
      * Fecha de el examen médico (medical_exam_date)
      * Hora del examén medico (medical_exam_hour)
      */
      $subject="Grupo La Fundadora - Información para examen médico.";
      $view = "preselection::emails.aspirantsMedicalExam";
      $laboratoryAddress=$this->setting->get('preselection::laboratory_address');
      $this->mail->to($aspirant->email)->send(new NotifyAspirantMedicalExam($aspirant,$subject,$view,$laboratoryAddress));
      //Correo al doctor con información del aspirante que se hará el examen médico.
      /*
      * Correo al doctor con información del aspirante:
      */
      $emailDoctor=$this->setting->get('preselection::doctor_email');
      $subject="Grupo La Fundadora - Aspirante por realizar examén médico";
      $view = "preselection::emails.doctorMedicalExam";
      $this->mail->to($emailDoctor)->send(new NotifyPreSelected($aspirant,$subject,$view,"1234"));

    }



    if(isset($request->date)){
      $interview=Interview::where('id',$request->interview_id)->update(["date"=>$request->date,"hour"=>$request->hour]);
      //Aqui debe enviar el correo al aspirante de su entrevista
      $interview=Interview::whereStatus(1)->where('aspirant_id',$aspirants->id)->first();
      $subject="Grupo La Fundadora - Has sido seleccionado a entrevista";
      $view = "preselection::emails.interviewAspirants";
      $this->mail->to($aspirants->email)->send(new NotifyInterview($aspirants,$subject,$view,$interview->date,$interview->hour));
    }//

    if($aspirants->status==10 || $request->status==10){
      $emailAspirant=$aspirants->identification;
      $user=User::where('email',$emailAspirant)->first();
      if(!$user){
        //Create user if not exists
        $randomPassword=\Illuminate\Support\Str::random(15);
        $this->user->createWithRoles([
          "first_name"=>$aspirants->name,
          "last_name"=>$aspirants->last_name,
          "email"=>$emailAspirant,
          "password"=>bcrypt($randomPassword)
        ], 2, 1);

      }//if no exist user
    }//if aspirant status == 10

    // Si el estado enviado es liquitado 11
    if($request->status ==11){
      $solvency=Solvency::create([
        "aspirant_id"=>$request->aspirant_id,
        "reason"=>$request->reason,
        "observation"=>$request->observation,
        "user1_id"=>$request->user1_id,
        "user2_id"=>$request->user2_id,
        "user3_id"=>$request->user3_id,
        "user4_id"=>$request->user4_id,
      ]);

      $subject="Grupo La Fundadora - Se ha generado una solvencia para ".$aspirants->name." ".$aspirants->last_name;
      $view = "preselection::emails.notifySolvencyGenerated";
      $user_1=Aspirants::find($request->user1_id);
      $user_2=Aspirants::find($request->user2_id);
      $user_3=Aspirants::find($request->user3_id);
      $user_4=Aspirants::find($request->user4_id);
      if($user_1)
      $this->mail->to($user_1->email)->send(new NotifySolvency($solvency,$subject,$view));
      if($user_2)
      $this->mail->to($user_2->email)->send(new NotifySolvency($solvency,$subject,$view));
      if($user_3)
      $this->mail->to($user_3->email)->send(new NotifySolvency($solvency,$subject,$view));
      if($user_4)
      $this->mail->to($user_4->email)->send(new NotifySolvency($solvency,$subject,$view));
    }

    AspirantStatus::create([
      "aspirant_id"=>$aspirants->id,
      "status"=>$request->status,
      "user_id"=>\Auth::user()->id
    ]);


    return redirect()->route('admin.preselection.workers.index')
    ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::aspirants.title.aspirants')]));
  }

  public function updateRetiredWorkers(Aspirants $aspirants, Request $request)
  {
    if($request->unity_id && $request->business_id){

      $data=$request->all();


      $this->aspirants->update($aspirants, $data);

      if($request->status==10){
        $emailAspirant=$aspirants->identification;
        $user=User::where('email',$emailAspirant)->first();
        $randomPassword=\Illuminate\Support\Str::random(15);
        if(!$user){
          //Create user if not exists
          $this->user->createWithRoles([
            "first_name"=>$aspirants->name,
            "last_name"=>$aspirants->last_name,
            "email"=>$emailAspirant,
            "password"=>bcrypt($randomPassword)
          ], 2, 1);
          \Log::error((string)$emailAspirant);
          \Log::error((string)$randomPassword);
          $subject="Grupo La Fundadora - Datos de acceso";
          $view = "preselection::emails.emailContratado";
          $this->mail->to($aspirants->email)->send(new NotifyCheckingInterview((object)[
            "first_name"=>$aspirants->name,
            "last_name"=>$aspirants->last_name,
            "email"=>$emailAspirant,
            "password"=>$randomPassword
          ],$subject,$view));
        }//if no exist user
        else{
          $user->password=bcrypt($randomPassword);
          $user->update();
          \Log::error((string)$emailAspirant);
          \Log::error((string)$randomPassword);
          $subject="Grupo La Fundadora - Datos de acceso";
          $view = "preselection::emails.emailContratado";
          $this->mail->to($aspirants->email)->send(new NotifyCheckingInterview((object)[
            "first_name"=>$aspirants->name,
            "last_name"=>$aspirants->last_name,
            "email"=>$emailAspirant,
            "password"=>$randomPassword
          ],$subject,$view));
        }
      }//if aspirant status == 10

      AspirantStatus::create([
        "aspirant_id"=>$aspirants->id,
        "status"=>$request->status,
        "user_id"=>\Auth::user()->id
      ]);

    }else if($request->business_id){
      return redirect()->back()
      ->withError("Debes seleccionar una empresa");
    }else if($request->unity_id){
      return redirect()->back()
      ->withError("Debes seleccionar un cargo");
    }
    return redirect()->route('admin.preselection.workers.index')
    ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::aspirants.title.aspirants')]));
  }

  public function getWorkers(Request $request){
    $workers=Aspirants::where('unity_id',$request->unity_id)->where('status',10)->get();
    return response()->json($workers,200);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  Aspirants $aspirants
  * @return Response
  */

  public function testEmail(){
    $solvency = Solvency::find(1);
    dd($solvency);
    return view('preselection::emails.notifySolvencyGenerated',compact('solvency'));
  }




  public function destroy(Aspirants $aspirants)
  {

    $ApplicantCourse=ApplicantCourse::where('aspirant_id',$aspirants->id)->delete();
    $ApplicantWorkExperience=ApplicantWorkExperience::where('aspirant_id',$aspirants->id)->delete();
    $StudiesApplicant=StudiesApplicant::where('aspirant_id',$aspirants->id)->delete();


    $interviews=\Modules\Interviews\Entities\Interview::where('aspirant_id',$aspirants->id)->get();
    foreach($interviews as $interview){
      \Modules\Interviews\Entities\InterviewDetail::where('interview_id',$interview->id)->delete();
      \Modules\Interviews\Entities\InterviewHistory::where('interview_id',$interview->id)->delete();
    }
    \Modules\Interviews\Entities\Interview::where('aspirant_id',$aspirants->id)->delete();
    StudiesApplicant::where('aspirant_id',$aspirants->id)->delete();
    $this->aspirants->destroy($aspirants);
    return redirect()->route('admin.preselection.aspirants.index')
    ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('preselection::aspirants.title.aspirants')]));
  }
}
