<?php

namespace Modules\Preselection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Preselection\Entities\DataChangeRequest;
use Modules\Preselection\Http\Requests\CreateDataChangeRequestRequest;
use Modules\Preselection\Http\Requests\UpdateDataChangeRequestRequest;
use Modules\Preselection\Repositories\DataChangeRequestRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class DataChangeRequestController extends AdminBaseController
{
    /**
     * @var DataChangeRequestRepository
     */
    private $datachangerequest;

    public function __construct(DataChangeRequestRepository $datachangerequest)
    {
        parent::__construct();

        $this->datachangerequest = $datachangerequest;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $datachangerequests = $this->datachangerequest->all();
        $datachangerequests = datachangerequest::with('aspirant')->groupBy('aspirant_id')->get();
        // dd($datachangerequests);
        return view('preselection::admin.datachangerequests.index', compact('datachangerequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('preselection::admin.datachangerequests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateDataChangeRequestRequest $request
     * @return Response
     */
    public function store(CreateDataChangeRequestRequest $request)
    {
        $this->datachangerequest->create($request->all());

        return redirect()->route('admin.preselection.datachangerequest.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('preselection::datachangerequests.title.datachangerequests')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  DataChangeRequest $datachangerequest
     * @return Response
     */
    public function edit(DataChangeRequest $datachangerequest)
    {
        $aspirant=$datachangerequest->aspirant;
        $datachangerequests = datachangerequest::with('aspirant')->get();
        // $datachangerequests2=$datachangerequests->toJson();
        return view('preselection::admin.datachangerequests.edit', compact('aspirant','datachangerequests'));
    }

    public function updateApi($id,Request $request){
      return response()->json(["data"=>$request->all()]);
    }//updateApi()

    /**
     * Update the specified resource in storage.
     *
     * @param  DataChangeRequest $datachangerequest
     * @param  UpdateDataChangeRequestRequest $request
     * @return Response
     */
    // public function update(DataChangeRequest $datachangerequest, UpdateDataChangeRequestRequest $request)
    // {
    //     $this->datachangerequest->update($datachangerequest, $request->all());
    //
    //     return redirect()->route('admin.preselection.datachangerequest.index')
    //         ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::datachangerequests.title.datachangerequests')]));
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DataChangeRequest $datachangerequest
     * @return Response
     */
    public function destroy(DataChangeRequest $datachangerequest)
    {
        $this->datachangerequest->destroy($datachangerequest);

        return redirect()->route('admin.preselection.datachangerequest.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('preselection::datachangerequests.title.datachangerequests')]));
    }
}
