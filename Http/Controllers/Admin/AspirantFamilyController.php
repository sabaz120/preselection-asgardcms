<?php

namespace Modules\Preselection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Preselection\Entities\AspirantFamily;
use Modules\Preselection\Http\Requests\CreateAspirantFamilyRequest;
use Modules\Preselection\Http\Requests\UpdateAspirantFamilyRequest;
use Modules\Preselection\Repositories\AspirantFamilyRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class AspirantFamilyController extends AdminBaseController
{
    /**
     * @var AspirantFamilyRepository
     */
    private $aspirantfamily;

    public function __construct(AspirantFamilyRepository $aspirantfamily)
    {
        parent::__construct();

        $this->aspirantfamily = $aspirantfamily;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$aspirantfamilies = $this->aspirantfamily->all();

        return view('preselection::admin.aspirantfamilies.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('preselection::admin.aspirantfamilies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAspirantFamilyRequest $request
     * @return Response
     */
    public function store(CreateAspirantFamilyRequest $request)
    {
        $this->aspirantfamily->create($request->all());

        return redirect()->route('admin.preselection.aspirantfamily.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('preselection::aspirantfamilies.title.aspirantfamilies')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  AspirantFamily $aspirantfamily
     * @return Response
     */
    public function edit(AspirantFamily $aspirantfamily)
    {
        return view('preselection::admin.aspirantfamilies.edit', compact('aspirantfamily'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AspirantFamily $aspirantfamily
     * @param  UpdateAspirantFamilyRequest $request
     * @return Response
     */
    public function update(AspirantFamily $aspirantfamily, UpdateAspirantFamilyRequest $request)
    {
        $this->aspirantfamily->update($aspirantfamily, $request->all());

        return redirect()->route('admin.preselection.aspirantfamily.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::aspirantfamilies.title.aspirantfamilies')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AspirantFamily $aspirantfamily
     * @return Response
     */
    public function destroy(AspirantFamily $aspirantfamily)
    {
        $this->aspirantfamily->destroy($aspirantfamily);

        return redirect()->route('admin.preselection.aspirantfamily.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('preselection::aspirantfamilies.title.aspirantfamilies')]));
    }
}
