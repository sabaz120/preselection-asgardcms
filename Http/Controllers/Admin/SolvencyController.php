<?php

namespace Modules\Preselection\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Preselection\Entities\Solvency;
use Modules\Preselection\Http\Requests\CreateSolvencyRequest;
use Modules\Preselection\Http\Requests\UpdateSolvencyRequest;
use Modules\Preselection\Repositories\SolvencyRepository;
use Modules\Preselection\Repositories\AspirantsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Preselection\Entities\Aspirants;
use Carbon\Carbon;

class SolvencyController extends AdminBaseController
{
    /**
     * @var SolvencyRepository
     */
    private $solvency;
    private $aspirants;

    public function __construct(SolvencyRepository $solvency,AspirantsRepository $aspirants)
    {
        parent::__construct();

        $this->solvency = $solvency;
        $this->aspirants = $aspirants;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
          $solvencies = Solvency::with('aspirant')->where('status',0)->get();
        return view('preselection::admin.solvencies.index', compact('solvencies'));
    }

    public function solvenciesGenerated()
    {
          $solvencies = Solvency::with('aspirant')->where('status',1)->get();
        return view('preselection::admin.solvencies.indexGenerated', compact('solvencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('preselection::admin.solvencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSolvencyRequest $request
     * @return Response
     */
    public function store(CreateSolvencyRequest $request)
    {
        $this->solvency->create($request->all());

        return redirect()->route('admin.preselection.solvency.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('preselection::solvencies.title.solvencies')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Solvency $solvency
     * @return Response
     */
    public function edit(Solvency $solvency)
    {
        $solvency->load('aspirant');
        $solvency->load('implementAttendant');
        if ($solvency->aspirant->work_contract_date !=null) {
          $work_contract_date =$solvency->aspirant->work_contract_date->format('Y-m-d');
          $medical_exam_date= Carbon::now()->diffInDays($work_contract_date);   // 1 year after
        }else{
          $medical_exam_date='';
        }
        return view('preselection::admin.solvencies.edit', compact('solvency','medical_exam_date'));
    }

    public function editSolvencyGenerated(Solvency $solvency)
    {
        $humanTalentCoordinator= Aspirants::with('unit')->whereIn('unity_id',[75,76])->get(); //GERENTE Y COORDINADOR DE TALENTO HUMANO
        return view('preselection::admin.solvencies.editSolvenciesGenerated', compact('solvency','humanTalentCoordinator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Solvency $solvency
     * @param  UpdateSolvencyRequest $request
     * @return Response
     */
    public function update(Solvency $solvency, UpdateSolvencyRequest $request)
    {
      $data=[];
      $medicalExamen=0;
      if ($request->medical_exam_date >=29  ) {
        $medicalExamen=1;
        if ($request->medical_exam_voucher) {
          $file = $request->file('medical_exam_voucher');
          $extension = $file->getClientOriginalExtension();
          $nameFile =\Illuminate\Support\Str::random(32);
          $allowedextensions = array('JPG', 'JPEG', 'PNG', 'GIF', 'ICO', 'BMP');
          $name = $nameFile . '.' . $extension;
          $destination_path = 'assets/preselection/aspirants/medical_exam/solvencies/'.$name;
          $disk = 'publicmedia';
          if (!in_array(strtoupper($extension), $allowedextensions)) {
            throw new Exception("Extensión del archivo no permitida");
          }
          if (in_array(strtoupper($extension), ['JPG', 'JPEG'])) {
            $image = \Image::make($file);

            \Storage::disk($disk)->put($destination_path, $image->stream($extension, '90'));
          } else {

            \Storage::disk($disk)->put($destination_path, \File::get($file));
          }
          $data['medical_exam_voucher']=$destination_path;
        }else{
          if(isset($request->medical_exam_voucher)){
            $data['medical_exam_voucher']=$request->medical_exam_voucher;
          }
        }

      }

        if(isset($request->implement_attendant_id))
        $data['implement_attendant_id']=$request->implement_attendant_id;
        if (isset($request->implements )) {
          // code...
          $implements=json_decode($request->implements);
          $b=0;
          foreach($implements as $implement){
            if($implement->status==0 || $implement->status=="0"){
              $b=1;
            }//
          }//foreach

          if($b==0 && ($medicalExamen==0 || isset($data['medical_exam_voucher']))){
            $data=[
              'status'=>1
            ];//Status generado
          }
          $aspirant=$this->aspirants->find($solvency->aspirant_id);
          $this->aspirants->update($aspirant, ["implements"=>json_decode($request->implements)]);
        }


        $this->solvency->update($solvency, $data);


        return redirect()->route('admin.preselection.solvency.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::solvencies.title.solvencies')]));
    }

    public function updateGenerated(Solvency $solvency, UpdateSolvencyRequest $request)
    {

        $this->solvency->update($solvency, $request->all());
        return redirect()->route('admin.preselection.solvency.generated')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('preselection::solvencies.title.solvenciesGenerate')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Solvency $solvency
     * @return Response
     */
    public function destroy(Solvency $solvency)
    {
        $this->solvency->destroy($solvency);

        return redirect()->route('admin.preselection.solvency.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('preselection::solvencies.title.solvencies')]));
    }
}
