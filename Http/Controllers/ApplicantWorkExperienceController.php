<?php

namespace Modules\Preselection\Http\Controllers;

use Illuminate\Http\Request;
// use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Ihelpers\Http\Controllers\Api\BaseApiController;
//Repository
use Modules\Preselection\Repositories\AspirantsRepository;
//Request
use Modules\Preselection\Http\Requests\CreateAspirantsRequest;
//Transactions & auth
use Auth;
use DB;
//Entities
use Modules\Preselection\Entities\Aspirants;
// use Modules\Preselection\Entities\StudiesApplicant;
use Modules\Preselection\Entities\ApplicantWorkExperience;
use Modules\Preselection\Entities\DataChangeRequest;
// use Modules\Preselection\Entities\ApplicantCourse;
// use Modules\Preselection\Entities\AspirantFamily;
class ApplicantWorkExperienceController extends BaseApiController
{
  private $aspirants;
  public function __construct(AspirantsRepository $aspirants)
  {
    parent::__construct();
    $this->aspirants=$aspirants;
  }

  public function update($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant){
        throw new \Exception("No posee relación de aspirante",500);
      }
      $dataChangeRequests=DataChangeRequest::where('type','aspirant')->where('aspirant_id',$aspirant->id)->whereStatus(0)->get();
      if(count($dataChangeRequests)>0){
        throw new \Exception("Aún tienes algunas solicitudes de cambio de datos pendientes por validar.",500);
      }
      //Get data
      // $data=$request->all();
      $entity=ApplicantWorkExperience::where('id',$id)->where('aspirant_id',$aspirant->id)->first();
      if(isset($request->business_name)){
        if($request->business_name!=$entity->business_name){
          DataChangeRequest::create([
            'field'=>'business_name',
            'type'=>'workExperience',
            'value'=>$request->business_name,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//business_name
      if(isset($request->business_address)){
        if($request->business_address!=$entity->business_address){
          DataChangeRequest::create([
            'field'=>'business_address',
            'type'=>'workExperience',
            'value'=>$request->business_address,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//business_address
      if(isset($request->business_phone)){
        if($request->business_phone!=$entity->business_phone){
          DataChangeRequest::create([
            'field'=>'business_phone',
            'type'=>'workExperience',
            'value'=>$request->business_phone,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//business_phone
      if(isset($request->business_contact_person)){
        if($request->business_contact_person!=$entity->business_contact_person){
          DataChangeRequest::create([
            'field'=>'business_contact_person',
            'type'=>'workExperience',
            'value'=>$request->business_contact_person,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//business_contact_person
      if(isset($request->job_title)){
        if($request->job_title!=$entity->job_title){
          DataChangeRequest::create([
            'field'=>'job_title',
            'type'=>'workExperience',
            'value'=>$request->job_title,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//job_title
      if(isset($request->description_functions_performed)){
        if($request->description_functions_performed!=$entity->description_functions_performed){
          DataChangeRequest::create([
            'field'=>'description_functions_performed',
            'type'=>'workExperience',
            'value'=>$request->description_functions_performed,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//description_functions_performed
      if(isset($request->since)){
        if($request->since!=$entity->since){
          DataChangeRequest::create([
            'field'=>'since',
            'type'=>'workExperience',
            'value'=>$request->since,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//since
      if(isset($request->until)){
        if($request->until!=$entity->until){
          DataChangeRequest::create([
            'field'=>'until',
            'type'=>'workExperience',
            'value'=>$request->until,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//until
      if(isset($request->reason_for_termination)){
        if($request->reason_for_termination!=$entity->reason_for_termination){
          DataChangeRequest::create([
            'field'=>'reason_for_termination',
            'type'=>'workExperience',
            'value'=>$request->reason_for_termination,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//reason_for_termination
      if(isset($request->last_salary)){
        if($request->last_salary!=$entity->last_salary){
          DataChangeRequest::create([
            'field'=>'last_salary',
            'type'=>'workExperience',
            'value'=>$request->last_salary,
            'entity_id'=>$entity->id,
            'aspirant_id'=>$aspirant->id
          ]);
        }
      }//last_salary
      // $entity->update($data);
      DB::commit();
      return response()->json(["data"=>'','msg'=>'Solicitud de cambio de datos enviada exitosamente.'],200);
      // return response()->json(["data"=>'',"msg"=>"Datos de experiencia laboral actualizados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//update()

  public function delete($id,Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $entity=ApplicantWorkExperience::where('id',$id)->where('aspirant_id',$aspirant->id)->delete();
      DB::commit();
      return response()->json(["data"=>'',"msg"=>"Datos de experiencia laboral eliminados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

  public function create(Request $request){
    try {
      DB::beginTransaction();
      //get Auth id and relation aspirant
      $user=\Auth::user();
      $aspirant=Aspirants::where('identification',$user->email)->first();
      if(!$aspirant)
        throw new \Exception("No posee relación de aspirante",500);
      //Get data
      $data=$request->all();
      $data['aspirant_id']=$aspirant->id;
      $entity=ApplicantWorkExperience::create($data);
      DB::commit();
      return response()->json(["data"=>$entity,"msg"=>"Datos de experiencia laboral registrados exitosamente."],200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(["error"=>$e->getMessage()],500);
    }
  }//delete()

}
