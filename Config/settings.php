<?php

return [
    'percentage_studies' => [
        'description'  => 'preselection::common.settings.percentage_studies',
        'view'         => 'number',
        'default'      => 40,
        'translatable' => false,
    ],
    'percentage_work_experience' => [
        'description'  => 'preselection::common.settings.percentage_work_experience',
        'view'         => 'number',
        'default'      => 40,
        'translatable' => false,
    ],
    'percentage_address' => [
        'description'  => 'preselection::common.settings.percentage_address',
        'view'         => 'number',
        'default'      => 20,
        'translatable' => false,
    ],
    'laboratory_address' => [
        'description'  => 'preselection::common.settings.laboratory_address',
        'view'         => 'text',
        'default'      => "Dirección por configurar",
        'translatable' => false,
    ],
    'doctor_email' => [
        'description'  => 'preselection::common.settings.doctor_email',
        'view'         => 'text',
        'default'      => "sabascarloseduardo@gmail.com",
        'translatable' => false,
    ],
];
