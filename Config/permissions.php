<?php

return [
    'preselection.aspirants' => [
        'menu' => 'preselection::aspirants.list resource',
        'index' => 'preselection::aspirants.list resource',
        'indexDoctor' => 'preselection::aspirants.list resource doctor',
        'indexWorkers' => 'preselection::aspirants.list resource workers',
        'retiredWorkers' => 'preselection::aspirants.list resource retiredWorkers',
        'create' => 'preselection::aspirants.create resource',
        'edit' => 'preselection::aspirants.edit resource',
        'destroy' => 'preselection::aspirants.destroy resource',
    ],
    // 'preselection.aspirantfamilies' => [
    //     'index' => 'preselection::aspirantfamilies.list resource',
    //     'create' => 'preselection::aspirantfamilies.create resource',
    //     'edit' => 'preselection::aspirantfamilies.edit resource',
    //     'destroy' => 'preselection::aspirantfamilies.destroy resource',
    // ],
    'preselection.datachangerequests' => [
        'index' => 'preselection::datachangerequests.list resource',
        'create' => 'preselection::datachangerequests.create resource',
        'edit' => 'preselection::datachangerequests.edit resource',
        'destroy' => 'preselection::datachangerequests.destroy resource',
    ],
    'preselection.solvencies' => [
        'index' => 'preselection::solvencies.list resource',
        'indexGenerada' => 'preselection::solvencies.list resource generate',
        'create' => 'preselection::solvencies.create resource',
        'edit' => 'preselection::solvencies.edit resource',
        'destroy' => 'preselection::solvencies.destroy resource',
    ],
// append





];
